**THIS PROJECT IS OUTDATED AND NO LONGER ACTIVELY WORKED ON**
**THE SUCCESSOR OF THIS PROJECT IS THE [SonarQube Integration for the Atlassian Suite](https://marvelution.atlassian.net/wiki/display/SIFAS) PROJECT**

This project features a plugin for JIRA for integration with Sonar
More details on <https://marvelution.atlassian.net/wiki/display/MARVJIRASONAR>

Issue Tracker
=============
<https://marvelution.atlassian.net/browse/MARVJIRASONAR>

License
=======
[The Apache Software License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
