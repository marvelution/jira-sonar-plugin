/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

AJS.$.namespace("AJS.sonar.panel");

/**
 * Create an error message box for the given message
 * 
 * @param message the message to show in the error box
 * @returns the error box
 */
AJS.sonar.panel.createErrorMessage = function(message) {
	var errorDiv = AJS.$('<div/>').addClass('aui-message').addClass('error');
	errorDiv.append(AJS.$('<span/>').addClass('aui-icon').addClass('icon-error'));
	errorDiv.append(message);
	return errorDiv;
}

/**
 * Create an success message box for the given message
 * 
 * @param message the message to show in the success box
 * @returns the success box
 */
AJS.sonar.panel.createSuccessMessage = function(message) {
	var successDiv = AJS.$('<div/>').addClass('aui-message').addClass('success');
	successDiv.append(AJS.$('<span/>').addClass('aui-icon').addClass('icon-success'));
	successDiv.append(message);
	return successDiv;
}
