/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

AJS.$.namespace("AJS.sonar.admin");

AJS.sonar.accessor.FORCE_SERVLET_QUERY = true;

/**
 * Register the auto-complete feature for the Sonar Project text field
 * This function will register the auto-complete jQuery plugin for the #sonarProject element and will add a onchange to
 * the #sonarServer element
 */
AJS.sonar.admin.registerSonarResourcesAutoComplete = function() {
	var sonarIdField = AJS.$("#sonarId");
	AJS.$("#resourceName").autocomplete({
		minLength: 4,
		source: []
	});
	if (sonarIdField.val() !== "") {
		// The server url is already given, populate the auto complete
		AJS.sonar.admin.populateProjectAutocomplete(sonarIdField.val());
	}
	// Register the focusout event of the server pref field
	sonarIdField.focusout(function() {
		AJS.sonar.admin.populateProjectAutocomplete(this.value);
	});
}

/**
 * Method to populate the autocomplete of the resource name field
 * 
 * @param serverId the server Id to get the resource keys from
 */
AJS.sonar.admin.populateProjectAutocomplete = function(serverId) {
	if (serverId !== "") {
		AJS.$.ajax({
			type: 'GET',
			dataType: 'xml',
			url: contextPath + '/plugins/servlet/sonar/resources',
			data: {
				serverId: serverId,
				url: '/api/resources',
				type: 'xml'
			},
			success: function(data) {
				var resourceTextField = AJS.$("#resourceName");
				$xml = AJS.$(data);
				var source = new Array();
				// Add the new keys
				AJS.$($xml.find("resource")).each(function() {
					source.push(AJS.$(this).find("key").text());
				});
				// Set the new auto complete source
				resourceTextField.autocomplete("option", "source", source);
				// Use offset instead of position to get the absolute position and not the relative one
				var position = resourceTextField.offset();
				AJS.$(".ui-autocomplete").css({
					top: (position.top + resourceTextField.height() + 5) + "px",
					left: position.left + "px"
				});
			}
		});
	} else {
		alert("Please select a valid Sonar Server");
	}
}

/**
 * Register the Project select change to update the Project Components select
 */
AJS.sonar.admin.registerProjectOnChange = function() {
	AJS.$('#projectId').change(function() {
		AJS.sonar.admin.getProjectComponents(AJS.$(this).val());
	});
}

/**
 * Get the project components and update the select box for it
 * 
 * @param projectId the project Id to get the components for
 */
AJS.sonar.admin.getProjectComponents = function (projectId) {
	AJS.$.ajax({
		type: 'GET',
		dataType: 'json',
		data: ({
			projectId: projectId
		}),
		url: contextPath + '/rest/sonar/1.0/projectComponents',
		success: function(data) {
			var components = AJS.$('#componentId');
			components.empty();
			AJS.$('<option/>')
				.attr({value: '0', selected: 'selected'})
				.text('No Component')
				.appendTo(components);
			if (data.components !== undefined) {
				AJS.$(data.components).each(function() {
					AJS.$('<option/>')
						.attr({value: this.componentId})
						.text(this.componentName)
						.appendTo(components);
				});
			}
		}
	});
}
