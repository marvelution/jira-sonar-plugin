/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/**
 * Base object containing all the options for the panel
 */
AJS.sonar.panel.config = {
	readOnly: true,
	panelContainer: '#panelContainer',
	thobber: '#thobber',
	operationsContainer: '#operations',
	columnSelector: '.column',
	columnClass: 'column',
	handle: '.dragbox-header',
	cursor: 'pointer',
	placeHolder: 'placeholder',
	context: 0,
	afterInit: function() {},
	dialogs: {
		gadgets: undefined,
		layout: undefined
	},
	layout: 0,
	associationId: 0,
	oldColumn: undefined
};

/**
 * Initiate the Sonar panel
 * 
 * @param panel the configuration object for the panel
 */
AJS.sonar.panel.initPanel = function(options) {
	jQuery.extend(AJS.sonar.panel.config, options);
	AJS.$(AJS.sonar.panel.config.thobber).addClass('loading');
	AJS.sonar.text.load(contextPath);
	AJS.sonar.panel.initLayout();
	if (!AJS.sonar.panel.config.readOnly) {
		var operationsContainer = AJS.$(AJS.sonar.panel.config.operationsContainer).empty();
		var operations = AJS.$('<ul>').addClass('operations').addClass('enabled');
		operationsContainer.append(operations);
		operations.append(AJS.$('<li/>').html('<div class="throbber" id="dash-throbber"></div>'));
		var gadgetLink = AJS.$('<a/>').attr({id: 'add-gadget', href: '#'})
			.addClass('lnk').addClass('icon-add').addClass('add-gadget-link')
			.text(AJS.sonar.text.getMsg('sonar.add.gadget.link'));
		gadgetLink.click(function() {
			AJS.$('#dash-thobber').addClass('loading');
			var configuredGadgets = new Array();
			AJS.$(AJS.sonar.panel.config.columnSelector).each(function() {
				configuredGadgets = configuredGadgets.concat(AJS.$(this).sortable('toArray'));
			});
			if (AJS.sonar.panel.config.dialogs.gadgets === undefined) {
				AJS.sonar.panel.config.dialogs.gadgets = new AJS.Dialog({
				    width: 650, 
				    height: 500, 
				    id: "gadgets-dialog", 
				    closeOnOutsideClick: false
				});
				AJS.sonar.panel.config.dialogs.gadgets.addHeader(AJS.sonar.text.getMsg('sonar.add.gadget'));
				AJS.sonar.panel.config.dialogs.gadgets.addPanel("Panel 1", "panel1");
				AJS.sonar.panel.config.dialogs.gadgets.addCancel(AJS.sonar.text.getMsg('sonar.close'), function() {
					AJS.sonar.panel.config.dialogs.gadgets.hide();
				});
				AJS.$.ajax({
					type: 'GET',
					url: contextPath + '/rest/sonar/1.0/layout/gadgets',
					dataType: 'json',
					success: function(gadgets) {
						var gadgetList = AJS.$('<ol/>').addClass('gadgetList');
						AJS.$(gadgets.gadget).each(function(index, gadgetId) {
							var gadgetOption = AJS.$('<li/>');
							AJS.$('<img/>').attr({
								src: contextPath + "/download/resources/com.marvelution.jira.plugins.sonar/images/gadgets/sonar-"
									+ gadgetId + "-thumb.png",
								width: 120,
								height: 60
							}).appendTo(gadgetOption);
							var buttons = AJS.$('<div/>').addClass('buttons');
							var action = ((configuredGadgets.indexOf(gadgetId) > -1) ? 'remove' : 'add');
							AJS.$('<input/>').attr({
								id: gadgetId,
								type: 'button',
								value: AJS.sonar.text.getMsg('sonar.' + action + '.button'),
								action: action
							}).click(function(event) {
								var button = AJS.$(this);
								AJS.sonar.panel.handleGadgetAction(button.attr('id'), button.attr('action'));
								event.preventDefault();
							}).appendTo(buttons);
							gadgetOption.append(buttons);
							gadgetOption.append(
								AJS.$('<h3/>').text(AJS.sonar.text.getMsg('sonar.gadget.' + gadgetId + '.title'))
							);
							gadgetOption.append(
								AJS.$('<p/>').text(AJS.sonar.text.getMsg('sonar.gadget.' + gadgetId + '.description'))
							);
							gadgetOption.hover(function() {
								AJS.$(this).addClass('hover');
							}, function() {
								AJS.$(this).removeClass('hover');
							});
							gadgetList.append(gadgetOption);
						});
						AJS.sonar.panel.config.dialogs.gadgets.getCurrentPanel().body.append(gadgetList);
						AJS.$('#dash-thobber').removeClass('loading');
						AJS.sonar.panel.config.dialogs.gadgets.show();
					},
					error: function(data, status, xhr) {
						AJS.$('#dash-thobber').removeClass('loading');
						alert(jQuery.parseJSON(data.responseText).message);
					}
				});
			} else {
				AJS.$('#dash-thobber').removeClass('loading');
				AJS.sonar.panel.config.dialogs.gadgets.show();
			}
		});
		AJS.$('<li/>').append(gadgetLink).appendTo(operations);
		var layoutLink = AJS.$('<a/>').attr({id: 'layout-changer', href: '#'})
			.addClass('lnk').addClass('icon-edit')
			.text(AJS.sonar.text.getMsg('sonar.edit.layout.link'));
		layoutLink.click(function() {
			AJS.$('#dash-thobber').addClass('loading');
			if (AJS.sonar.panel.config.dialogs.layout === undefined) {
				AJS.sonar.panel.config.dialogs.layout = new AJS.Dialog({
				    width: 507, 
				    height: 201, 
				    id: "layout-dialog", 
				    closeOnOutsideClick: false
				});
				AJS.$(AJS.sonar.panel.config.dialogs.layout.popup.element)
					.addClass('layout-' + AJS.sonar.panel.getCurrentLayoutType());
				AJS.sonar.panel.config.dialogs.layout.addHeader(AJS.sonar.text.getMsg('sonar.edit.layout'));
				AJS.sonar.panel.config.dialogs.layout.addPanel("Panel 1", "panel1");
				AJS.sonar.panel.config.dialogs.layout.getCurrentPanel().body.append(
					AJS.$('<p/>').append(
						AJS.$('<strong/>').text(AJS.sonar.text.getMsg('sonar.choose.panel.layout'))
					)
				);
				AJS.sonar.panel.config.dialogs.layout.addCancel(AJS.sonar.text.getMsg('sonar.close'), function() {
					AJS.sonar.panel.config.dialogs.layout.hide();
				});
				AJS.$.ajax({
					type: 'GET',
					url: contextPath + '/rest/sonar/1.0/layout/types',
					dataType: 'json',
					success: function(types) {
						var list = AJS.$('<ul/>');
						AJS.$(types.type).each(function(index, type) {
							var link = AJS.$('<a/>').attr({href: '#', id: 'layout-' + type.toLowerCase()})
								.append(AJS.$('<strong/>').text(type));
							link.click(function() {
								AJS.sonar.panel.config.dialogs.layout.hide();
								AJS.$.ajax({
									type: 'POST',
									url: contextPath + '/rest/sonar/1.0/layout/change/' + AJS.sonar.panel.config.layout,
									data: {
										type: type.toUpperCase()
									},
									success: function() {
										AJS.sonar.panel.initLayout();
									},
									error: function(data, status, xhr) {
										alert(jQuery.parseJSON(data.responseText).message);
									}
								});
							});
							AJS.$('<li/>').append(link).appendTo(list);
						});
						AJS.sonar.panel.config.dialogs.layout.getCurrentPanel().body.append(list);
						AJS.$('#dash-thobber').removeClass('loading');
						AJS.sonar.panel.config.dialogs.layout.show();
					},
					error: function(data, status, xhr) {
						AJS.$('#dash-thobber').removeClass('loading');
						alert(jQuery.parseJSON(data.responseText).message);
					}
				});
			} else {
				AJS.$(AJS.$(AJS.sonar.panel.config.dialogs.layout.popup.element).attr('class').split(' '))
					.each(function(index, clazz) {
						if (clazz.indexOf('layout') == 0) {
							AJS.$(AJS.sonar.panel.config.dialogs.layout.popup.element).removeClass(clazz);
						}
					});
				AJS.$(AJS.sonar.panel.config.dialogs.layout.popup.element)
					.addClass('layout-' + AJS.sonar.panel.getCurrentLayoutType());
				AJS.$('#dash-thobber').removeClass('loading');
				AJS.sonar.panel.config.dialogs.layout.show();
			}
		});
		AJS.$('<li/>').append(layoutLink).appendTo(operations);
		if (AJS.sonar.panel.config.context !== 0) {
			var deleteLink = AJS.$('<a/>').attr({
					id: 'layout-deleter',
					href: 'AdministerSonarLayout!delete.jspa?context=' + AJS.sonar.panel.config.context
				}).addClass('lnk').addClass('icon-tools')
				.text(AJS.sonar.text.getMsg('sonar.delete.layout.link'));
			AJS.$('<li/>').append(deleteLink).appendTo(operations);
		}
	}
}

/**
 * Get the current layout type
 * 
 * @returns the layout type
 */
AJS.sonar.panel.getCurrentLayoutType = function() {
	try {
		return AJS.$(AJS.sonar.panel.config.panelContainer + ' ' + AJS.sonar.panel.config.columnSelector)
			.attr('class').split(' ')[1].split('_')[1].toLowerCase();
	} catch (e) {
		return 'aa';
	}
}

/**
 * Internal method to initialize the layout
 */
AJS.sonar.panel.initLayout = function() {
	AJS.$.ajax({
		type: 'GET',
		url: contextPath + '/rest/sonar/1.0/layout/' + AJS.sonar.panel.config.context,
		dataType: 'json',
		async: false,
		success: function(layout) {
			var panelContainer = AJS.$(AJS.sonar.panel.config.panelContainer);
			panelContainer.empty();
			AJS.sonar.panel.config.layout = layout.layoutId;
			if (layout.contextID != AJS.sonar.panel.config.context) {
				AJS.sonar.panel.config.readOnly = true;
			}
			AJS.$(layout.column).each(function(index, column) {
				var columnElement = AJS.$('<div/>').attr({id: 'column_' + column.columnId});
				columnElement.addClass('column').addClass('column_' + layout.type + '_' + column.type);
				panelContainer.append(columnElement);
				AJS.$(column.gadget).each(function(index, gadgetId) {
					AJS.sonar.panel.addGadgetToColumn(columnElement, gadgetId);
				});
			});
			if (!AJS.sonar.panel.config.readOnly) {
				AJS.$(AJS.sonar.panel.config.panelContainer + ' ' + AJS.sonar.panel.config.columnSelector).sortable({
					connectWith: AJS.sonar.panel.config.columnSelector,
					handle: AJS.sonar.panel.config.handle,
					cursor: AJS.sonar.panel.config.cursor,
					placeholder: AJS.sonar.panel.config.placeHolder,
					forcePlaceholderSize: true,
					opacity: 0.4,
					start: function(event, ui) {
						AJS.sonar.panel.config.oldColumn = ui.helper.parent();
					},
					stop: function(event, ui) {
						AJS.sonar.panel.handleGadgetAction(ui.item.attr('id'), 'move');
					}
				}).disableSelection();
			}
			AJS.$(AJS.sonar.panel.config.thobber).removeClass('loading');
			AJS.sonar.panel.config.afterInit();
		},
		error: function(data, status, xhr) {
			AJS.$(AJS.sonar.panel.config.thobber).removeClass('loading');
			AJS.$(AJS.sonar.panel.config.panelContainer).empty().append(
				AJS.sonar.panel.createErrorMessage('Failed to load the Panel layout. Please constult your system administrator for assistance.')
			);
		}
	});
}

/**
 * After initialization of the Panel, load the content of the gadgets for the given association id.
 * 
 * @param associationId the association ID to load
 */
AJS.sonar.panel.loadAssociation = function(associationId) {
	AJS.$(AJS.sonar.panel.config.thobber).addClass('loading');
	AJS.sonar.panel.config.associationId = associationId;
	AJS.$('.dragbox .dragbox-content').each(function(index, item) {
		var parent = AJS.$(item).parent();
		AJS.sonar.panel.loadGadget(parent.attr('id'));
	});
	AJS.$(AJS.sonar.panel.config.thobber).removeClass('loading');
}

/**
 * Load the content of a gadget
 * 
 * @param gadgetId the gadget id
 */
AJS.sonar.panel.loadGadget = function(gadgetId) {
	AJS.$.ajax({
		type: 'GET',
		url: contextPath + '/rest/sonar/1.0/gadgets/generateHtml',
		dataType: 'html',
		data: ({
			associationId: AJS.sonar.panel.config.associationId,
			gadgetId: gadgetId
		}),
		success: function(html) {
			AJS.$('#' + gadgetId).find('.dragbox-content').html(html);
			AJS.sonar.panel.resizeContainer();
		},
		error: function(data, status, xhr) {
			var container = AJS.$('.dragbox[id="' + gadgetId + '"]').find('.dragbox-content');
			container.empty();
			container.append(AJS.sonar.panel.createErrorMessage(jQuery.parseJSON(data.responseText).message));
			AJS.sonar.panel.resizeContainer();
		}
	});
}

/**
 * Resize the container holding the panel
 */
AJS.sonar.panel.resizeContainer = function() {
	// TODO resize the panel container
}

/**
 * Internal method to handle the add, remove, and move actions of gadgets
 * 
 * @param gadgetId the Id of the gadget that is being handled
 * @param action the action (add, remove, move)
 */
AJS.sonar.panel.handleGadgetAction = function(gadgetId, action) {
	var ajaxOptions = {
		type: 'POST',
		url: contextPath + '/rest/sonar/1.0/layout/' + action + '/' + gadgetId,
		data: {},
		success: function() {
			AJS.$(AJS.sonar.panel.config.panelContainer).prepend(
				AJS.sonar.panel.createSuccessMessage(AJS.sonar.text.getMsg('sonar.layout.save.successful'))
			);
		},
		error: function(data, status, xhr) {
			AJS.$(AJS.sonar.panel.config.panelContainer).prepend(
				AJS.sonar.panel.createErrorsMessage(jQuery.parseJSON(data.responseText).message)
			);
		}
	};
	AJS.$('.gadgetList input[type="button"]').enable(false);
	var gadgetActionButton = AJS.$('.gadgetList input[id="' + gadgetId + '"]');
	AJS.$(AJS.sonar.panel.config.panelContainer + ' .aui-message').remove();
	if (action === 'add') {
		var column = AJS.$(AJS.$(AJS.sonar.panel.config.panelContainer + ' '
			+ AJS.sonar.panel.config.columnSelector)[0]);
		AJS.sonar.panel.addGadgetToColumn(column, gadgetId);
		AJS.sonar.panel.loadGadget(gadgetId);
		gadgetActionButton.attr({
			action: 'remove',
			value: AJS.sonar.text.getMsg('sonar.remove.button')
		});
		ajaxOptions.data = {columnId: column.attr('id').split('_')[1]};
	} else if (action === 'remove') {
		var gadget = AJS.$('.dragbox[id="' + gadgetId + '"]');
		var column = gadget.parent();
		gadget.remove();
		gadgetActionButton.attr({
			action: 'add',
			value: AJS.sonar.text.getMsg('sonar.add.button')
		});
		ajaxOptions.data = {columnId: column.attr('id').split('_')[1]};
	} else if (action === 'move') {
		var gadget = AJS.$('.dragbox[id="' + gadgetId + '"]');
		var column = gadget.parent();
		ajaxOptions.data = {
			oldColumnId: AJS.sonar.panel.config.oldColumn.attr('id').split('_')[1],
			newColumnId: column.attr('id').split('_')[1],
			position: column.sortable('toArray').indexOf(gadgetId)
		};
	}
	AJS.$.ajax(ajaxOptions);
	AJS.$('.gadgetList input[type="button"]').enable();
}

/**
 * Add a gadget to a column in the panel
 * 
 * @param column the column to add the gadget to
 * @param gadgetId the id of the gadget to add
 */
AJS.sonar.panel.addGadgetToColumn = function(column, gadgetId) {
	var gadget = AJS.sonar.panel.createGadgetBox(gadgetId);
	if (!AJS.sonar.panel.config.readOnly) {
		gadget.dblclick(function() {
			AJS.sonar.panel.handleGadgetAction(AJS.$(this).attr('id'), 'remove');
			event.preventDefault();
		});
		gadget.hover(function() {
			AJS.$(this).find('.menu').show();
		}, function() {
			AJS.$(this).find('.menu').hide();
		});
	}
	column.append(gadget);
}

/**
 * Helper method to create the gadget draggable box
 * 
 * @param gadgetId the id of the gadget
 * @return the jQuery wrapped div
 */
AJS.sonar.panel.createGadgetBox = function(gadgetId) {
	var header = AJS.$('<div/>').addClass('dragbox-header').append(
		AJS.$('<h3/>').text(AJS.sonar.text.getMsg('sonar.gadget.' + gadgetId + '.title'))
	).append(
		AJS.$('<div/>').addClass('menu').append(
			AJS.$('<ul/>').append(
				AJS.$('<li/>').addClass('aui-icon').addClass('i-move').text('move')
			)
		)
	);
	return AJS.$('<div/>').attr({id: gadgetId}).addClass('dragbox').append(header).append(
		AJS.$('<div/>').addClass('dragbox-content').append(
			AJS.$("<img/>").attr({src: contextPath + '/images/throbber/loading_barber_pole_horz.gif'})
				.addClass('loading-bar')
		)
	);
}
