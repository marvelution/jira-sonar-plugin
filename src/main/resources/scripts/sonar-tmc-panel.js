/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

AJS.$.namespace("AJS.sonar.tmc.panel");

/**
 * Base object containing all the options for the panel
 */
AJS.sonar.tmc.panel.config = {
	chartColumn : '#chart-metrics-column',
	chartContainer : '#chart-container',
	metricsContainer : '#metrics-container',
	versionsColumn : '#versions-column',
	thobber: '#thobber',
	defaultMetrics : [ 'violations_density', 'complexity', 'coverage' ],
	context : 0,
	afterInit : function() {},
	associationId : 0
};

/**
 * Initiate the Sonar panel
 * 
 * @param panel the configuration object for the panel
 */
AJS.sonar.tmc.panel.initPanel = function(options) {
	jQuery.extend(AJS.sonar.tmc.panel.config, options);
	AJS.sonar.tmc.panel.resetPanel();
	AJS.sonar.tmc.panel.config.afterInit();
}

/**
 * Reset the Sonar Panel
 */
AJS.sonar.tmc.panel.resetPanel = function() {
	AJS.$(AJS.sonar.tmc.panel.config.chartContainer).empty();
	AJS.$(AJS.sonar.tmc.panel.config.metricsContainer).empty();
	AJS.$(AJS.sonar.tmc.panel.config.versionsColumn).empty();
}

/**
 * After initialization of the Panel, load the content of the gadgets for the given association id.
 * 
 * @param associationId the association ID to load
 */
AJS.sonar.tmc.panel.loadAssociation = function(associationId) {
	AJS.sonar.tmc.panel.config.associationId = associationId;
	AJS.$(AJS.sonar.tmc.panel.config.thobber).addClass('loading');
	AJS.sonar.tmc.panel.resetPanel();
	// Update versions (sync), metrics (async), chart (sync/default metrics)
	AJS.$.ajax({
		type : "GET",
		async: false,
		dataType: "xml",
		url : contextPath + "/rest/sonar/1.0/association/" + AJS.sonar.tmc.panel.config.associationId
			+ "/project/versions",
		success: function(data) {
			$xml = AJS.$(data);
			var versionsColumn = AJS.$(AJS.sonar.tmc.panel.config.versionsColumn).empty();
			AJS.$("<div/>").addClass("mod-header").append(
				AJS.$("<h3/>").addClass("toggle-title").text("Versions")
			).appendTo(versionsColumn);
			var versionsList = AJS.$("<ol/>").appendTo(versionsColumn);
			AJS.$($xml.find("version")).each(function(index, item) {
				var $version = AJS.$(item);
				var versionLi = AJS.$("<li/>");
				var versionId = $version.find("sid").text();
				AJS.$("<input/>").attr({
					"type": "checkbox",
					"name": "tm-version",
					"value": $version.find("date").text(),
					"id" : "tm-version-" + versionId,
					"checked" : ((index < 5) ? "checked" : "")
				}).click(function() {
					AJS.sonar.tmc.panel.updateChart();
				}).appendTo(versionLi);
				AJS.$("<label/>").attr({"for": "tm-version-" + versionId}).text($version.find("name").text())
					.appendTo(versionLi);
				AJS.$("<div/>").addClass("desc").text(new Date($version.find("date").text()).format("M jS, Y"))
					.appendTo(versionLi);
				versionLi.appendTo(versionsList);
			});
			AJS.sonar.tmc.panel.updateChart();
		},
		error: function() {
			AJS.sonar.panel.createErrorMessage("Failed to load the versions related to the selected association.")
				.appendTo(versionsColumn);
		}
	});
	AJS.$.ajax({
		type: "GET",
		async: true,
		dataType: "xml",
		url : contextPath + "/rest/sonar/1.0/association/" + AJS.sonar.tmc.panel.config.associationId +
			"/server/metrics",
		success: function(data) {
			$xml = AJS.$(data);
			var metricsColumn = AJS.$(AJS.sonar.tmc.panel.config.metricsContainer).empty();
			AJS.$("<div/>").addClass("mod-header").append(
					AJS.$("<h3/>").addClass("toggle-title").text("Metrics")
				).appendTo(metricsColumn);
			var metricsList = AJS.$("<ul/>").appendTo(metricsColumn);
			AJS.$($xml.find("metric")).each(function(index, item) {
				var $metric = AJS.$(item);
				var metricLi = AJS.$("<li/>");
				var metricKey = $metric.find("key").text();
				AJS.$("<input/>").attr({
					"type": "checkbox",
					"name": "tm-metric",
					"value": metricKey,
					"id" : "tm-metric-" + metricKey,
					"checked" : ((AJS.$.inArray(metricKey,
						AJS.sonar.tmc.panel.config.defaultMetrics) != -1) ? "checked" : "")
				}).click(function() {
					AJS.sonar.tmc.panel.updateChart();
				}).appendTo(metricLi);
				AJS.$("<label/>").attr({"for": "tm-metric-" + metricKey}).text($metric.find("name").text())
					.appendTo(metricLi);
				metricLi.appendTo(metricsList);
			});
		},
		error: function() {
			AJS.sonar.panel.createErrorMessage("Failed to get the time machine metrics from the server."
					+ " Only the default metrics will be avialable.").appendTo(metricsColumn);
		}
	});
}

/**
 * Update the chart after versions or metrics change
 */
AJS.sonar.tmc.panel.updateChart = function() {
	AJS.$(AJS.sonar.tmc.panel.config.thobber).addClass('loading');
	var chartImg = AJS.$("#tm-chart");
	chartImg.show();
	if (chartImg.length == 0) {
		chartImg = AJS.$("<img/>").attr("id", "tm-chart").appendTo(AJS.$(AJS.sonar.tmc.panel.config.chartContainer));
		chartImg.load(function() {
			AJS.$(AJS.sonar.tmc.panel.config.thobber).removeClass('loading');
		});
		chartImg.error(function() {
			AJS.$(this).hide();
			AJS.$(AJS.sonar.tmc.panel.config.thobber).removeClass('loading');
			AJS.sonar.panel.createErrorMessage("Failed to load the Time Machine chart.")
				.css("margin", "20px")
				.appendTo(AJS.$(AJS.sonar.tmc.panel.config.chartContainer).empty());
		});
	}
	var tab = AJS.$(".tab[data-association-id='" + AJS.sonar.tmc.panel.config.associationId + "']");
	var sonarUrl = tab.attr("data-sonar-url");
	var sonarKey = tab.attr("data-sonar-key");
	var width = (Math.round(AJS.$(AJS.sonar.tmc.panel.config.chartContainer).width() / 10) * 9);
	var height = Math.round(width / 2.8);
	var versions = new Array();
	AJS.$(AJS.sonar.tmc.panel.config.versionsColumn + " input[type='checkbox']").each(function() {
		if (AJS.$(this).is(':checked')) {
			versions.push(AJS.$(this).val());
		}
	});
	var metrics = new Array();
	AJS.$(AJS.sonar.tmc.panel.config.metricsContainer + " input[type='checkbox']").each(function() {
		if (AJS.$(this).is(':checked')) {
			metrics.push(AJS.$(this).val());
		}
	});
	if (metrics.length == 0) {
		AJS.$(AJS.sonar.tmc.panel.config.defaultMetrics).each(function(index, item) {
			metrics.push(item);
		});
	}
	chartImg.attr("src", sonarUrl + "/charts/trends/" + sonarKey + "?locale=en-US&from=" + versions[versions.length-1]
		+ "&to=" + versions[0] + "&ts=" + new Date().getTime() + "&w=" + width + "&h=" + height + "&metrics="
		+ metrics.join()).css("margin-left", Math.round((chartImg.parent().width() / 10) / 2) + "px");
}
