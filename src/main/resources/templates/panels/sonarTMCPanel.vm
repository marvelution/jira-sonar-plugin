##
## Licensed to Marvelution under one or more contributor license 
## agreements.  See the NOTICE file distributed with this work 
## for additional information regarding copyright ownership.
## Marvelution licenses this file to you under the Apache License,
## Version 2.0 (the "License"); you may not use this file except
## in compliance with the License.
## You may obtain a copy of the License at
##
##  http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing,
## software distributed under the License is distributed on an
## "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
## KIND, either express or implied. See the License for the
## specific language governing permissions and limitations
## under the License.
##

$!webResourceManager.requireResourcesForContext("sonar-tmc-panel")

<html>
<head></head>
<body id="jira" class="aui-layout auu-style-default">
#if ($isAssociated)
<script type="text/javascript">
AJS.sonar.tmc.panel.troggle = function(associationId) {
	AJS.$("#tabs .tab").each(function(index, item) {
		AJS.$("#" + item.id).removeClass("selected");
	});
	AJS.$("#tab_" + associationId).addClass("selected");
	AJS.sonar.tmc.panel.loadAssociation(associationId);
}
AJS.sonar.tmc.panel.initTabs = function() {
	var first = 0;
	AJS.$("#tabs .tab").each(function(index, item) {
		var tab = AJS.$("#" + item.id);
		var tabId = tab.attr('data-association-id');
		if (tabId !== undefined) {
			tab.click(function(event) {
				AJS.sonar.tmc.panel.troggle(tabId);
				event.preventDefault();
			});
			if (first == 0) {
				first = tabId;
			}
			jQuery.ajax({
				type : "GET",
				async: true,
				dataType: "text",
				url : contextPath + "/rest/sonar/1.0/association/" + tabId + "/project/name",
				success: function(data) {
					tab.text(data);
				}
			});
		}
	});
	AJS.sonar.tmc.panel.troggle(first);
}
</script>
#end

<div class="module">
	#if ($isAssociated)
	<div class="module-header header">
		<div id="operations"></div>
		<div class="aui-message info">
		<span class="aui-icon icon-info"></span>
		$i18n.getText("sonar.panel.header.description")
		</div>
		<ul id="tabs">
		#foreach ($association in $associations)
			<li id="tab_$association.ID" class="tab" data-association-id="$association.ID" data-sonar-url="$association.sonarServer.host" data-sonar-key="$association.sonarProject">$association.sonarProject</li>
		#end
			<li id="thobber" class="tab"></li>
		</ul>
	</div>
	#end
	<div class="module-content">
		<div id="chart-metrics-column" class="column">
			<div id="chart-container"></div>
			<div id="metrics-container"></div>
		</div>
		<div id="versions-column" class="column"></div>
	</div>
</div>

<script type="text/javascript">
#if ($isAssociated)
// Initiate the tabs and set the first to selected
AJS.sonar.tmc.panel.initPanel({
	context: "${context}",
	afterInit: AJS.sonar.tmc.panel.initTabs
});
#end
</script>
</body>
</html>
