/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.panels;

import java.util.Collection;
import java.util.Map;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.plugin.TabPanelModuleDescriptor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.marvelution.jira.plugins.sonar.predicates.SonarPredicates;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociation;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociationManager;
import com.marvelution.jira.plugins.sonar.utils.PluginHelper;

/**
 * Helper class for Sonar Tab Panels
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * 
 * @since 1.2.0
 */
public class SonarTabPanelHelper {

	private final SonarAssociationManager associationManager;
	private final PermissionManager permissionManager;
	private final WebResourceManager webResourceManager;

	/**
	 * Constructor
	 * 
	 * @param associationManager the {@link SonarAssociationManager} implementation
	 * @param permissionManager the {@link PermissionManager} implementation
	 * @param webResourceManager the {@link WebResourceManager} implementation
	 */
	public SonarTabPanelHelper(SonarAssociationManager associationManager, PermissionManager permissionManager,
			WebResourceManager webResourceManager) {
		this.associationManager = associationManager;
		this.permissionManager = permissionManager;
		this.webResourceManager = webResourceManager;
	}

	/**
	 * Prepare the Velocity parameters for the Sonar Tab Panel
	 * 
	 * @param project the {@link Project}
	 * @param component the {@link ProjectComponent}
	 * @param user the {@link User}
	 * @param params the base {@link Map} of parameters where the panel parameters will be added to
	 * @return the {@link Map} of parameters added to the parameters given
	 */
	public Map<String, Object> prepareVelocityParameters(Project project, ProjectComponent component, User user,
					Map<String, Object> params, TabPanelModuleDescriptor<?> descriptor) {
		params.putAll(descriptor.getParams());
		Long componentId = 0L;
		if (component != null) {
			componentId = component.getId();
		}
		params.put("pluginKey", PluginHelper.getPluginKey());
		params.put("waitImage", "/download/resources/" + PluginHelper.getPluginKey() + "/images/wait.gif");
		if (!params.containsKey("context")) {
			params.put("context", project.getId());
		}
		params.put("componentId", componentId);
		Collection<SonarAssociation> associations = getAssociations(project, component, descriptor);
		if (!associations.isEmpty()) {
			params.put("isAssociated", Boolean.TRUE);
			params.put("associations", associations);
		} else {
			params.put("isAssociated", Boolean.FALSE);
		}
		final boolean isAdmin = permissionManager.hasPermission(Permissions.PROJECT_ADMIN, project, user);
		params.put("isAdmin", isAdmin);
		if (isAdmin) {
			webResourceManager.requireResource(PluginHelper.getPluginKey() + ":sonar-admin");
		}
		return params;
	}

	/**
	 * Check to see if the Panel should be shown or not
	 * 
	 * @param project the {@link Project}
	 * @param component the {@link ProjectComponent}
	 * @param user the {@link User}
	 * @param descriptor the {@link TabPanelModuleDescriptor} of the panel
	 * @return <code>true</code> to show the panel, <code>false</code> otherwise
	 */
	public boolean showPanel(Project project, ProjectComponent component, User user,
			TabPanelModuleDescriptor<?> descriptor) {
		return permissionManager.hasPermission(Permissions.BROWSE, project, user)
			&& associationManager.hasAssociations(project, component)
			&& Iterables.any(associationManager.getAssociations(project, component),
				getPredicates(descriptor.getParams()));
	}

	/**
	 * 
	 * @param project
	 * @param component
	 * @param params
	 * @return
	 */
	private Collection<SonarAssociation> getAssociations(Project project, ProjectComponent component,
		TabPanelModuleDescriptor<?> descriptor) {
		return Collections2.filter(associationManager.getAssociations(project, component),
			getPredicates(descriptor.getParams()));
	}

	private Predicate<SonarAssociation> getPredicates(Map<String, String> params) {
		Collection<Predicate<SonarAssociation>> predicates = Lists.newArrayList();
		if (params.containsKey("metricKey")) {
			predicates.add(SonarPredicates.associationServerHasMetric((String) params.get("metricKey")));
		}
		if (params.containsKey("serverMajorVersion") && params.containsKey("serverMinorVersion")) {
			predicates.add(SonarPredicates.isAssociationServerPriorToVersion(
				Integer.decode(params.get("serverMajorVersion")),
				Integer.decode(params.get("serverMinorVersion"))));
		}
		Predicate<SonarAssociation> predicate = Predicates.alwaysTrue();
		if (!predicates.isEmpty()) {
			predicate = Predicates.and(predicates);
		}
		return predicate;
	}

}
