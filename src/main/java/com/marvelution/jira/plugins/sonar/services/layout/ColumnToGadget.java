/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.services.layout;

import net.java.ao.Entity;
import net.java.ao.schema.NotNull;

/**
 * The {@link Column} to {@link Gadget} relationship helper
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * 
 * @since 2.4.0
 */
public interface ColumnToGadget extends Entity {

	/**
	 * Getter for the {@link Column}
	 * 
	 * @return the {@link Column}
	 */
	@NotNull
	Column getColumn();

	/**
	 * Setter of the {@link Column}
	 * 
	 * @param column the {@link Column} to set
	 */
	void setColumn(Column column);

	/**
	 * Getter of the {@link Gadget}
	 * 
	 * @return the {@link Gadget}
	 */
	@NotNull
	Gadget getGadget();

	/**
	 * Setter of the {@link Gadget}
	 * 
	 * @param gadget the {@link Gadget} to set
	 */
	void setGadget(Gadget gadget);

}
