/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.web.action.admin.associations;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.lang.StringUtils;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociation;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociationManager;
import com.marvelution.jira.plugins.sonar.services.servers.SonarClientFactory;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServerManager;
import com.marvelution.jira.plugins.sonar.web.action.admin.KeyValuePair;
import com.marvelution.jira.plugins.sonar.web.action.admin.ModifyActionType;

/**
 * Update {@link SonarAssociation} Web Action implementation
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class UpdateSonarAssociation extends AbstractModifySonarAssociation {

	private static final long serialVersionUID = 1L;

	private SonarAssociation association;
	private int associationId;

	/**
	 * Constructor
	 * 
	 * @param serverManager the {@link SonarServerManager} implementation
	 * @param associationManager the {@link SonarAssociationManager} implementation
	 * @param clientFactory the {@link SonarClientFactory} implementation
	 * @param componentManager the {@link ProjectComponentManager} implementation
	 */
	protected UpdateSonarAssociation(SonarServerManager serverManager, SonarAssociationManager associationManager,
										SonarClientFactory clientFactory, ProjectComponentManager componentManager) {
		super(serverManager, associationManager, clientFactory, componentManager);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String doDefault() throws Exception {
		if (!associationManager.hasAssociation(getAssociationId())) {
			return getRedirect(ADMINISTER_ASSOCIATIONS);
		}
		association = associationManager.getAssociation(getAssociationId());
		return super.doDefault();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveAssociation(int sonarId, long projectId, long componentId, String resourceName) {
		associationManager.updateAssociation(getAssociationId(), sonarId, projectId, componentId, resourceName);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ModifyActionType getActionType() {
		return ModifyActionType.UPDATE_ASSOCIATION;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<KeyValuePair> getExtraHiddenInput() {
		return new ArrayList<KeyValuePair>();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<ProjectComponent> getComponentOptions() {
		return componentManager.findAllForProject(getProjectId());
	}

	/**
	 * Getter for associationId
	 * 
	 * @return the associationId
	 */
	public int getAssociationId() {
		return associationId;
	}

	/**
	 * Setter for associationId
	 * 
	 * @param associationId the associationId to set
	 */
	public void setAssociationId(int associationId) {
		this.associationId = associationId;
	}

	/**
	 * Getter for sonarId
	 * 
	 * @return the sonarId
	 */
	public int getSonarId() {
		if (super.getSonarId() > 0) {
			return super.getSonarId();
		}
		return association.getSonarServer().getID();
	}

	/**
	 * Getter for projectId
	 * 
	 * @return the projectId
	 */
	public long getProjectId() {
		if (super.getProjectId() > 0L) {
			return super.getProjectId();
		}
		return association.getProjectId();
	}

	/**
	 * Getter for componentId
	 * 
	 * @return the componentId
	 */
	public long getComponentId() {
		if (super.getComponentId() > 0L) {
			return super.getComponentId();
		}
		return association.getComponentId();
	}

	/**
	 * Getter for resourceName
	 * 
	 * @return the resourceName
	 */
	public String getResourceName() {
		if (StringUtils.isNotBlank(super.getResourceName())) {
			return super.getResourceName();
		}
		return association.getSonarProject();
	}

}
