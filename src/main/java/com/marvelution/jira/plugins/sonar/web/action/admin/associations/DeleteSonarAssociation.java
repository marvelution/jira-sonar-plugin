/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.web.action.admin.associations;

import com.atlassian.jira.bc.EntityNotFoundException;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.project.Project;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociation;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociationManager;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServer;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServerManager;
import com.marvelution.jira.plugins.sonar.web.action.admin.AbstractSonarProjectAdminWebActionSupport;

/**
 * Delete {@link SonarServer} WebAction implementation
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class DeleteSonarAssociation extends AbstractSonarProjectAdminWebActionSupport {

	private static final long serialVersionUID = 1L;

	private final SonarAssociationManager associationManager;
	protected final ProjectComponentManager componentManager;

	private int associationId;
	private SonarAssociation association;

	/**
	 * Constructor
	 * 
	 * @param serverManager the {@link SonarServerManager} implementation
	 * @param associationManager the {@link SonarAssociationManager} implementation
	 * @param componentManager the {@link ProjectComponentManager} implementation
	 */
	protected DeleteSonarAssociation(SonarServerManager serverManager, SonarAssociationManager associationManager,
								ProjectComponentManager componentManager) {
		super(serverManager);
		this.associationManager = associationManager;
		this.componentManager = componentManager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String doDefault() throws Exception {
		association = associationManager.getAssociation(getAssociationId());
		return super.doDefault();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String doExecute() throws Exception {
		associationManager.removeAssociation(getAssociationId());
		return getRedirect(ADMINISTER_ASSOCIATIONS + getContext());
	}

	/**
	 * Getter for associationId
	 * 
	 * @return the associationId
	 */
	public int getAssociationId() {
		return associationId;
	}

	/**
	 * Setter for association Id
	 * 
	 * @param associationId the association Id to set
	 */
	public void setAssociationId(int associationId) {
		this.associationId = associationId;
	}

	/**
	 * Getter for association
	 * 
	 * @return the association
	 */
	public SonarAssociation getAssociation() {
		return association;
	}

	/**
	 * Getter for the Resource Name
	 * 
	 * @return the Resource Name
	 */
	public String getResourceName() {
		return getAssociation().getSonarProject();
	}

	/**
	 * Getter for the {@link Project}
	 * 
	 * @return the {@link Project}
	 */
	public Project getAssociationProject() {
		return getProjectManager().getProjectObj(getAssociation().getProjectId());
	}

	/**
	 * Getter for the {@link ProjectComponent}
	 * 
	 * @return the {@link ProjectComponent}
	 * @throws EntityNotFoundException 
	 */
	public ProjectComponent getProjectComponent() throws EntityNotFoundException {
		return componentManager.find(getAssociation().getComponentId());
	}

	/**
	 * Getter for the {@link SonarServer}
	 * 
	 * @return the {@link SonarServer}
	 */
	public SonarServer getServer() {
		return getAssociation().getSonarServer();
	}

}
