/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.services.servers;

import static com.google.common.base.Preconditions.checkNotNull;

import com.marvelution.gadgets.sonar.wsclient.connectors.BasicAuthenticationHttpClient4Connector;
import org.sonar.wsclient.Host;
import org.sonar.wsclient.Sonar;

import com.atlassian.activeobjects.external.ActiveObjects;

/**
 * Default {@link SonarClientFactory}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class SonarClientFactoryService implements SonarClientFactory {

	private final ActiveObjects objects;

	/**
	 * Constructor
	 *
	 * @param objects the {@link ActiveObjects} implementation
	 */
	public SonarClientFactoryService(ActiveObjects objects) {
		this.objects = checkNotNull(objects, "activeObjects");
	}

	@Override
	public Sonar create(Host host) {
		return new Sonar(new BasicAuthenticationHttpClient4Connector(host));
	}

	@Override
	public Sonar create(SonarServer server) {
		return new Sonar(new CacheAwareSonarDelegatingConnector(objects, server,
				new BasicAuthenticationHttpClient4Connector(new Host(server.getHost(), server.getUsername(),
						server.getPassword()))));
	}

}
