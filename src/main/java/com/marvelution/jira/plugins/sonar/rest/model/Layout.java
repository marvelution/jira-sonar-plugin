/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.rest.model;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.google.common.collect.Lists;

/**
 * Columns resource
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "layout")
public class Layout {

	private static final ToStringStyle TO_STRING_STYLE = ToStringStyle.SIMPLE_STYLE;

	@XmlElement(required = true)
	private int layoutId;
	@XmlElement(required = true)
	private com.marvelution.jira.plugins.sonar.services.layout.Layout.Type type;
	@XmlElement(required = true)
	private com.marvelution.jira.plugins.sonar.services.layout.Layout.Context context;
	@XmlElement
	private long contextID;
	@XmlElement(name = "column", required = true)
	private Collection<Column> columns;

	/**
	 * Constructor
	 */
	public Layout() {
		// Default constructor used by the REST framework
	}

	/**
	 * Constructor
	 * 
	 * @param layout the {@link com.marvelution.jira.plugins.sonar.services.layout.Layout} to create the rest resource
	 * 				 form
	 */
	public Layout(com.marvelution.jira.plugins.sonar.services.layout.Layout layout) {
		layoutId = layout.getID();
		type = layout.getType();
		context = layout.getContext();
		if (com.marvelution.jira.plugins.sonar.services.layout.Layout.Context.PROJECT.equals(layout.getContext())) {
			contextID = layout.getContextID();
		}
		columns = Lists.newLinkedList();
		for (com.marvelution.jira.plugins.sonar.services.layout.Column column : layout.getColumns()) {
			columns.add(new Column(column));
		}
	}
	
	/**
	 * Getter for layoutId
	 *
	 * @return the layoutId
	 */
	public int getLayoutId() {
		return layoutId;
	}

	/**
	 * Getter for type
	 *
	 * @return the type
	 */
	public com.marvelution.jira.plugins.sonar.services.layout.Layout.Type getType() {
		return type;
	}

	/**
	 * Getter for context
	 *
	 * @return the context
	 */
	public com.marvelution.jira.plugins.sonar.services.layout.Layout.Context getContext() {
		return context;
	}

	/**
	 * Getter for contextID
	 *
	 * @return the contextID
	 */
	public long getContextID() {
		return contextID;
	}

	/**
	 * @return the columns
	 */
	public Collection<Column> getColumns() {
		return columns;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object object) {
		return EqualsBuilder.reflectionEquals(this, object);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, Layout.TO_STRING_STYLE);
	}

}
