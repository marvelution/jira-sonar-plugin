/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.upgrade.activeobjects;

import static com.google.common.base.Preconditions.checkNotNull;

import org.apache.log4j.Logger;

import net.java.ao.DBParam;
import net.java.ao.Query;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.ActiveObjectsUpgradeTask;
import com.atlassian.activeobjects.external.ModelVersion;
import com.marvelution.gadgets.sonar.utils.SonarGadgetsUtils;
import com.marvelution.jira.plugins.sonar.services.layout.Gadget;
import com.marvelution.jira.plugins.sonar.utils.PluginHelper;

/**
 * {@link ActiveObjectsUpgradeTask} implementation to make sure that all the gadgets are available in the
 * {@link ActiveObjects} table.
 * 
 * This {@link ActiveObjectsUpgradeTask} has a dynamic {@link ModelVersion} based on the plugin version.
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 2.4.0
 */
public class SonarGadgetsActiveObjectsValidator implements ActiveObjectsUpgradeTask {

	private final Logger logger = Logger.getLogger(SonarGadgetsActiveObjectsValidator.class);
	private final SonarGadgetsUtils gadgetsUtils;

	/**
	 * Constructor
	 *
	 * @param gadgetsUtils the {@link SonarGadgetsUtils} implementation
	 */
	public SonarGadgetsActiveObjectsValidator(SonarGadgetsUtils gadgetsUtils) {
		this.gadgetsUtils = checkNotNull(gadgetsUtils, "gadgetsUtils");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ModelVersion getModelVersion() {
		String rawVersion = PluginHelper.getPluginVersion().replaceAll("[^0-9]+", "");
		return ModelVersion.valueOf(rawVersion);
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void upgrade(ModelVersion currentVersion, ActiveObjects ao) {
		try {
			ao.count(Gadget.class);
		} catch (Exception e) {
			// Looks like the Gadgets table doesn't exist yet, so migrate the Interface using the EntiryManager of
			// ActiveObjects
			ao.migrate(Gadget.class);
		}
		if (gadgetsUtils.getGadgetIds().size() != ao.count(Gadget.class)) {
			logger.debug("We are missing some Gadgets in the Gadget table");
			for (String gadgetId : gadgetsUtils.getGadgetIds()) {
				int count = ao.count(Gadget.class, Query.select().where("SHORT_NAME = ?", gadgetId));
				if (count == 0) {
					logger.debug("Adding Gadget '" + gadgetId + "'");
					Gadget gadget = ao.create(Gadget.class, new DBParam("SHORT_NAME", gadgetId));
					gadget.save();
				}
			}
		}
	}

}
