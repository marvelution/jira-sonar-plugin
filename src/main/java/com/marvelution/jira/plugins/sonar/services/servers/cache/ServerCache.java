/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.services.servers.cache;

import com.marvelution.jira.plugins.sonar.services.servers.SonarServer;

import net.java.ao.Entity;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;

/**
 * {@link SonarServer} cache object
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 3.0.0
 */
public interface ServerCache extends Entity {

	/**
	 * Getter for the {@link SonarServer} the cache applies for
	 * 
	 * @return the {@link SonarServer}
	 */
	@NotNull
	SonarServer getServer();

	/**
	 * Setter for the {@link SonarServer}
	 * 
	 * @param server the {@link SonarServer} to set
	 */
	void setServer(SonarServer server);

	/**
	 * Getter for the Query {@link String}
	 * 
	 * @return the Query {@link String}
	 */
	@NotNull
	@StringLength(StringLength.MAX_LENGTH)
	String getQuery();

	/**
	 * Setter for the Query {@link String}
	 * 
	 * @param query the Query {@link String}
	 */
	void setQuery(String query);

	/**
	 * Getter for the last update time in milliseconds
	 * 
	 * @return the last update time in milliseconds
	 */
	long getLastUpdate();

	/**
	 * Setter for the last update time in milliseconds
	 * 
	 * @param time the last update time in milliseconds to set
	 */
	void setLastUpdate(long time);

	/**
	 * Getter for the cached response {@link String}
	 * 
	 * @return the cached response {@link String}
	 */
	@StringLength(StringLength.UNLIMITED)
	String getResponse();

	/**
	 * Setter for the cached response {@link String}
	 * 
	 * @param repsonse the cached response {@link String}
	 */
	void setResponse(String repsonse);

}
