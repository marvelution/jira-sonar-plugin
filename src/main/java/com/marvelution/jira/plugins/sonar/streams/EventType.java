/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.streams;

import org.sonar.wsclient.services.Event;

/**
 * Sonar {@link Event} type enumeration
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * 
 * @since 2.4.0
 */
public enum EventType {

	ALL(""), ALERT("Alert"), OTHER("Other"), PROFILE("Profile"), VERSION("Version");

	/**
	 * The category name of the Event Type
	 */
	public final String category;

	/**
	 * Constructor
	 *
	 * @param category the category name
	 */
	private EventType(String category) {
		this.category = category;
	}

	/**
	 * Get the {@link EventType} for the given {@link Event}
	 * 
	 * @param event the {@link Event} to get the {@link EventType} of
	 * @return the {@link EventType}
	 */
	public static EventType getEventTypeForEvent(Event event) {
		try {
			return valueOf(event.getCategory().toUpperCase());
		} catch (Exception e) {
			return OTHER;
		}
	}

}
