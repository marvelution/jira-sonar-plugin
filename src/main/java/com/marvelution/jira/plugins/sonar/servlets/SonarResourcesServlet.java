/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.servlets;

import com.marvelution.gadgets.sonar.servlet.SonarMakeRequestServlet;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServer;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServerManager;

import javax.servlet.http.HttpServletRequest;
import java.net.URISyntaxException;

import org.sonar.wsclient.Host;

/**
 * Custom {@link SonarMakeRequestServlet} that is able to get a Sonar server using the {link SonarServerManager}
 *
 * @author Mark Rekveld
 * @since 3.2.3
 */
public class SonarResourcesServlet extends SonarMakeRequestServlet {

	private final SonarServerManager serverManager;

	/**
	 * Constructor
	 *
	 * @param serverManager the {@link SonarServerManager} implementation
	 */
	public SonarResourcesServlet(SonarServerManager serverManager) {
		this.serverManager = serverManager;
	}

	@Override
	protected Host getSonarHostFromRequest(HttpServletRequest req) throws URISyntaxException {
		if (req.getParameter("serverId") != null) {
			SonarServer server = serverManager.getServer(Integer.valueOf(req.getParameter("serverId")));
			if (server != null) {
				return new Host(server.getHost(), server.getUsername(), server.getPassword());
			}
		}
		return super.getSonarHostFromRequest(req);
	}

}
