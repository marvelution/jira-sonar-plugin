/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.web.action.admin;

import org.apache.log4j.Logger;
import org.apache.velocity.tools.generic.SortTool;

import com.marvelution.jira.plugins.sonar.services.servers.SonarServerManager;
import com.marvelution.jira.plugins.sonar.web.action.SonarWebActionSupport;

/**
 * Abstract {@link SonarWebActionSupport} implementation for the Administrator actions of the Sonar plugin
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public abstract class AbstractSonarAdminWebActionSupport extends SonarWebActionSupport {

	private static final long serialVersionUID = 1L;
	
	private SortTool sorter;
	
	protected static final Logger LOGGER = Logger.getLogger(AbstractSonarAdminWebActionSupport.class);
	protected static final String ADMINISTER_SERVERS = "AdministerSonarServers.jspa";
	protected static final String ADMINISTER_ASSOCIATIONS= "AdministerSonarAssociations.jspa?context=";

	protected final SonarServerManager serverManager;

	/**
	 * Constructor
	 * 
	 * @param serverManager the {@link SonarServerManager} implementation
	 */
	protected AbstractSonarAdminWebActionSupport(SonarServerManager serverManager) {
		this.serverManager = serverManager;
		this.sorter = new SortTool();
	}

	/**
	 * Getter for the {@link SortTool}
	 * 
	 * @return the {@link SortTool}
	 */
	public SortTool getSorter() {
		return sorter;
	}

	/**
	 * Getter for the {@link SonarServerManager}
	 * 
	 * @return the {@link SonarServerManager} implementation
	 */
	public SonarServerManager getServerManager() {
		return serverManager;
	}

}
