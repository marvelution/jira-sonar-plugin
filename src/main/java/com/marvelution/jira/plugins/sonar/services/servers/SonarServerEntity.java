/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.services.servers;

import org.apache.log4j.Logger;
import org.sonar.wsclient.Host;

import com.marvelution.security.crypto.SimpleStringEncryptor;
import com.marvelution.security.crypto.StringEncryptor;

/**
 * Entity implementation for the {@link SonarServer} entity
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 1.0.0
 */
public class SonarServerEntity {

	private static final Logger LOGGER = Logger.getLogger(SonarServerEntity.class);
	private static final StringEncryptor ENCRYPTOR = new SimpleStringEncryptor("fraxugenATabEy46RethUQabacrEBa");

	private SonarServer server;

	/**
	 * Constructor
	 *
	 * @param server the {@link SonarServer} entity
	 */
	public SonarServerEntity(SonarServer server) {
		this.server = server;
	}

	/**
	 * Decrypt the password from the database
	 * 
	 * @return the decrypted password
	 * @see com.marvelution.jira.plugins.sonar.services.servers.SonarServer#getPassword()
	 */
	public String getPassword() {
		LOGGER.debug("Decrypting password for user " + server.getUsername());
		try {
			return ENCRYPTOR.decrypt(server.getPassword());
		} catch (Exception e) {
			LOGGER.error("Failed to decrypt the password, returning the original string", e);
			return server.getPassword();
		}
	}

	/**
	 * Encrypt the password before storing it in the database
	 * 
	 * @param password the password to encrypt
	 * @see com.marvelution.jira.plugins.sonar.services.servers.SonarServer#setPassword(java.lang.String)
	 */
	public void setPassword(String password) {
		LOGGER.debug("Encrypting password for user " + server.getUsername());
		server.setPassword(ENCRYPTOR.encrypt(password));
	}

	/**
	 * Getter for the Sonar {@link Host}
	 * 
	 * @return the Sonar {@link Host}
	 * @see com.marvelution.jira.plugins.sonar.services.servers.SonarServer#getSonarHost()
	 * @since 3.0.0
	 */
	public Host getSonarHost() {
		return new Host(server.getHost(), server.getUsername(), getPassword());
	}

}
