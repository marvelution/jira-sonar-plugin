/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.web.action.admin;

import org.apache.log4j.Logger;

import com.google.common.collect.Iterables;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociation;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociationManager;
import com.marvelution.jira.plugins.sonar.services.layout.Column;
import com.marvelution.jira.plugins.sonar.services.layout.Layout;
import com.marvelution.jira.plugins.sonar.services.layout.SonarPanelLayoutManager;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServerManager;

/**
 * WebAction to view and manage the Sonar Panel Layout
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class AdministerSonarLayout extends AbstractSonarProjectAdminWebActionSupport {

	private static final long serialVersionUID = 1L;
	private static final String LAYOUT_PAGE = "AdministerSonarLayout.jspa?context=";
	private static final Logger LOGGER = Logger.getLogger(AdministerSonarLayout.class);

	private final SonarAssociationManager associationManager;
	private final SonarPanelLayoutManager layoutManager;

	/**
	 * Constructor
	 * 
	 * @param serverManager the {@link SonarServerManager} implementation
	 * @param associationManager the {@link SonarAssociationManager} implementation
	 * @param layoutManager the {@link SonarPanelLayoutManager} implementation
	 */
	public AdministerSonarLayout(SonarServerManager serverManager, SonarAssociationManager associationManager,
									SonarPanelLayoutManager layoutManager) {
		super(serverManager);
		this.associationManager = associationManager;
		this.layoutManager = layoutManager;
	}

	/**
	 * Get the sample {@link SonarAssociation} for the panel layout view
	 * 
	 * @return the default {@link SonarAssociation}, may be <code>null</code>
	 */
	public SonarAssociation getAssociation() {
		if (associationManager.hasAssociations()) {
			if (getContext() > 0L && getProject() != null) {
				return Iterables.get(associationManager.getAssociations(getProject()), 0);
			} else {
				return Iterables.get(associationManager.getAssociations(), 0);
			}
		} else {
			return null;
		}
	}

	/**
	 * Action method to copy the system layout for a specific context
	 * 
	 * @return redirect url to the layout page
	 * @throws Exception in case of errors
	 */
	public String doCopy() throws Exception {
		if (getProject() != null) {
			LOGGER.debug("Going to copy the System Layout for project " + getProject().getKey());
			Layout systemLayout = layoutManager.getSystemLayout();
			Layout newLayout = layoutManager.createLayout(systemLayout.getType(), getProject().getId());
			for (int cIndex = 0; cIndex < systemLayout.getColumns().length; cIndex++) {
				Column column = systemLayout.getColumns()[cIndex];
				for (int gIndex = 0; gIndex < column.getGadgets().length; gIndex++) {
					layoutManager.addGadgetToColumn(column.getGadgets()[gIndex], newLayout.getColumns()[cIndex]);
				}
			}
		}
		return getRedirect(LAYOUT_PAGE + getContext());
	}

	/**
	 * Action method to create layout for a specific context
	 * 
	 * @return redirect url to the layout page
	 * @throws Exception in case of errors
	 */
	public String doCreate() throws Exception {
		if (getProject() != null) {
			LOGGER.debug("Creating a new AA Layout for project " + getProject().getKey());
			layoutManager.createLayout(Layout.Type.AA, getProject().getId());
		}
		return getRedirect(LAYOUT_PAGE + getContext());
	}

	/**
	 * Action method to delete a layout for a context
	 * 
	 * @return redirect url to the layout page
	 * @throws Exception in case of errors
	 */
	public String doDelete() throws Exception {
		Layout layout = null;
		if (getProject() != null && (layout = layoutManager.getLayout(getContext())) != null) {
			LOGGER.debug("Deleting Layout for project: " + getProject().getName());
			layoutManager.removeLayout(layout);
		}
		return getRedirect(LAYOUT_PAGE + getContext());
	}

}
