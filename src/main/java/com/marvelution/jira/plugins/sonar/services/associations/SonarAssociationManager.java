/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.services.associations;

import java.util.Collection;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.project.Project;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServer;

/**
 * {@link SonarAssociation} manager implementation
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public interface SonarAssociationManager {

	/**
	 * Check if there is a {@link SonarAssociation} configured with the given Id
	 * 
	 * @param associationId the Id to check
	 * @return <code>true</code> if configured, <code>false</code> otherwise
	 */
	boolean hasAssociation(int associationId);

	/**
	 * Check if any {@link SonarAssociation}s are configured
	 * 
	 * @return <code>true</code> if there are any, <code>false</code> otherwise
	 */
	boolean hasAssociations();

	/**
	 * Check if there are any {@link SonarAssociation}s for the given {@link Project}
	 * 
	 * @param project the {@link Project} to check
	 * @return <code>true</code> if there are any associations, <code>false</code> otherwise
	 */
	boolean hasAssociations(Project project);

	/**
	 * Check if there are any {@link SonarAssociation}s for the given {@link Project}
	 * 
	 * @param project the {@link Project} to check
	 * @param component the {@link ProjectComponent}
	 * @return <code>true</code> if there are any associations, <code>false</code> otherwise
	 */
	boolean hasAssociations(Project project, ProjectComponent component);

	/**
	 * Getter for a specific {@link SonarAssociation} by its Id
	 * 
	 * @param associationId the {@link SonarAssociation} Id to get
	 * @return the {@link SonarAssociation}
	 */
	SonarAssociation getAssociation(int associationId);

	/**
	 * Getter for all the {@link SonarAssociation} objects in a unmodifiable {@link Collection}
	 * 
	 * @return the unmodifiable {@link Collection}
	 */
	Collection<SonarAssociation> getAssociations();

	/**
	 * Get all the {@link SonarAssociation} objects for the given {@link Project}
	 * 
	 * @param project the {@link Project} to get all the {@link SonarAssociation} for
	 * @return the {@link Collection} with all {@link SonarAssociation} associated with the {@link Project}
	 */
	Collection<SonarAssociation> getAssociations(Project project);

	/**
	 * Get all the {@link SonarAssociation} objects for the given {@link Project}
	 * 
	 * @param project the {@link Project} to get all the {@link SonarAssociation} for
	 * @param component the {@link ProjectComponent}
	 * @return the {@link Collection} with all {@link SonarAssociation} associated with the {@link Project}
	 */
	Collection<SonarAssociation> getAssociations(Project project, ProjectComponent component);

	/**
	 * Add a {@link SonarAssociation}
	 * 
	 * @param server the {@link SonarServer}
	 * @param projectId the JIRA {@link Project} ID
	 * @param componentId the JIRA {@link ProjectComponent} ID
	 * @param resourceName the Sonar resource Name
	 * @return the new {@link SonarAssociation}
	 */
	SonarAssociation addAssociation(SonarServer server, long projectId, long componentId, String resourceName);

	/**
	 * Add a {@link SonarAssociation}
	 * 
	 * @param serverId the Sonar ServerId
	 * @param projectId the JIRA {@link Project} ID
	 * @param componentId the JIRA {@link ProjectComponent} ID
	 * @param resourceName the Sonar resource Name
	 * @return the new {@link SonarAssociation}
	 */
	SonarAssociation addAssociation(int serverId, long projectId, long componentId, String resourceName);

	/**
	 * Copy a {@link SonarAssociation} to the collection
	 * 
	 * @param association the {@link SonarAssociation} to copy
	 * @return the new {@link SonarAssociation}
	 */
	SonarAssociation addAssociation(SonarAssociation association);

	/**
	 * Update a {@link SonarAssociation}
	 * 
	 * @param associationId the Sonar AssociationId
	 * @param server the {@link SonarServer}
	 * @param projectId the JIRA {@link Project} ID
	 * @param componentId the JIRA {@link ProjectComponent} ID
	 * @param resourceName the Sonar resource Name
	 * @return the updated {@link SonarAssociation}
	 */
	SonarAssociation updateAssociation(int associationId, SonarServer server, long projectId, long componentId,
					String resourceName);

	/**
	 * Update a {@link SonarAssociation}
	 * 
	 * @param associationId the Sonar AssociationId
	 * @param serverId the Sonar ServerId
	 * @param projectId the JIRA {@link Project} ID
	 * @param componentId the JIRA {@link ProjectComponent} ID
	 * @param resourceName the Sonar Resource name
	 * @return the updated {@link SonarAssociation}
	 */
	SonarAssociation updateAssociation(int associationId, int serverId, long projectId, long componentId,
					String resourceName);

	/**
	 * Update a {@link SonarAssociation}
	 * 
	 * @param association the {@link SonarAssociation} to update
	 * @return the updated {@link SonarAssociation}
	 */
	SonarAssociation updateAssociation(SonarAssociation association);

	/**
	 * Remove a {@link SonarAssociation} from the collection by Id
	 * 
	 * @param associationId the {@link SonarAssociation} Id to remove
	 * @see #removeAssociation(SonarAssociation)
	 */
	void removeAssociation(int associationId);

	/**
	 * Remove a {@link SonarAssociation} from the collection
	 * 
	 * @param association the {@link SonarAssociation} to remove
	 */
	void removeAssociation(SonarAssociation association);

}
