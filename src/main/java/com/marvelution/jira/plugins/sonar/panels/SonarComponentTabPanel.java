/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.panels;

import java.util.Map;

import com.atlassian.jira.plugin.componentpanel.BrowseComponentContext;
import com.atlassian.jira.plugin.componentpanel.impl.GenericTabPanel;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.FieldVisibilityManager;

/**
 * Tabpanel to show Sonar data
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class SonarComponentTabPanel extends GenericTabPanel {

	private final SonarTabPanelHelper panelHelper;

	/**
	 * Constructor
	 * 
	 * @param authenticationContext the {@link JiraAuthenticationContext} implementation
	 * @param projectManager the {@link ProjectManager} implementation
	 * @param panelHelper the {@link SonarTabPanelHelper} implementation
	 */
	public SonarComponentTabPanel(JiraAuthenticationContext authenticationContext, ProjectManager projectManager,
									FieldVisibilityManager fieldVisibilityManager, SonarTabPanelHelper panelHelper) {
		super(projectManager, authenticationContext, fieldVisibilityManager);
		this.panelHelper = panelHelper;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Map<String, Object> createVelocityParams(BrowseComponentContext context) {
		return panelHelper.prepareVelocityParameters(context.getProject(), context.getComponent(), context.getUser(),
			super.createVelocityParams(context), descriptor);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean showPanel(BrowseComponentContext context) {
		return panelHelper.showPanel(context.getProject(), context.getComponent(), context.getUser(), descriptor);
	}

}
