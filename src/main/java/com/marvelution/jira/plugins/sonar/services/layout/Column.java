/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.services.layout;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

import net.java.ao.ManyToMany;
import net.java.ao.Entity;
import net.java.ao.schema.NotNull;

/**
 * The Column Active Object class
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * 
 * @since 2.4.0
 */
public interface Column extends Entity {

	/**
	 * Getter of the {@link Type} of column
	 * 
	 * @return the {@link Type} of the column
	 */
	@NotNull
	Type getType();

	/**
	 * Setter of the {@link Type} of column
	 * 
	 * @param type the {@link Type} to set
	 */
	void setType(Type type);

	/**
	 * Getter of the {@link Layout}
	 * 
	 * @return the {@link Layout}
	 */
	@NotNull
	Layout getLayout();

	/**
	 * Setter of the {@link Layout}
	 * 
	 * @param layout the {@link Layout} to set
	 */
	void setLayout(Layout layout);

	/**
	 * Getter of the Gadgets linked to the column
	 * 
	 * @return the Array of {@link Gadget}s
	 */
	@ManyToMany(value = ColumnToGadget.class)
	Gadget[] getGadgets();

	/**
	 * Column Type definitions
	 * 
	 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
	 */
	@XmlEnum(String.class)
	@XmlType(namespace = "column")
	public enum Type {
		A, B;
	}

}
