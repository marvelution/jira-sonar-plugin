/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;
import org.sonar.wsclient.Sonar;
import org.sonar.wsclient.services.Metric;
import org.sonar.wsclient.services.MetricQuery;
import org.sonar.wsclient.services.Resource;
import org.sonar.wsclient.services.ResourceQuery;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.marvelution.gadgets.sonar.wsclient.services.Project;
import com.marvelution.gadgets.sonar.wsclient.services.ProjectQuery;
import com.marvelution.gadgets.sonar.wsclient.unmarshallers.ProjectUnmarshaller;
import com.marvelution.gadgets.sonar.wsclient.utils.MetricUtils;
import com.marvelution.jira.plugins.sonar.rest.exceptions.InvalidAssociationException;
import com.marvelution.jira.plugins.sonar.rest.model.Metrics;
import com.marvelution.jira.plugins.sonar.rest.model.Versions;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociation;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociationManager;
import com.marvelution.jira.plugins.sonar.services.servers.SonarClientFactory;

/**
 * REST API for Sonar Project Associations
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
@Path("/association")
@AnonymousAllowed
@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_XML })
public class SonarAssociationRestResource {

	private final SonarAssociationManager associationManager;
	private final SonarClientFactory clientFactory;

	/**
	 * Constructor
	 * 
	 * @param associationManager the {@link SonarAssociationManager} implementation
	 * @param clientFactory the {@link SonarClientFactory} implementation
	 */
	public SonarAssociationRestResource(SonarAssociationManager associationManager, SonarClientFactory clientFactory) {
		this.associationManager = associationManager;
		this.clientFactory = clientFactory;
	}

	/**
	 * Get the Sonar project name from the Sonar Server
	 * 
	 * @param associationId the Association Id to get the project name for
	 * @return the project name in a {@link Response} object
	 */
	@GET
	@Path("{associationId}/project/name")
	@Produces(MediaType.TEXT_PLAIN)
	public Response getSonarProjectName(@PathParam("associationId") Integer associationId) {
		final SonarAssociation association = associationManager.getAssociation(associationId);
		String resourceName = "UNKNOWN";
		if (association != null) {
			try {
				final Resource resource = clientFactory.create(association.getSonarServer()).find(
						new ResourceQuery(association.getSonarProject()));
				if (StringUtils.isNotBlank(resource.getName())) {
					resourceName = resource.getName();
				} else {
					resourceName = association.getSonarProject();
				}
			} catch (Exception e) {
				// Ignore this exception
				resourceName = association.getSonarProject();
			}
		}
		return Response.ok(resourceName).build();
	}

	/**
	 * Get the Sonar project versions from the Sonar Server
	 * 
	 * @param associationId the Association Id to get the project versions for
	 * @return the project versions
	 */
	@GET
	@Path("{associationId}/project/versions")
	@AnonymousAllowed
	public Versions getSonarProjectVersions(@PathParam("associationId") Integer associationId) {
		if (associationManager.hasAssociation(associationId)) {
			SonarAssociation association = associationManager.getAssociation(associationId);
			Sonar sonar = clientFactory.create(association.getSonarServer());
			String json = sonar.getConnector().execute(new ProjectQuery(association.getSonarProject()).setVersions(true));
			if (StringUtils.isNotBlank(json)) {
				Project project = new ProjectUnmarshaller().toModel(json);
				return new Versions(project.getVersions());
			} else {
				return new Versions();
			}
		} else {
			throw new InvalidAssociationException("No Association with id " + associationId);
		}
	}

	/**
	 * Get the Sonar metrics from the Sonar Server
	 * 
	 * @param associationId the Association Id to get the metrics for
	 * @return the metrics
	 */
	@GET
	@Path("{associationId}/server/metrics")
	@AnonymousAllowed
	public Metrics getSonarMetrics(@PathParam("associationId") Integer associationId) {
		if (associationManager.hasAssociation(associationId)) {
			SonarAssociation association = associationManager.getAssociation(associationId);
			Sonar sonar = clientFactory.create(association.getSonarServer());
			return new Metrics(Collections2.filter(sonar.findAll(MetricQuery.all()), new Predicate<Metric>() {
				@Override
				public boolean apply(Metric input) {
					return MetricUtils.isTimeMachineMetric(input);
				}
			}));
		} else {
			throw new InvalidAssociationException("No Association with id " + associationId);
		}
	}

}
