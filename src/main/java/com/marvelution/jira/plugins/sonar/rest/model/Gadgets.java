/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.rest.model;

import java.util.Collection;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.common.collect.Lists;
import com.marvelution.jira.plugins.sonar.services.layout.Gadget;

/**
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 2.4.0
 */
@XmlRootElement
public class Gadgets {

	@XmlElement
	private Collection<String> gadget;

	/**
	 * Default Constructor
	 */
	public Gadgets() {
		this.gadget = Lists.newArrayList();
	}

	/**
	 * Constructor
	 *
	 * @param gadgets array of {@link Gadget} objects to add to the collection
	 */
	public Gadgets(Gadget[] gadgets) {
		this();
		for (Gadget gadget : gadgets) {
			this.gadget.add(gadget.getShortName());
		}
	}

	/**
	 * Getter for gadget
	 *
	 * @return the gadget
	 */
	public Collection<String> getGadget() {
		return gadget;
	}

}
