/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.predicates;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.sonar.wsclient.Sonar;
import org.sonar.wsclient.services.MetricQuery;
import org.sonar.wsclient.services.Server;
import org.sonar.wsclient.services.ServerQuery;

import com.atlassian.jira.component.ComponentAccessor;
import com.google.common.base.Predicate;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociation;
import com.marvelution.jira.plugins.sonar.services.servers.SonarClientFactory;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServer;

/**
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 3.0.0
 */
public class SonarPredicates {

	private static final Logger LOGGER = Logger.getLogger(SonarPredicates.class);
	private static final Pattern VERSION_PATTERN = Pattern.compile("^(\\d+)\\.(\\d+)");
	private static SonarClientFactory clientFactory;

	public static Predicate<SonarServer> isServerPriorToVersion(Integer maxMajorVersion, Integer maxMinorVersion) {
		return new SonarServerVersionPredicate(maxMajorVersion, maxMinorVersion);
	}

	public static Predicate<SonarAssociation> isAssociationServerPriorToVersion(final Integer maxMajorVersion,
			final Integer maxMinorVersion) {
		return new Predicate<SonarAssociation>() {
			@Override
			public boolean apply(SonarAssociation input) {
				return new SonarServerVersionPredicate(maxMajorVersion, maxMinorVersion).apply(input.getSonarServer());
			}
		};
	}

	public static Predicate<SonarServer> serverHasMetric(String metricKey) {
		return new SonarServerMetricPredicate(metricKey);
	}

	public static Predicate<SonarAssociation> associationServerHasMetric(final String metricKey) {
		return new Predicate<SonarAssociation>() {
			@Override
			public boolean apply(SonarAssociation input) {
				return new SonarServerMetricPredicate(metricKey).apply(input.getSonarServer());
			}
		};
	}

	/**
	 * Getter for the {@link SonarClientFactory} implementation
	 * 
	 * @return the {@link SonarClientFactory} implementation
	 */
	/*package*/ static SonarClientFactory getClientFactory() { 
		if (clientFactory == null) {
			clientFactory = ComponentAccessor.getOSGiComponentInstanceOfType(SonarClientFactory.class);
		}
		return clientFactory;
	}

	public static class SonarServerVersionPredicate implements Predicate<SonarServer> {

		private final Integer maxMajorVersion;
		private final Integer maxMinorVersion;

		/**
		 * Constructor
		 *
		 * @param maxMajorVersion the maximum major version
		 * @param maxMinorVersion the maximum minor version
		 */
		public SonarServerVersionPredicate(Integer maxMajorVersion, Integer maxMinorVersion) {
			this.maxMajorVersion = maxMajorVersion;
			this.maxMinorVersion = maxMinorVersion;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean apply(SonarServer input) {
			try {
				Sonar sonar = getClientFactory().create(input);
				Server server = sonar.find(new ServerQuery());
				Matcher versionMatcher = VERSION_PATTERN.matcher(server.getVersion());
				versionMatcher.find();
				Integer majorVersion = Integer.decode(versionMatcher.group(1));
				Integer minorVersion = Integer.decode(versionMatcher.group(2));
				return (majorVersion.intValue() < maxMajorVersion.intValue()) || ((majorVersion == maxMajorVersion)
					&& (minorVersion.intValue() < maxMinorVersion.intValue()));
			} catch (Exception e) {
				LOGGER.debug(e);
				return false;
			}
		}

	}

	public static class SonarServerMetricPredicate implements Predicate<SonarServer> {

		private final String metricKey;

		/**
		 * Constructor
		 *
		 * @param metricKey the metric key to check
		 */
		public SonarServerMetricPredicate(String metricKey) {
			this.metricKey = metricKey;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean apply(SonarServer input) {
			try {
				Sonar sonar = getClientFactory().create(input);
				return sonar.find(MetricQuery.byKey(metricKey)) != null;
			} catch (Exception e) {
				LOGGER.debug(e);
				return false;
			}
		}

	}

}
