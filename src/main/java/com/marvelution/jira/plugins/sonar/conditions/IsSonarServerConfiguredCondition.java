/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.conditions;

import java.util.Map;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServerManager;

/**
 * {@link Condition} web-items depending if a Sonar Server is configured
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class IsSonarServerConfiguredCondition implements Condition {

	private final SonarServerManager serverManager;

	/**
	 * Constructor
	 * 
	 * @param serverManager the {@link SonarServerManager} implementation
	 */
	public IsSonarServerConfiguredCondition(SonarServerManager serverManager) {
		this.serverManager = serverManager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void init(Map<String, String> params) throws PluginParseException {
		// Not needed for this Condition
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean shouldDisplay(Map<String, Object> context) {
		return serverManager.hasServers();
	}

}
