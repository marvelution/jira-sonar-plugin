/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URI;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.atlassian.gadgets.GadgetId;
import com.atlassian.gadgets.GadgetRequestContext;
import com.atlassian.gadgets.GadgetRequestContextFactory;
import com.atlassian.gadgets.GadgetState;
import com.atlassian.gadgets.view.GadgetViewFactory;
import com.atlassian.gadgets.view.ModuleId;
import com.atlassian.gadgets.view.View;
import com.atlassian.gadgets.view.ViewType;
import com.atlassian.jira.util.collect.MapBuilder;
import com.marvelution.gadgets.sonar.utils.SonarGadgetsUtils;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociation;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServerUtils;

/**
 * Utility class for Sonar Gadgets
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class SonarGadgetUtils {

	/**
	 * isConfigured gadget preference field name
	 */
	public static final String PREF_IS_CONFIGURED = "isConfigured";

	/**
	 * titleRequired gadget preference field name
	 */
	public static final String PREF_TITLE_REQUIRED = "titleRequired";

	/**
	 * isConfigurable gadget preference field name
	 */
	public static final String PREFS_IS_CONFIGURABLE = "isConfigurable";

	/**
	 * refresh gadget preference field name
	 */
	public static final String PREFS_REFRESH = "refresh";

	/**
	 * sonarServer gadget reference field name
	 */
	public static final String PREF_SONAR_SERVER = "sonarServer";

	/**
	 * sonarProject gadget preference field name
	 */
	public static final String PREF_SONAR_PROJECT = "sonarProject";

	/**
	 * Gadget URL prefix
	 */
	public static final String GADGET_URI_PREFIX = "rest/gadgets/1.0/g/" + PluginHelper.getPluginKey()
		+ "/gadgets/sonar-";

	/**
	 * Gadget URL suffix
	 */
	public static final String GADGET_URI_SUFFIX = "-gadget.xml";

	private final Logger logger = Logger.getLogger(SonarGadgetUtils.class);

	private final GadgetRequestContextFactory gadgetRequestContextFactory;

	private final GadgetViewFactory gadgetViewFactory;

	private final SonarGadgetsUtils sonarGadgetsUtils;

	/**
	 * Constructor
	 * 
	 * @param gadgetRequestContextFactory the {@link GadgetRequestContextFactory} implementation
	 * @param gadgetViewFactory the {@link GadgetViewFactory} implementation
	 * @param sonarGadgetsUtils the {@link SonarGadgetsUtils} implementation
	 */
	public SonarGadgetUtils(GadgetRequestContextFactory gadgetRequestContextFactory,
							GadgetViewFactory gadgetViewFactory, SonarGadgetsUtils sonarGadgetsUtils) {
		this.gadgetRequestContextFactory = gadgetRequestContextFactory;
		this.gadgetViewFactory = gadgetViewFactory;
		this.sonarGadgetsUtils = sonarGadgetsUtils;
	}

	/**
	 * Create the Gadget preferences in a {@link MapBuilder} object
	 * 
	 * @param association the {@link SonarAssociation} to create the preferences for
	 * @return the {@link MapBuilder} containing the Gadget preferences
	 * @see SonarGadgetUtils#getGadgetPreferences(String, String)
	 */
	public MapBuilder<String, String> createGadgetPreferences(SonarAssociation association) {
		return createGadgetPreferences(SonarServerUtils.getSonarServerGadgetUrl(association.getSonarServer()),
			association.getSonarProject());
	}

	/**
	 * Create the Gadget preferences for the given Sonar Server Url and Sonar Project key
	 * 
	 * @param sonarServer the Sonar Server Url
	 * @param sonarProject the Sonar Project Key
	 * @return the {@link MapBuilder} containing the Gadget preferences
	 */
	public MapBuilder<String, String> createGadgetPreferences(String sonarServer, String sonarProject) {
		final MapBuilder<String, String> prefsBuilder = MapBuilder.newBuilder();
		prefsBuilder.add(PREF_IS_CONFIGURED, Boolean.TRUE.toString());
		prefsBuilder.add(PREFS_IS_CONFIGURABLE, Boolean.FALSE.toString());
		prefsBuilder.add(PREFS_REFRESH, Boolean.FALSE.toString());
		prefsBuilder.add(PREF_TITLE_REQUIRED, Boolean.FALSE.toString());
		prefsBuilder.add(PREF_SONAR_SERVER, sonarServer);
		prefsBuilder.add(PREF_SONAR_PROJECT, sonarProject);
		return prefsBuilder;
	}

	/**
	 * Generate the HTML to view a gadget on a page
	 * 
	 * @param gadgetId the id of the gadget
	 * @param prefsBuilder the {@link MapBuilder} configured with all the gadget preferences
	 * @param request the {@link HttpServletRequest} to get the {@link GadgetRequestContext} from
	 * @return the HTML {@link String}
	 */
	public String generateGadgetHtml(String gadgetId, MapBuilder<String, String> prefsBuilder,
					HttpServletRequest request) {
		final GadgetState gadget = GadgetState.gadget(getGadgetId(gadgetId))
				.specUri(URI.create(GADGET_URI_PREFIX + gadgetId + GADGET_URI_SUFFIX))
				.userPrefs(prefsBuilder.toMap()).build();
        try {
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			final Writer gadgetWriter = new OutputStreamWriter(baos);
			final GadgetRequestContext requestContext = gadgetRequestContextFactory.get(request);
			final View settings = new View.Builder().viewType(ViewType.DEFAULT).writable(false).build();
			gadgetViewFactory.createGadgetView(gadget, getModuleId(gadgetId), settings, requestContext)
				.writeTo(gadgetWriter);
			gadgetWriter.flush();
			return baos.toString();
		} catch (IOException e) {
			logger.error("Error rendering Sonar '" + gadgetId + "' gadget.", e);
		} catch (RuntimeException e) {
			logger.error("Runtime error rendering Sonar '" + gadgetId + "' gadget.", e);
		}
		return "";
	}

	/**
	 * Get the {@link GadgetId} object from a given gadgetId {@link String}
	 * 
	 * @param gadgetId the gadget Id {@link String}
	 * @return the {@link GadgetId}
	 * @since 2.2.0
	 */
	private GadgetId getGadgetId(String gadgetId) {
		return GadgetId.valueOf(String.valueOf(gadgetId.hashCode()));
	}

	/**
	 * Get the {@link ModuleId} object from a given gadgetId {@link String}
	 * 
	 * @param gadgetId the gadget Id {@link String}
	 * @return the {@link ModuleId}
	 * @since 2.2.0
	 */
	private ModuleId getModuleId(String gadgetId) {
		return ModuleId.valueOf(gadgetId.hashCode());
	}

	/**
	 * Get all the Sonar gadget Ids
	 * 
	 * @return {@link Collection} with all the Sonar Gadget Ids
	 */
	public Collection<String> getGadgetIds() {
		return sonarGadgetsUtils.getGadgetIds();
	}

}
