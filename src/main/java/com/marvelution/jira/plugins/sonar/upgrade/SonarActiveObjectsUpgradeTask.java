/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.upgrade;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociation;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociationManager;
import com.marvelution.jira.plugins.sonar.services.layout.Column;
import com.marvelution.jira.plugins.sonar.services.layout.Gadget;
import com.marvelution.jira.plugins.sonar.services.layout.Layout;
import com.marvelution.jira.plugins.sonar.services.layout.SonarPanelLayoutManager;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServer;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServerManager;
import com.marvelution.jira.plugins.sonar.utils.PluginHelper;
import com.opensymphony.module.propertyset.PropertyException;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.module.propertyset.PropertySetManager;

/**
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * 
 * @since 2.4.0
 */
public class SonarActiveObjectsUpgradeTask implements PluginUpgradeTask {

	// Server/Association related
	private static final String CONFIG_SONAR_PREFIX = "sonar.association.";
	private static final String CONFIG_SONAR_ASSOCIATION_ID_SUFFIX = ".associationId";
	private static final String CONFIG_SONAR_PROJECT_ID_SUFFIX = ".projectId";
	private static final String CONFIG_SONAR_COMPONENT_ID_SUFFIX = ".componentId";
	private static final String CONFIG_SONAR_SERVER_SUFFIX = ".server";
	private static final String CONFIG_SONAR_PROJECT_KEY_SUFFIX = ".projectKey";

	// Layout related
	private static final String CONFIG_LAYOUT_PREFIX = "sonar.layout.";
	private static final String CONFIG_LAYOUT_GADGETS_SUFFIX = ".gadgetIds";

	private static final Long PROPERTIES_ID = 3L;

	private final Logger logger = Logger.getLogger(SonarActiveObjectsUpgradeTask.class);
	private final SonarServerManager serverManager;
	private final SonarAssociationManager associationManager;
	private final SonarPanelLayoutManager layoutManager;

	private PropertySet propertySet;

	/**
	 * Constructor
	 *
	 * @param serverManager the {@link SonarServerManager} implementation
	 * @param associationManager the {@link SonarAssociationManager} implementation
	 * @param layoutManager the {@link SonarPanelLayoutManager} implementation
	 */
	public SonarActiveObjectsUpgradeTask(SonarServerManager serverManager,
					SonarAssociationManager associationManager, SonarPanelLayoutManager layoutManager) {
		this.serverManager = Preconditions.checkNotNull(serverManager, "serverManager");
		this.associationManager = Preconditions.checkNotNull(associationManager, "associationManager");
		this.layoutManager = Preconditions.checkNotNull(layoutManager, "layoutManager");
		this.propertySet = loadPropertySet();
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Message> doUpgrade() throws Exception {
		final Collection<String> associationKeys = propertySet.getKeys(CONFIG_SONAR_PREFIX);
		logger.info("Upgrading all preconfigured Sonar Associations to the new active-objects database");
		for (String key : associationKeys) {
			if (key.endsWith(CONFIG_SONAR_ASSOCIATION_ID_SUFFIX)) {
				long associationId = propertySet.getLong(key);
				SonarAssociation association = migrateAssociation(associationId);
				logger.info("Upgraded old association with id " + associationId + " to " + association.getID()
					+ " with server " + association.getSonarServer().getID());
			}
		}
		for (String key : associationKeys) {
			if (propertySet.exists(key)) {
				logger.info("Removing association PropertySet key: " + key);
				propertySet.remove(key);
			}
		}
		logger.info("Upgrading the old Sonar Panel Layout to the new format");
		final Collection<String> layoutKeys = propertySet.getKeys(CONFIG_LAYOUT_PREFIX);
		List<String[]> columns = Lists.newArrayList();
		for (String key : layoutKeys) {
			if (key.endsWith(CONFIG_LAYOUT_GADGETS_SUFFIX)) {
				columns.add(StringUtils.split(propertySet.getString(key), ','));
			}
		}
		if (columns != null && columns.size() > 0) {
			Layout layout = layoutManager.getSystemLayout();
			for (int index = 0; index < layout.getColumns().length; index++) {
				if (index >= columns.size()) {
					break;
				} else {
					Column column = layout.getColumns()[index];
					layoutManager.removeAllGadgetsFromColumn(column);
					for (String gadgetShortName : columns.get(index)) {
						Gadget gadget = layoutManager.getGadget(gadgetShortName);
						if (gadget != null) {
							layoutManager.addGadgetToColumn(gadget, column);
						} else {
							logger.warn("Unable to add gadget '" + gadgetShortName + "' to the system layout.");
						}
					}
				}
			}
		}
		for (String key : layoutKeys) {
			if (propertySet.exists(key)) {
				logger.info("Removing layout PropertySet key: " + key);
				propertySet.remove(key);
			}
		}
		return null;
	}

	/**
	 * Migrate a {@link SonarAssociation} to the Active-Objects model
	 * 
	 * @param associationId the old association Id
	 * @return the new {@link SonarAssociation}
	 * @throws URISyntaxException 
	 * @throws PropertyException 
	 */
	private SonarAssociation migrateAssociation(long associationId) throws PropertyException, URISyntaxException {
		long projectId = propertySet.getLong(getProjectIdPropertyKey(associationId));
		long componentId = propertySet.getLong(getComponentIdPropertyKey(associationId));
		String resource = propertySet.getString(getSonarProjectPropertyKey(associationId));
		SonarServer server = getSonarServerFromString(propertySet.getString(getSonarServerPropertyKey(associationId)));
		return associationManager.addAssociation(server, projectId, componentId, resource);
	}

	/**
	 * Get the {@link SonarServer} for a given Host string
	 * 
	 * @param hostString the Host Strong to get the server for
	 * @return the {@link SonarServer}
	 * @throws URISyntaxException 
	 */
	private SonarServer getSonarServerFromString(String hostString) throws URISyntaxException {
		logger.info("Getting a SonarServer for host string: " + hostString);
		String host, hostUri, username = null, password = null;
		URI uri = new URI(hostString);
		if (uri.getAuthority().indexOf("@") > -1) {
			host = uri.getAuthority().substring(uri.getAuthority().lastIndexOf("@") + 1);
		} else {
			host = uri.getAuthority();
		}
		hostUri = uri.getScheme() + "://" + host;
		if (uri.getAuthority().indexOf("@") > -1) {
			String userInfo = uri.getAuthority().substring(0, uri.getAuthority().lastIndexOf("@"));
			username = userInfo.substring(0, userInfo.indexOf(":"));
			password = userInfo.substring(userInfo.indexOf(":") + 1);
		}
		logger.info("Checking if a server exists with host '" + hostUri + "', username '" + username + "'");
		for (SonarServer other : serverManager.getServers()) {
			if (StringUtils.equals(hostUri, other.getHost()) && StringUtils.equals(username, other.getUsername())) {
				logger.info("Found an existing Sonar Server with the same host string.");
				logger.info("Reusing that server (with name '" + other.getName() + "') instead of creating a new one");
				// The same server was used on another association. Reuse for this association
				return other;
			}
		}
		// Still here? Then we need to store the new server. But making sure the name is unique
		String name = host;
		int index = 1;
		while (serverManager.hasServer(name)) {
			name = host + " - " + index++;
		}
		logger.info("Creating a new SonarServer with name '" + name + "' for host string " + hostString);
		return serverManager.addServer(name, null, hostUri, username, password);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getBuildNumber() {
		return 12;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getPluginKey() {
		return PluginHelper.getPluginKey();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getShortDescription() {
		return "UpgradeTask to upgrade the Sonar Server starage to ActiveObjects";
	}

	/**
	 * Internal method to load the {@link PropertySet}
	 */
	protected PropertySet loadPropertySet() {
		Map<String, Object> arguments = new HashMap<String, Object>();
		arguments.put("delegator.name", "default");
		arguments.put("entityName", "SonarServerProperties");
		arguments.put("entityId", PROPERTIES_ID);
	    return PropertySetManager.getInstance("ofbiz", arguments);
	}

	/**
	 * Get the {@link PropertySet} key for the Jira project Id property
	 * 
	 * @param associationId the associationId to get the property key for
	 * @return the property key
	 */
	private static String getProjectIdPropertyKey(long associationId) {
		return CONFIG_SONAR_PREFIX + associationId + CONFIG_SONAR_PROJECT_ID_SUFFIX;
	}

	/**
	 * Get the {@link PropertySet} key for the Jira project component Id property
	 * 
	 * @param associationId the associationId to get the property key for
	 * @return the property key
	 */
	private static String getComponentIdPropertyKey(long associationId) {
		return CONFIG_SONAR_PREFIX + associationId + CONFIG_SONAR_COMPONENT_ID_SUFFIX;
	}

	/**
	 * Get the {@link PropertySet} key for the Soner Server property
	 * 
	 * @param associationId the associationId to get the property key for
	 * @return the property key
	 */
	private static String getSonarServerPropertyKey(long associationId) {
		return CONFIG_SONAR_PREFIX + associationId + CONFIG_SONAR_SERVER_SUFFIX;
	}

	/**
	 * Get the {@link PropertySet} key for the Sonar Project Key property
	 * 
	 * @param associationId the associationId to get the property key for
	 * @return the property key
	 */
	private static String getSonarProjectPropertyKey(long associationId) {
		return CONFIG_SONAR_PREFIX + associationId + CONFIG_SONAR_PROJECT_KEY_SUFFIX;
	}

}
