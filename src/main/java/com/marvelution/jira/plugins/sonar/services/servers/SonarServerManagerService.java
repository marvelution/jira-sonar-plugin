/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.services.servers;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.atlassian.sal.api.transaction.TransactionCallback;
import net.java.ao.DBParam;
import net.java.ao.Query;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.bc.whitelist.WhitelistManager;
import com.google.common.collect.Lists;

/**
 * {@link SonarServer} manager implementation
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 2.4.0
 */
public class SonarServerManagerService implements SonarServerManager {

	private final ActiveObjects objects;
	private final WhitelistManager whitelistManager;

	/**
	 * Constructor
	 *
	 * @param objects          the {@link ActiveObjects} implementation
	 * @param whitelistManager the {@link WhitelistManager} implementation
	 */
	public SonarServerManagerService(ActiveObjects objects, WhitelistManager whitelistManager) {
		this.objects = checkNotNull(objects, "activeObjects");
		this.whitelistManager = checkNotNull(whitelistManager, "whitelistManager");
	}

	@Override
	public boolean isConfigured() {
		return hasServers();
	}

	@Override
	public boolean hasServers() {
		return objects.executeInTransaction(new TransactionCallback<Boolean>() {
			@Override
			public Boolean doInTransaction() {
				return objects.count(SonarServer.class) > 0;
			}
		});
	}

	@Override
	public boolean hasServer(int serverId) {
		return getServer(serverId) != null;
	}

	@Override
	public boolean hasServer(String name) {
		return getServer(name) != null;
	}

	@Override
	public Collection<SonarServer> getServers() {
		return objects.executeInTransaction(new TransactionCallback<Collection<SonarServer>>() {
			@Override
			public Collection<SonarServer> doInTransaction() {
				return Lists.newArrayList(objects.find(SonarServer.class));
			}
		});
	}

	@Override
	public SonarServer getServer(final int serverId) {
		return objects.executeInTransaction(new TransactionCallback<SonarServer>() {
			@Override
			public SonarServer doInTransaction() {
				return objects.get(SonarServer.class, serverId);
			}
		});
	}

	@Override
	public SonarServer getServer(final String name) {
		return objects.executeInTransaction(new TransactionCallback<SonarServer>() {
			@Override
			public SonarServer doInTransaction() {
				SonarServer[] matches = objects.find(SonarServer.class, Query.select().where("NAME = ?", name).limit(1));
				if (matches != null && matches.length == 1) {
					return matches[0];
				} else {
					return null;
				}
			}
		});
	}

	@Override
	public SonarServer addServer(String name, String description, String host) {
		return addServer(name, description, host, null, null, true);
	}

	@Override
	public SonarServer addServer(String name, String description, String host, String username, String password) {
		return addServer(name, description, host, username, password, true);
	}

	@Override
	public SonarServer addServer(final String name, final String description, final String host,
	                             final String username, final String password,
	                             final boolean includeInStreams) {
		return objects.executeInTransaction(new TransactionCallback<SonarServer>() {
			@Override
			public SonarServer doInTransaction() {
				SonarServer server = objects.create(SonarServer.class, new DBParam("NAME", name),
						new DBParam("HOST", host));
				server.setDescription(description);
				server.setUsername(username);
				server.setPassword(password);
				server.setIncludeInStreams(includeInStreams);
				server.save();
				List<String> rules = whitelistManager.getRules();
				if (!rules.contains(SonarServerUtils.getHostWhitelistUrl(server))) {
					List<String> newRules = new ArrayList<String>(rules);
					newRules.add(SonarServerUtils.getHostWhitelistUrl(server));
					whitelistManager.updateRules(newRules, whitelistManager.isDisabled());
				}
				return server;
			}
		});
	}

	@Override
	public SonarServer addServer(SonarServer server) {
		checkNotNull(server, "server argument may NOT be null");
		return addServer(server.getName(), server.getDescription(), server.getHost(), server.getUsername(),
				server.getPassword(), true);
	}

	@Override
	public SonarServer updateServer(final int serverId, final String name, final String description,
	                                final String host, final String username,
	                                final String password, final boolean includeInStreams) {
		return objects.executeInTransaction(new TransactionCallback<SonarServer>() {
			@Override
			public SonarServer doInTransaction() {
				SonarServer server = objects.get(SonarServer.class, serverId);
				checkNotNull(server, "server");
				server.setName(name);
				server.setDescription(description);
				server.setHost(host);
				server.setUsername(username);
				server.setPassword(password);
				server.setIncludeInStreams(includeInStreams);
				server.save();
				return server;
			}
		});
	}

	@Override
	public SonarServer updateServer(final SonarServer server) {
		checkNotNull(server, "server argument may NOT be null");
		// This one is easy, just save and return
		return objects.executeInTransaction(new TransactionCallback<SonarServer>() {
			@Override
			public SonarServer doInTransaction() {
				server.save();
				return server;
			}
		});
	}

	@Override
	public void removeServer(int serverId) {
		SonarServer server = getServer(serverId);
		checkNotNull(server, "No server configured with Id: " + serverId);
		removeServer(server);
	}

	@Override
	public void removeServer(final SonarServer server) {
		checkNotNull(server, "sonarServer");
		clearCache(server);
		objects.executeInTransaction(new TransactionCallback<Object>() {
			@Override
			public Object doInTransaction() {
				List<String> rules = whitelistManager.getRules();
				if (rules.contains(SonarServerUtils.getHostWhitelistUrl(server))) {
					List<String> newRules = new ArrayList<String>(rules);
					newRules.remove(SonarServerUtils.getHostWhitelistUrl(server));
					whitelistManager.updateRules(newRules, whitelistManager.isDisabled());
				}
				objects.delete(server);
				return null;
			}
		});
	}

	@Override
	public void clearCache(final SonarServer server) {
		checkNotNull(server, "sonarServer");
		objects.executeInTransaction(new TransactionCallback<Object>() {
			@Override
			public Object doInTransaction() {
				objects.delete(server.getCache());
				return null;
			}
		});
	}

}
