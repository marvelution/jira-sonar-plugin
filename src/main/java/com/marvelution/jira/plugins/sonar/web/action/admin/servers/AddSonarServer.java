/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.web.action.admin.servers;

import java.util.ArrayList;
import java.util.Collection;

import com.marvelution.jira.plugins.sonar.services.servers.SonarClientFactory;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServer;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServerManager;
import com.marvelution.jira.plugins.sonar.web.action.admin.KeyValuePair;
import com.marvelution.jira.plugins.sonar.web.action.admin.ModifyActionType;

/**
 * Add {@link SonarServer} Web Action implementation
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class AddSonarServer extends AbstractModifySonarServer {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 * 
	 * @param serverManager the {@link SonarServerManager} implementation
	 * @param clientFactory the {@link SonarClientFactory} implementation
	 */
	protected AddSonarServer(SonarServerManager serverManager, SonarClientFactory clientFactory) {
		super(serverManager, clientFactory);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void doValidation() {
		super.doValidation();
		if (serverManager.hasServer(getName())) {
			addError("name", getText("sonar.server.name.duplicate", getName()));
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveServer(String name, String description, String host, String username, String password,
					boolean includeInStreams) {
		serverManager.addServer(name, description, host, username, password, includeInStreams);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ModifyActionType getActionType() {
		return ModifyActionType.ADD_SERVER;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<KeyValuePair> getExtraHiddenInput() {
		return new ArrayList<KeyValuePair>();
	}

}
