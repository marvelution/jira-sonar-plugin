/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.panels;

import java.util.Map;

import com.atlassian.jira.plugin.projectpanel.impl.AbstractProjectTabPanel;
import com.atlassian.jira.project.browse.BrowseContext;
import com.atlassian.jira.security.JiraAuthenticationContext;

/**
 * Tabpanel to show Sonar data
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class SonarProjectTabPanel extends AbstractProjectTabPanel {

	private final SonarTabPanelHelper panelHelper;

	/**
	 * Constructor
	 * 
	 * @param authenticationContext the {@link JiraAuthenticationContext} implementation
	 * @param panelHelper the {@link SonarTabPanelHelper} implementation
	 */
	public SonarProjectTabPanel(JiraAuthenticationContext authenticationContext, SonarTabPanelHelper panelHelper) {
		super(authenticationContext);
		this.panelHelper = panelHelper;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Map<String, Object> createVelocityParams(BrowseContext context) {
		return panelHelper.prepareVelocityParameters(context.getProject(), null, context.getUser(),
			super.createVelocityParams(context), descriptor);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean showPanel(BrowseContext context) {
		return panelHelper.showPanel(context.getProject(), null, context.getUser(), descriptor);
	}

}
