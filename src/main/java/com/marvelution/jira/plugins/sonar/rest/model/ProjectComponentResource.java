/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.rest.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * {@link ProjectComponent} resource
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark rekveld</a>
 * 
 * @since 3.0.0
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement(name = "component")
public class ProjectComponentResource {

	private static final ToStringStyle TO_STRING_STYLE = ToStringStyle.SIMPLE_STYLE;

	private Long componentId;

	private String componentName;

	/**
	 * Default Constructor
	 */
	public ProjectComponentResource() {
		// Default constructor used by the REST framework
	}

	/**
	 * Constructor
	 * 
	 * @param componentId the Project Component Id
	 * @param componentName the Project Component Name
	 */
	public ProjectComponentResource(Long componentId, String componentName) {
		this.componentId = componentId;
		this.componentName = componentName;
	}

	
	/**
	 * Getter for componentId
	 * 
	 * @return the componentId
	 */
	public Long getComponentId() {
		return componentId;
	}

	
	/**
	 * Setter for componentId
	 * 
	 * @param componentId the componentId to set
	 */
	public void setComponentId(Long componentId) {
		this.componentId = componentId;
	}

	
	/**
	 * Getter for componentName
	 * 
	 * @return the componentName
	 */
	public String getComponentName() {
		return componentName;
	}

	
	/**
	 * Setter for componentName
	 * 
	 * @param componentName the componentName to set
	 */
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object object) {
		return EqualsBuilder.reflectionEquals(this, object);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ProjectComponentResource.TO_STRING_STYLE);
	}
	
}
