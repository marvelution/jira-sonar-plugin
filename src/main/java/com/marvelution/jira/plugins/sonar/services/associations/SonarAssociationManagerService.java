/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.services.associations;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;

import com.atlassian.sal.api.transaction.TransactionCallback;
import net.java.ao.DBParam;
import net.java.ao.Query;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.project.Project;
import com.google.common.collect.Lists;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServer;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServerManager;

/**
 * Default {@link SonarAssociationManager} implementation
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class SonarAssociationManagerService implements SonarAssociationManager {

	private final ActiveObjects objects;
	private final SonarServerManager serverManager;

	/**
	 * Constructor
	 *
	 * @param objects       the {@link ActiveObjects} implementation
	 * @param serverManager the {@link SonarServerManager} implementation
	 */
	public SonarAssociationManagerService(ActiveObjects objects, SonarServerManager serverManager) {
		this.objects = checkNotNull(objects, "activeObjects");
		this.serverManager = checkNotNull(serverManager, "serverManager");
	}

	@Override
	public boolean hasAssociation(int associationId) {
		return getAssociation(associationId) != null;
	}

	@Override
	public boolean hasAssociations() {
		return objects.executeInTransaction(new TransactionCallback<Boolean>() {
			@Override
			public Boolean doInTransaction() {
				return objects.count(SonarAssociation.class) > 0;
			}
		});
	}

	@Override
	public boolean hasAssociations(Project project) {
		return getAssociations(project).size() > 0;
	}

	@Override
	public boolean hasAssociations(Project project, ProjectComponent component) {
		return getAssociations(project, component).size() > 0;
	}

	@Override
	public SonarAssociation getAssociation(final int associationId) {
		return objects.executeInTransaction(new TransactionCallback<SonarAssociation>() {
			@Override
			public SonarAssociation doInTransaction() {
				return objects.get(SonarAssociation.class, associationId);
			}
		});
	}

	@Override
	public Collection<SonarAssociation> getAssociations() {
		return objects.executeInTransaction(new TransactionCallback<Collection<SonarAssociation>>() {
			@Override
			public Collection<SonarAssociation> doInTransaction() {
				return Lists.newArrayList(objects.find(SonarAssociation.class));
			}
		});
	}

	@Override
	public Collection<SonarAssociation> getAssociations(final Project project) {
		return objects.executeInTransaction(new TransactionCallback<Collection<SonarAssociation>>() {
			@Override
			public Collection<SonarAssociation> doInTransaction() {
				return Lists.newArrayList(objects.find(SonarAssociation.class,
						Query.select().where("PROJECT_ID = ?", project.getId())));
			}
		});
	}

	@Override
	public Collection<SonarAssociation> getAssociations(final Project project, final ProjectComponent component) {
		if (component == null) {
			return getAssociations(project);
		}
		return objects.executeInTransaction(new TransactionCallback<Collection<SonarAssociation>>() {
			@Override
			public Collection<SonarAssociation> doInTransaction() {
				return Lists.newArrayList(objects.find(SonarAssociation.class,
						Query.select().where("PROJECT_ID = ? AND COMPONENT_ID = ?", project.getId(),
								component.getId())));
			}
		});
	}

	@Override
	public SonarAssociation addAssociation(final SonarServer server, final long projectId, final long componentId,
	                                       final String resourceName) {
		checkNotNull(server, "server");
		checkNotNull(projectId, "projectId");
		checkNotNull(componentId, "componentId");
		checkNotNull(resourceName, "resourceName");
		return objects.executeInTransaction(new TransactionCallback<SonarAssociation>() {
			@Override
			public SonarAssociation doInTransaction() {
				SonarAssociation association = objects.create(SonarAssociation.class, new DBParam("PROJECT_ID",
						projectId), new DBParam("SONAR_SERVER_ID", server.getID()), new DBParam("SONAR_PROJECT",
						resourceName));
				association.setComponentId(componentId);
				association.save();
				return association;
			}
		});
	}

	@Override
	public SonarAssociation addAssociation(int serverId, long projectId, long componentId, String resourceName) {
		return addAssociation(serverManager.getServer(serverId), projectId, componentId, resourceName);
	}

	@Override
	public SonarAssociation addAssociation(SonarAssociation association) {
		return addAssociation(association.getSonarServer(), association.getProjectId(), association.getComponentId(),
				association.getSonarProject());
	}

	@Override
	public SonarAssociation updateAssociation(final int associationId, final SonarServer server, final long projectId,
	                                          final long componentId, final String resourceName) {
		return objects.executeInTransaction(new TransactionCallback<SonarAssociation>() {
			@Override
			public SonarAssociation doInTransaction() {
				SonarAssociation association = objects.get(SonarAssociation.class,
						associationId);
				checkNotNull(association, "association");
				checkNotNull(server, "server");
				checkNotNull(projectId, "projectId");
				checkNotNull(componentId, "componentId");
				checkNotNull(resourceName, "resourceName");
				association.setSonarServer(server);
				association.setProjectId(projectId);
				association.setComponentId(componentId);
				association.setSonarProject(resourceName);
				association.save();
				return association;
			}
		});
	}

	@Override
	public SonarAssociation updateAssociation(int associationId, int serverId, long projectId, long componentId,
	                                          String resourceName) {
		return updateAssociation(associationId, serverManager.getServer(serverId), projectId, componentId,
				resourceName);
	}

	@Override
	public SonarAssociation updateAssociation(final SonarAssociation association) {
		checkNotNull(association, "association");
		return objects.executeInTransaction(new TransactionCallback<SonarAssociation>() {
			@Override
			public SonarAssociation doInTransaction() {
				association.save();
				return association;
			}
		});
	}

	@Override
	public void removeAssociation(int associationId) {
		SonarAssociation association = getAssociation(associationId);
		checkNotNull(association, "No SonarAssociation configured with Id: " + associationId);
		removeAssociation(association);
	}

	@Override
	public void removeAssociation(final SonarAssociation association) {
		checkNotNull(association, "association");
		objects.executeInTransaction(new TransactionCallback<Object>() {
			@Override
			public Object doInTransaction() {
				objects.delete(association);
				return null;
			}
		});
	}

}
