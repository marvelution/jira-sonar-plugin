/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.web.action.admin.servers;

import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServer;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServerManager;
import com.marvelution.jira.plugins.sonar.web.action.admin.AbstractSonarAdminWebActionSupport;

/**
 * Delete {@link SonarServer} WebAction implementation
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
@WebSudoRequired
public class DeleteSonarServer extends AbstractSonarAdminWebActionSupport {

	private static final long serialVersionUID = 1L;

	private int serverId;
	private SonarServer server;

	/**
	 * Constructor
	 * 
	 * @param serverManager the {@link SonarServerManager} implementation
	 */
	protected DeleteSonarServer(SonarServerManager serverManager) {
		super(serverManager);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String doDefault() throws Exception {
		server = serverManager.getServer(serverId);
		return super.doDefault();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String doExecute() throws Exception {
		serverManager.removeServer(serverId);
		return getRedirect(ADMINISTER_SERVERS);
	}

	/**
	 * Getter for serverId
	 * 
	 * @return the serverId
	 */
	public int getSid() {
		return serverId;
	}

	/**
	 * Setter for serverId
	 * 
	 * @param sid the serverId to set
	 */
	public void setSid(int sid) {
		serverId = sid;
	}

	/**
	 * Getter for server
	 * 
	 * @return the server
	 */
	public String getName() {
		return server.getName();
	}

}
