/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.web.action.admin.associations;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.sonar.wsclient.Sonar;
import org.sonar.wsclient.connectors.ConnectionException;
import org.sonar.wsclient.services.Resource;
import org.sonar.wsclient.services.ResourceQuery;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociationManager;
import com.marvelution.jira.plugins.sonar.services.servers.SonarClientFactory;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServerManager;
import com.marvelution.jira.plugins.sonar.web.action.admin.AbstractSonarProjectAdminWebActionSupport;
import com.marvelution.jira.plugins.sonar.web.action.admin.KeyValuePair;
import com.marvelution.jira.plugins.sonar.web.action.admin.ModifyActionType;

/**
 * Abstract Modify Server WebAction
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
@SuppressWarnings("unchecked")
public abstract class AbstractModifySonarAssociation extends AbstractSonarProjectAdminWebActionSupport {

	private static final long serialVersionUID = 1L;

	private int sonarId = 0;
	private long projectId = 0L;
	private long componentId = 0L;
	private String resourceName;

	protected final SonarAssociationManager associationManager;
	protected final SonarClientFactory clientFactory;
	protected final ProjectComponentManager componentManager;

	/**
	 * Constructor
	 * 
	 * @param serverManager the {@link SonarServerManager} implementation
	 * @param associationManager the {@link SonarAssociationManager} implementation
	 * @param clientFactory the {@link SonarClientFactory} implementation
	 * @param componentManager the {@link ProjectComponentManager} implementation
	 */
	protected AbstractModifySonarAssociation(SonarServerManager serverManager,
												SonarAssociationManager associationManager,
												SonarClientFactory clientFactory,
												ProjectComponentManager componentManager) {
		super(serverManager);
		this.associationManager = associationManager;
		this.clientFactory = clientFactory;
		this.componentManager = componentManager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void doValidation() {
		initRequest();
		if (sonarId == 0 || !serverManager.hasServer(sonarId)) {
			addError("sonarId", getText("sonar.association.server.required"));
		}
		if (projectId == 0 || getProjectManager().getProjectObj(projectId) == null) {
			addError("projectId", getText("sonar.association.project.required"));
		}
		if (StringUtils.isBlank(resourceName)) {
			addError("resourceName", getText("sonar.association.resource.required"));
		} else if (serverManager.hasServer(sonarId)) {
			Sonar sonar = clientFactory.create(serverManager.getServer(sonarId));
			try {
				Resource resource = sonar.find(new ResourceQuery().setResourceKeyOrId(resourceName));
				if (resource == null) {
					addError("resourceName", getText("sonar.association.no.resource.with.key"));
				}
			} catch (ConnectionException e) {
				addError("resourceName", getText("sonar.association.no.resource.with.key"));
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String doExecute() throws Exception {
		initRequest();
		if (hasAnyErrors()) {
			return INPUT;
		}
		saveAssociation(sonarId, projectId, componentId, resourceName);
		return getRedirect(ADMINISTER_ASSOCIATIONS + getContext());
	}

	/**
	 * Internal method to save the Association data
	 * 
	 * @param sonarId the Sonar server Id
	 * @param projectId the JIRA project Id
	 * @param jobName the Sonar Job name
	 */
	protected abstract void saveAssociation(int sonarId, long projectId, long componentId, String jobName);

	/**
	 * Getter for the action type eg: Add/Update
	 * 
	 * @return the {@link ModifyActionType} action
	 */
	public abstract ModifyActionType getActionType();

	/**
	 * Getter for a {@link Collection} of {@link KeyValuePair} objects to be added to the form in hidden inputs
	 * 
	 * @return the {@link Collection} of {@link KeyValuePair} objects
	 */
	public abstract Collection<KeyValuePair> getExtraHiddenInput();

	/**
	 * Getter for the Job Options available on the Sonar Server
	 * 
	 * @return the {@link Collection} of {@link String} options
	 */
	public abstract Collection<ProjectComponent> getComponentOptions();

	/**
	 * Getter for sonarId
	 * 
	 * @return the sonarId
	 */
	public int getSonarId() {
		return sonarId;
	}

	/**
	 * Setter for sonarId
	 * 
	 * @param sonarId the sonarId to set
	 */
	public void setSonarId(int sonarId) {
		this.sonarId = sonarId;;
	}

	/**
	 * Getter for projectId
	 * 
	 * @return the projectId
	 */
	public long getProjectId() {
		return projectId;
	}

	/**
	 * Setter for projectId
	 * 
	 * @param projectId the projectId to set
	 */
	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}

	/**
	 * Getter for componentId
	 * 
	 * @return the componentId
	 */
	public long getComponentId() {
		return componentId;
	}

	/**
	 * Setter for componentId
	 * 
	 * @param componentId the componentId to set
	 */
	public void setComponentId(long componentId) {
		this.componentId = componentId;
	}

	/**
	 * Getter for resourceName
	 * 
	 * @return the resourceName
	 */
	public String getResourceName() {
		return resourceName;
	}

	/**
	 * Setter for resourceName
	 * 
	 * @param resourceName the resourceName to set
	 */
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

}
