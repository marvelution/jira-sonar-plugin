/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.web.action.admin.associations;

import java.util.Collection;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociationManager;
import com.marvelution.jira.plugins.sonar.services.servers.SonarClientFactory;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServer;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServerManager;
import com.marvelution.jira.plugins.sonar.web.action.admin.KeyValuePair;
import com.marvelution.jira.plugins.sonar.web.action.admin.ModifyActionType;

/**
 * Add {@link SonarServer} Web Action implementation
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
@SuppressWarnings("unchecked")
public class AddSonarAssociation extends AbstractModifySonarAssociation {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 * 
	 * @param serverManager the {@link SonarServerManager} implementation
	 * @param associationManager the {@link SonarAssociationManager} implementation
	 * @param clientFactory the {@link SonarClientFactory} implementation
	 * @param componentManager the {@link ProjectComponentManager} implementation
	 */
	protected AddSonarAssociation(SonarServerManager serverManager, SonarAssociationManager associationManager,
									SonarClientFactory clientFactory,ProjectComponentManager componentManager) {
		super(serverManager, associationManager, clientFactory, componentManager);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void doValidation() {
		super.doValidation();
		// TODO check for duplicate association
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveAssociation(int sonarId, long projectId, long componentId, String resourceName) {
		associationManager.addAssociation(sonarId, projectId, componentId, resourceName);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ModifyActionType getActionType() {
		return ModifyActionType.ADD_ASSOCIATION;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<KeyValuePair> getExtraHiddenInput() {
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<ProjectComponent> getComponentOptions() {
		if (getContext() > 0L) {
			return componentManager.findAllForProject(getContext());
		}
		return null;
	}

}
