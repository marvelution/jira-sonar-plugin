/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.streams;

import static com.google.common.base.Preconditions.checkNotNull;

import org.sonar.wsclient.services.Event;
import org.apache.commons.lang.StringUtils;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.common.Option;

/**
 * Helper factory object to get {@link StreamsEntry} {@link Renderer} implementations for different activities
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @Since 4.2.0
 */
public class SonarStreamsEntryRenderer implements StreamsEntry.Renderer {

	private final I18nResolver i18nResolver;
	private final Event event;

	/**
	 * Constructor
	 *
	 * @param i18nResolver the {@link I18nResolver} implementation
	 * @param event the {@link Event}
	 */
	public SonarStreamsEntryRenderer(I18nResolver i18nResolver, Event event) {
		this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
		this.event = checkNotNull(event, "event");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Html renderTitleAsHtml(StreamsEntry entry) {
		return new Html(i18nResolver.getText(getI18nKeyForEvent(EventType.getEventTypeForEvent(event)),
			event.getName(), event.getResourceKey()));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Option<Html> renderSummaryAsHtml(StreamsEntry entry) {
		if (StringUtils.isNotBlank(event.getDescription())) {
			return Option.option(new Html("<blockquote><p>" + event.getDescription() + "</p></blockquote>"));
		}
		return Option.none();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Option<Html> renderContentAsHtml(StreamsEntry entry) {
		return Option.none();
	}

	/**
	 * Get the I18N key for the given {@link EventType}
	 * 
	 * @param activity the {@link EventType} to get he I18N key for
	 * @return the I18N key
	 */
	private String getI18nKeyForEvent(EventType event) {
		return String.format("streams.sonar.activity.title.%1$s", event.name().toLowerCase().replace('_', '.'));
	}

}
