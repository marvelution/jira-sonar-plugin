/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.web.action.admin.servers;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;

import com.google.common.collect.Lists;
import com.marvelution.jira.plugins.sonar.services.servers.SonarClientFactory;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServer;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServerManager;
import com.marvelution.jira.plugins.sonar.web.action.admin.KeyValuePair;
import com.marvelution.jira.plugins.sonar.web.action.admin.ModifyActionType;

/**
 * Update {@link SonarServer} Web Action implementation
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class UpdateSonarServer extends AbstractModifySonarServer {

	private static final long serialVersionUID = 1L;

	private SonarServer server;
	private int sid;

	/**
	 * Constructor
	 * 
	 * @param serverManager the {@link SonarServerManager} implementation
	 * @param clientFactory the {@link SonarClientFactory} implementation
	 */
	protected UpdateSonarServer(SonarServerManager serverManager, SonarClientFactory clientFactory) {
		super(serverManager, clientFactory);
	}

	@Override
	public String doDefault() throws Exception {
		if (!serverManager.hasServer(getSid())) {
			return getRedirect(ADMINISTER_SERVERS);
		}
		server = serverManager.getServer(getSid());
		return super.doDefault();
	}

	@Override
	protected void doValidation() {
		super.doValidation();
		server = serverManager.getServer(getSid());
		SonarServer other = serverManager.getServer(getName());
		if (other != null && other.getID() != getSid()) {
			addError("name", getText("sonar.server.name.duplicate", getName()));
		}
	}

	/**
	 * Action support web method to clear the cache of a {@link SonarServer}
	 * 
	 * @return the return URL {@link String}
	 */
	public String doClearCache() {
		if (serverManager.hasServer(getSid())) {
			SonarServer server = serverManager.getServer(getSid());
			serverManager.clearCache(server);
		}
		return getRedirect(ADMINISTER_SERVERS);
	}

	@Override
	protected void saveServer(String name, String description, String host, String username, String password,
					boolean includeInStreams) {
		serverManager.updateServer(getSid(), name, description, host, username, password, includeInStreams);
	}

	@Override
	public ModifyActionType getActionType() {
		return ModifyActionType.UPDATE_SERVER;
	}

	@Override
	public Collection<KeyValuePair> getExtraHiddenInput() {
		return Lists.newArrayList();
	}

	@Override
	public int getSid() {
		return sid;
	}

	/**
	 * Setter for sid
	 *
	 * @param sid the sid to set
	 */
	public void setSid(int sid) {
		this.sid = sid;
	}

	@Override
	public String getName() {
		if (StringUtils.isNotBlank(super.getName())) {
			return super.getName();
		}
		return server.getName();
	}

	@Override
	public String getDescription() {
		if (StringUtils.isNotBlank(super.getDescription())) {
			return super.getDescription();
		}
		return server.getDescription();
	}

	@Override
	public String getHost() {
		if (StringUtils.isNotBlank(super.getHost())) {
			return super.getHost();
		}
		return server.getHost();
	}

	@Override
	public String getUsername() {
		if (StringUtils.isNotBlank(super.getUsername())) {
			return super.getUsername();
		}
		return server.getUsername();
	}

	@Override
	public String getPassword() {
		if (StringUtils.isNotBlank(super.getPassword())) {
			return super.getPassword();
		}
		return server.getPassword();
	}

	@Override
	public boolean isIncludeInStreams() {
		return server.isIncludeInStreams();
	}

}
