/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.web.action.admin.associations;

import java.util.Collection;

import com.atlassian.jira.bc.EntityNotFoundException;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociation;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociationManager;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServerManager;
import com.marvelution.jira.plugins.sonar.web.action.admin.AbstractSonarProjectAdminWebActionSupport;

/**
 * {@link AbstractSonarProjectAdminWebActionSupport} implementation to administer Sonar association
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class AdministerSonarAssociations extends AbstractSonarProjectAdminWebActionSupport {

	private static final long serialVersionUID = 1L;

	private final SonarAssociationManager associationManager;

	/**
	 * Constructor
	 * 
	 * @param serverManager the {@link SonarServerManager} implementation
	 * @param associationManager the {@link SonarAssociationManager} implementation
	 */
	protected AdministerSonarAssociations(SonarServerManager serverManager, SonarAssociationManager associationManager) {
		super(serverManager);
		this.associationManager = associationManager;
	}

	/**
	 * Getter for all the {@link SonarAssociation}s
	 * 
	 * @return {@link Collection} with all the configured {@link SonarAssociation} objects
	 */
	public Collection<SonarAssociation> getAssociations() {
		if (getContext() != 0L && getProject() != null) {
			return associationManager.getAssociations(getProject());
		} else {
			return associationManager.getAssociations();
		}
	}

	/**
	 * Getter for a {@link ProjectComponent}
	 * 
	 * @param componentId the Id of the {@link ProjectComponent} to get
	 * @return the {@link ProjectComponent}, may be <code>null</code>
	 */
	public ProjectComponent getProjectComponent(long componentId) {
		try {
			return getProjectComponentManager().find(componentId);
		} catch (EntityNotFoundException e) {
			return null;
		}
	}

}
