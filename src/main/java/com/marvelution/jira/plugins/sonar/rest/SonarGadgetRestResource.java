/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.rest;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.marvelution.jira.plugins.sonar.rest.exceptions.InvalidGadgetException;
import com.marvelution.jira.plugins.sonar.rest.exceptions.NotAssociatedException;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociationManager;
import com.marvelution.jira.plugins.sonar.utils.SonarGadgetUtils;

/**
 * REST Resource for Sonar Gadgets
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
@Path("/gadgets")
@AnonymousAllowed
public class SonarGadgetRestResource {

	private final SonarAssociationManager sonarAssociationManager;

	private final SonarGadgetUtils sonarGadgetUtils;

	/**
	 * Constructor
	 * 
	 * @param sonarAssociationManager the {@link SonarAssociationManager} implementation
	 * @param sonarGadgetUtils the {@link SonarGadgetUtils} helper class
	 */
	public SonarGadgetRestResource(SonarAssociationManager sonarAssociationManager,
					SonarGadgetUtils sonarGadgetUtils) {
		this.sonarAssociationManager = sonarAssociationManager;
		this.sonarGadgetUtils = sonarGadgetUtils;
	}

	/**
	 * Generate Gadget HTML
	 * 
	 * @param associationId the associationId used to get the Sonar Server URL and Sonar Project Key
	 * @param gadgetId the Id of the gadget to generate the HTML for
	 * @param request the {@link HttpServletRequest} request {@link Context}
	 * @return the Gadget HTML
	 */
	@GET
	@Path("/generateHtml")
	@Produces(MediaType.TEXT_HTML)
	public String generateGadgetHtml(@QueryParam("associationId") Integer associationId,
					@QueryParam("gadgetId") String gadgetId, @Context HttpServletRequest request) {
		if (!sonarGadgetUtils.getGadgetIds().contains(gadgetId)) {
			throw new InvalidGadgetException("No gadget found with id " + gadgetId);
		}
		if (sonarAssociationManager.getAssociation(associationId) != null) {
			return sonarGadgetUtils.generateGadgetHtml(gadgetId, sonarGadgetUtils
					.createGadgetPreferences(sonarAssociationManager.getAssociation(associationId)), request);
		} else {
			throw new NotAssociatedException("Project not Associated with a Sonar Server");
		}
	}

}
