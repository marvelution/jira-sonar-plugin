/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.services.servers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociation;

/**
 * Sonar Server utility class
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 4.2.0
 */
public class SonarServerUtils {

	private static final Pattern SERVER_HOST_PATTERN = Pattern.compile("^(http|https):\\/\\/(.*)$");

	/**
	 * Get a Whitelist url for the given {@link SonarServer} host url
	 * 
	 * @param server the {@link SonarServer}
	 * @return the whitelist url
	 */
	public static String getHostWhitelistUrl(SonarServer server) {
		return getWhitelistUrl(server.getHost());
	}

	/**
	 * Get a Whitelist url for the given url
	 * 
	 * @param url the url to get a whitelist url for
	 * @return the whitelist url
	 */
	public static String getWhitelistUrl(String url) {
		if (url.endsWith("/")) {
			return url + "*";
		} else {
			return url + "/*";
		}
	}

	/**
	 * Check method to see if a {@link SonarServer} is secured or anonymous
	 * 
	 * @param server the {@link SonarServer} to check
	 * @return <code>true</code> if username and password are set (secured), <code>false</code> (anonymous) otherwise
	 */
	public static boolean isServerSecured(SonarServer server) {
		return StringUtils.isNotBlank(server.getUsername()) && StringUtils.isNotBlank(server.getPassword());
	}

	/**
	 * Check method to see if the {@link SonarServer} has associations
	 * 
	 * @param server the {@link SonarServer} to check
	 * @return <code>true</code> if there are {@link SonarAssociation} objects using the server
	 */
	public static boolean hasAssociations(SonarServer server) {
		return server.getAssociations() != null && server.getAssociations().length > 0;
	}

	/**
	 * Get the Server URL used for Sonar Gadgets
	 * 
	 * @param server the {@link SonarServer}
	 * @return the gadget server url
	 */
	public static String getSonarServerGadgetUrl(SonarServer server) {
		if (isServerSecured(server)) {
			final Matcher matcher = SERVER_HOST_PATTERN.matcher(server.getHost());
			if (matcher.find()) {
				return matcher.group(1) + "://" + server.getUsername() + ":" + server.getPassword() + "@"
					+ matcher.group(2);
			}
		}
		return server.getHost();
	}

	/**
	 * Check if two {@link SonarServer} instances are pointing to the same server using the same username and password
	 * 
	 * @param server1 {@link SonarServer}
	 * @param server2 {@link SonarServer}
	 * @return <code>true</code> if the host, username and password properties are equal, <code>false</code> otherwise
	 */
	public static boolean equals(SonarServer server1, SonarServer server2) {
		if (server1 != null && server2 != null) {
			return StringUtils.equals(server1.getHost(), server2.getHost())
				&& StringUtils.equals(server1.getUsername(), server2.getUsername())
				&& StringUtils.equals(server1.getPassword(), server2.getPassword());
		}
		return false;
	}

}
