/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.rest.model;

import com.marvelution.gadgets.sonar.wsclient.services.Version;

import java.util.Collection;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 2.5.0
 */
@XmlRootElement
public class Versions {

	@XmlElement
	private Collection<Version> version;

	/**
	 * Constructor
	 */
	public Versions() {
	}

	/**
	 * Constructor
	 *
	 * @param version
	 */
	public Versions(Collection<Version> version) {
		this.version = version;
	}

	/**
	 * Getter for version
	 *
	 * @return the version
	 */
	public Collection<Version> getVersion() {
		return version;
	}

}
