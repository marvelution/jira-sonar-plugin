/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.services.associations;

import com.marvelution.jira.plugins.sonar.services.servers.SonarServer;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.NotNull;

/**
 * Sonar Association Interface
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
@Preload
public interface SonarAssociation extends Entity {

	/**
	 * Get the Jira Project Id
	 * 
	 * @return the Jira Project Id
	 */
	@NotNull
	Long getProjectId();

	/**
	 * Set the Jira Project Id
	 * 
	 * @param projectId the Jira Project Id
	 */
	void setProjectId(Long projectId);

	/**
	 * Get the Jira Project component Id
	 * 
	 * @return the Jira Project component Id
	 */
	Long getComponentId();

	/**
	 * Set the Jira Project component Id
	 * 
	 * @param componentId the Jira Project component Id
	 */
	void setComponentId(Long componentId);

	/**
	 * Get the Sonar server url
	 * 
	 * @return the Sonar server url
	 */
	@NotNull
	SonarServer getSonarServer();

	/**
	 * Set the Sonar server url
	 * 
	 * @param sonarServer the Sonar server url
	 */
	void setSonarServer(SonarServer sonarServer);

	/**
	 * Get the Sonar server project key
	 * 
	 * @return the Sonar server project key
	 */
	@NotNull
	String getSonarProject();

	/**
	 * Set the Sonar server project key
	 * 
	 * @param sonarProject the Sonar server project key
	 */
	void setSonarProject(String sonarProject);

}
