/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.services.layout;

import com.marvelution.jira.plugins.sonar.services.layout.Layout.Context;

/**
 * Sonar panel layout manager interface
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public interface SonarPanelLayoutManager {

	/**
	 * Get all the available Gadgets that can be configured on a {@link Layout}
	 * 
	 * @return the {@link Gadget} array
	 */
	Gadget[] getGadgets();

	/**
	 * Get a single {@link Gadget} by its short name
	 * 
	 * @param shortName the short name to get the {@link Gadget} for
	 * @return the {@link Gadget}, may be <code>null</code> in case the gadget with the given short name can't be found
	 */
	Gadget getGadget(String shortName);

	/**
	 * Add a single {@link Gadget} to the given {@link Column}
	 * 
	 * @param gadget the {@link Gadget} to add
	 * @param columnId the Id of the {@link Column} to add the {@link Gadget} to
	 * @return <code>true</code> if successful
	 */
	boolean addGadgetToColumn(Gadget gadget, Integer columnId);

	/**
	 * Add a single {@link Gadget} to the given {@link Column}
	 * 
	 * @param gadget the {@link Gadget} to add
	 * @param columnId the Id of the {@link Column} to add the {@link Gadget} to
	 * @param position the position to add the gadget on, -1 for last
	 * @return <code>true</code> if successful
	 */
	boolean addGadgetToColumn(Gadget gadget, Integer columnId, int position);

	/**
	 * Add a single {@link Gadget} to the given {@link Column}
	 * 
	 * @param gadget the {@link Gadget} to add
	 * @param column the {@link Column} to add the {@link Gadget} to
	 * @return <code>true</code> if successful
	 */
	boolean addGadgetToColumn(Gadget gadget, Column column);

	/**
	 * Add a single {@link Gadget} to the given {@link Column}
	 * 
	 * @param gadget the {@link Gadget} to add
	 * @param column the {@link Column} to add the {@link Gadget} to
	 * @param position the position to add the gadget on, -1 for last
	 * @return <code>true</code> if successful
	 */
	boolean addGadgetToColumn(Gadget gadget, Column column, int position);

	/**
	 * Remove a single {@link Gadget} from the given {@link Column}
	 * 
	 * @param gadget the {@link Gadget} to remove
	 * @param columnId the Id of the {@link Column} to remove the {@link Gadget} from
	 * @return <code>true</code> if successful
	 */
	boolean removeGadgetFromColumn(Gadget gadget, Integer columnId);

	/**
	 * Remove a single {@link Gadget} from the given {@link Column}
	 * 
	 * @param gadget the {@link Gadget} to remove
	 * @param column the {@link Column} to remove the {@link Gadget} from
	 * @return <code>true</code> if successful
	 */
	boolean removeGadgetFromColumn(Gadget gadget, Column column);

	/**
	 * Remove all the Gadgets from the given {@link Column}
	 * 
	 * @param column the {@link Column} to remove all the Gadgets from
	 * @return <code>true</code> if successful
	 */
	boolean removeAllGadgetsFromColumn(Column column);

	/**
	 * Create a new {@link Layout} for the Layout.Context.PROJECT context
	 * 
	 * @param type the {@link Layout.Type} for the new {@link Layout} 
	 * @param contextID the Project ID the new Layout applies to
	 * @return the new {@link Layout}
	 */
	Layout createLayout(Layout.Type type, Long contextID);

	/**
	 * Change the {@link Layout.Type} of the given {@link Layout}
	 * @param layout the {@link Layout} to change
	 * @param newType the new {@link Layout.Type}
	 * @return the changed {@link Layout}
	 */
	Layout changeLayoutType(Layout layout, Layout.Type newType);

	/**
	 * Remove a {@link Layout}, this can't be the layout of Context SYSTEM
	 * 
	 * @param layout the {@link Layout} to remove
	 * @return <code>true</code> is successful
	 */
	boolean removeLayout(Layout layout);

	/**
	 * Getter for the SYSTEM {@link Layout}
	 * 
	 * @return the {@link Layout} with a {@link Layout.Context} of SYSTEM
	 */
	Layout getSystemLayout();

	/**
	 * Getter for a {@link Layout} by the contextID
	 * If there is no Layout for the specific contextID then the SYSTEM Layout is returned
	 * 
	 * @param contextID the Context ID to get the {@link Layout} for
	 * @return the {@link Layout}
	 */
	Layout getLayout(Long contextID);

	/**
	 * Getter for a {@link Layout} by the context
	 * 
	 * @param context the {@link Context}
	 * @return the {@link Layout}
	 * @since 2.5.0
	 */
	Layout getLayout(Layout.Context context);

	/**
	 * Getter for all the Layouts avaliable
	 * 
	 * @return {@link Layout} array
	 */
	Layout[] getLayouts();

	/**
	 * Getter for a {@link Layout} by tID
	 * 
	 * @param layoutID the Layout ID
	 * @return the {@link Layout}, may be <code>null</code>
	 */
	Layout getLayoutById(int layoutID);

	/**
	 * Getter for a {@link Layout} by a {@link Column} ID
	 * 
	 * @param columnID the Column ID
	 * @return the {@link Layout}, may be <code>null</code>
	 */
	Layout getLayoutByColumnId(int columnID);

}
