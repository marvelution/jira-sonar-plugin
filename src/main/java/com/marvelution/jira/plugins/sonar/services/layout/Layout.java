/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.services.layout;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

import net.java.ao.Implementation;
import net.java.ao.OneToMany;
import net.java.ao.Entity;
import net.java.ao.schema.NotNull;

/**
 * The Layout Active Objects object
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * 
 * @since 2.4.0
 */
@Implementation(LayoutEntity.class)
public interface Layout extends Entity {

	/**
	 * Getter of the {@link Type} of the Layout
	 * 
	 * @return the {@link Type}
	 */
	@NotNull
	Type getType();

	/**
	 * Setter of the {@link Type} of the Layout
	 * 
	 * @param type the {@link Type} of the Layout
	 */
	void setType(Type type);

	/**
	 * Getter of the {@link Context} of the Layout
	 * 
	 * @return the {@link Context} of the Layout
	 */
	@NotNull
	Context getContext();

	/**
	 * Setter of the {@link Context} of the Layout
	 * 
	 * @param context the {@link Context} of the Layout to set
	 */
	void setContext(Context context);

	/**
	 * Getter of the Contextual ID
	 * 
	 * @return the Contextual ID
	 */
	Long getContextID();

	/**
	 * Setter of the the Contextual ID
	 * 
	 * @param contextID the Contextual ID
	 */
	void setContextID(Long contextID);

	/**
	 * Getter of the Columns linked to the Layout
	 * 
	 * @return the Columns linked to the Layout
	 */
	@OneToMany
	Column[] getColumns();

	/**
	 * 
	 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
	 */
	@XmlEnum(String.class)
	@XmlType(namespace = "layout")
	public enum Type {
		A(new Column.Type[] {Column.Type.A}),
		AA(new Column.Type[] {Column.Type.A, Column.Type.A}),
		AB(new Column.Type[] {Column.Type.A, Column.Type.B}),
		BA(new Column.Type[] {Column.Type.B, Column.Type.A}),
		AAA(new Column.Type[] {Column.Type.A, Column.Type.A, Column.Type.A});

		public Column.Type[] columns;

		/**
		 * Constructor
		 *
		 * @param columns the Array of Columns required by the Layout
		 */
		private Type(Column.Type[] columns) {
			this.columns = columns;
		}
	}

	/**
	 * Context definitions for the Layout
	 * 
	 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
	 */
	@XmlEnum(String.class)
	public enum Context {
		SYSTEM, PROJECT, SQALE;
	}

}
