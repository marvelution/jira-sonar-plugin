/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.rest.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * {@link ProjectComponent} resource
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark rekveld</a>
 * 
 * @since 3.0.0
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement(name = "components")
public class ProjectComponentsResource {

	private static final ToStringStyle TO_STRING_STYLE = ToStringStyle.SIMPLE_STYLE;

	private Collection<ProjectComponentResource> components;

	/**
	 * Default Constructor
	 */
	public ProjectComponentsResource() {
		// Default constructor used by the REST framework
	}

	/**
	 * Constructor
	 * 
	 * @param components {@link Collection} of {@link ProjectComponentResource}
	 */
	public ProjectComponentsResource(Collection<ProjectComponentResource> components) {
		this.components = components;
	}

	/**
	 * Getter for components
	 * 
	 * @return the components
	 */
	@XmlElement(name = "components")
	public Collection<ProjectComponentResource> getComponents() {
		if (components == null) {
			components = new ArrayList<ProjectComponentResource>();
		}
		return components;
	}

	/**
	 * Setter for components
	 * 
	 * @param components the components to set
	 */
	public void setComponents(Collection<ProjectComponentResource> components) {
		this.components = components;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object object) {
		return EqualsBuilder.reflectionEquals(this, object);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ProjectComponentsResource.TO_STRING_STYLE);
	}

}
