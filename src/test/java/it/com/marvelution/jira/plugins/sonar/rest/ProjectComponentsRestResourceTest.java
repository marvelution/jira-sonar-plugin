/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.com.marvelution.jira.plugins.sonar.rest;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;

import com.atlassian.jira.nimblefunctests.annotation.RestoreOnce;
import com.marvelution.jira.plugins.sonar.rest.ProjectComponentsRestResource;
import com.marvelution.jira.plugins.sonar.utils.SonarPluginTestConstants;
import com.sun.jersey.api.client.WebResource;

/**
 * Testcase for the {@link ProjectComponentsRestResource}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 3.1.0
 */
@RestoreOnce(SonarPluginTestConstants.DEFAULT_JIRA_EXPORT)
public class ProjectComponentsRestResourceTest extends AbstractRestResourceTestcase {

	/**
	 * Test {@link ProjectComponentsRestResource#getProjectComponents(Long)} with an admin account
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testGetProjectComponentsWithAdminAccount() throws Exception {
		setAdmin();
		testGetProjectComponents();
	}

	/**
	 * Test {@link ProjectComponentsRestResource#getProjectComponents(Long)} with an user account
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testGetProjectComponentsWithUserAccount() throws Exception {
		setUser();
		testGetProjectComponents();
	}

	/**
	 * Test {@link ProjectComponentsRestResource#getProjectComponents(Long)} with an anonymous account
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testGetProjectComponentsWithAnonymousAccount() throws Exception {
		setAnonymous();
		testGetProjectComponents();
	}

	private void testGetProjectComponents() throws Exception {
		WebResource resource = client.resource(UriBuilder.fromUri(restUri).path(ProjectComponentsRestResource.class)
			.queryParam("projectId", SonarPluginTestConstants.JIRA_SONAR_PLUGIN_ID).build());
		resource.accept(MediaType.APPLICATION_JSON);
		JSONObject json = resource.get(JSONObject.class);
		JSONArray array = json.getJSONArray("components");
		JSONObject component = (JSONObject) array.get(0);
		assertThat(component.getLong("componentId"), is(10101L));
		assertThat(component.getString("componentName"), is("Administration"));
		component = (JSONObject) array.get(1);
		assertThat(component.getLong("componentId"), is(10000L));
		assertThat(component.getString("componentName"), is("Gadgets"));
		component = (JSONObject) array.get(2);
		assertThat(component.getLong("componentId"), is(10100L));
		assertThat(component.getString("componentName"), is("Panels"));
	}

}
