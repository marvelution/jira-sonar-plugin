/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.com.marvelution.jira.plugins.sonar.rest;

import static com.marvelution.jira.plugins.sonar.utils.SonarPluginTestConstants.*;

import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.core.UriBuilder;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;

import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import com.atlassian.jira.nimblefunctests.framework.NimbleFuncTestCase;
import com.sun.jersey.client.apache.ApacheHttpClient;
import com.sun.jersey.client.apache.ApacheHttpClientHandler;
import com.sun.jersey.client.apache.config.ApacheHttpClientConfig;
import com.sun.jersey.client.apache.config.DefaultApacheHttpClientConfig;

/**
 * Base testcase for REST resources
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 3.1.0
 */
public abstract class AbstractRestResourceTestcase extends NimbleFuncTestCase {

	protected URI jiraUri;
	protected URI restUri;
	protected ApacheHttpClient client;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void beforeMethod() {
		super.beforeMethod();
		try {
			jiraUri = UriBuilder.fromUri(environmentData.getBaseUrl().toURI()).build();
			restUri = UriBuilder.fromUri(jiraUri).path("/rest/sonar/1.0/").build();
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Set the {@link ApacheHttpClient} with an admin account
	 */
	public void setAdmin() {
		setClient(ADMIN_USERNAME, ADMIN_PASSWORD);
	}

	/**
	 * Set the {@link ApacheHttpClient} with an user account
	 */
	public void setUser() {
		setClient(USER_USERNAME, USER_PASSWORD);
	}

	/**
	 * Set the {@link ApacheHttpClient} with an anonymous account
	 */
	public void setAnonymous() {
		setClient(null, null);
	}

	/**
	 * Create the {@link ApacheHttpClient} for REST invocations
	 * 
	 * @param username the username, if it is <code>empty</code> or <code>null</code> then no authenticator will be configured
	 * @param password the password for the user
	 */
	private final void setClient(String username, String password) {
	    DefaultApacheHttpClientConfig config = new DefaultApacheHttpClientConfig();
	    if (StringUtils.isNotBlank(username)) {
	    	config.getState().setCredentials(null, null, -1, username, password);
	    	config.getProperties().put(ApacheHttpClientConfig.PROPERTY_PREEMPTIVE_AUTHENTICATION, true);
	    }
	    client = new ApacheHttpClient(createDefaultClientHander(config));
	}

	/**
	 * Internal method to create the {@link ApacheHttpClientHandler}
	 * 
	 * @param config the {@link DefaultApacheHttpClientConfig}
	 * @return the {@link ApacheHttpClientHandler}
	 */
	private static ApacheHttpClientHandler createDefaultClientHander(DefaultApacheHttpClientConfig config) {
        final HttpClient client = new HttpClient(new MultiThreadedHttpConnectionManager());
        return new ApacheHttpClientHandler(client, config);
    }

}
