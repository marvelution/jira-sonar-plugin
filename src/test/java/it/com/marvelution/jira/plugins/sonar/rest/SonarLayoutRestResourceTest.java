/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.com.marvelution.jira.plugins.sonar.rest;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.*;

import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.atlassian.jira.nimblefunctests.annotation.RestoreOnce;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.marvelution.gadgets.sonar.utils.SonarGadgetsUtils;
import com.marvelution.jira.plugins.sonar.rest.SonarLayoutRestResource;
import com.marvelution.jira.plugins.sonar.services.layout.Column;
import com.marvelution.jira.plugins.sonar.services.layout.Layout;
import com.marvelution.jira.plugins.sonar.services.layout.Layout.Context;
import com.marvelution.jira.plugins.sonar.utils.SonarPluginTestConstants;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.representation.Form;

/**
 * Testcase for the {@link SonarLayoutRestResource}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 3.1.0
 */
@RestoreOnce(SonarPluginTestConstants.DEFAULT_JIRA_EXPORT)
public class SonarLayoutRestResourceTest extends AbstractRestResourceTestcase {

	@Mock private I18nResolver i18nResolver;
	private SonarGadgetsUtils gadgetsUtils;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void beforeMethod() {
		super.beforeMethod();
		MockitoAnnotations.initMocks(this);
		when(i18nResolver.getAllTranslationsForPrefix("sonar.gadget", Locale.ENGLISH)).thenAnswer(new Answer<Map<String, String>>() {
			@Override
			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				Properties properties = new Properties();
				properties.load(getClass().getClassLoader().getResourceAsStream("i18n/sonar-gadgets.properties"));
				return Maps.fromProperties(properties);
			}
		});
		gadgetsUtils = new SonarGadgetsUtils(i18nResolver);
	}

	/**
	 * Test the {@link SonarLayoutRestResource#getLayoutTypes()}
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testGetTypes() throws Exception {
		setAnonymous();
		WebResource resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class).path("/types").build());
		resource.accept(MediaType.APPLICATION_JSON);
		JSONObject json = resource.get(JSONObject.class);
		JSONArray types = json.getJSONArray("type");
		for (int index = 0; index < types.length(); index++) {
			assertThat(types.getString(index), is(Layout.Type.values()[index].name()));
		}
	}

	/**
	 * Test the {@link SonarLayoutRestResource#getGadgets()}
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testGetGadgets() throws Exception {
		setAnonymous();
		WebResource resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class).path("/gadgets").build());
		resource.accept(MediaType.APPLICATION_JSON);
		JSONObject json = resource.get(JSONObject.class);
		JSONArray gadgets = json.getJSONArray("gadget");
		Collection<String> gadgetIds = gadgetsUtils.getGadgetIds();
		assertThat(gadgets.length(), is(gadgetIds.size()));
		for (int index = 0; index < gadgets.length(); index++) {
			assertThat("Invalid Gadget ID found in response: " + gadgets.getString(index), gadgetIds.contains(gadgets.getString(index)),
				is(true));
		}
	}

	/**
	 * Test the {@link SonarLayoutRestResource#getSystemLayout()}
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testGetSystemLayout() throws Exception {
		setAnonymous();
		WebResource resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class).build());
		resource.accept(MediaType.APPLICATION_JSON);
		com.marvelution.jira.plugins.sonar.rest.model.Layout layout =
			resource.get(com.marvelution.jira.plugins.sonar.rest.model.Layout.class);
		assertThat(layout.getContext(), is (Layout.Context.SYSTEM));
		assertThat(layout.getContextID(), is (0L));
		assertThat(layout.getType(), is (Layout.Type.AA));
		com.marvelution.jira.plugins.sonar.rest.model.Column[] columns = layout.getColumns().toArray(
			new com.marvelution.jira.plugins.sonar.rest.model.Column[layout.getColumns().size()]);
		assertThat(columns[0].getType(), is(Column.Type.A));
		assertThat(columns[0].getGadgets().size(), is(4));
		assertThat(columns[0].getGadgets().contains("loc"), is(true));
		assertThat(columns[0].getGadgets().contains("comments"), is(true));
		assertThat(columns[0].getGadgets().contains("violations"), is(true));
		assertThat(columns[0].getGadgets().contains("coverage"), is(true));
		assertThat(columns[1].getType(), is(Column.Type.A));
		assertThat(columns[1].getGadgets().size(), is(2));
		assertThat(columns[1].getGadgets().contains("complexity"), is(true));
		assertThat(columns[1].getGadgets().contains("treemap"), is(true));
	}

	/**
	 * Test {@link SonarLayoutRestResource#getLayout(com.marvelution.jira.plugins.sonar.services.layout.Layout.Context)} for the
	 * {@link Context#SQALE} context
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testGetSqaleLayout() throws Exception {
		setAnonymous();
		WebResource resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class).path("/context/SQALE")
			.build());
		resource.accept(MediaType.APPLICATION_JSON);
		com.marvelution.jira.plugins.sonar.rest.model.Layout layout =
			resource.get(com.marvelution.jira.plugins.sonar.rest.model.Layout.class);
		assertThat(layout.getContext(), is (Layout.Context.SQALE));
		assertThat(layout.getType(), is(Layout.Type.AA));
		com.marvelution.jira.plugins.sonar.rest.model.Column[] columns = layout.getColumns().toArray(
			new com.marvelution.jira.plugins.sonar.rest.model.Column[layout.getColumns().size()]);
		assertThat(columns[0].getType(), is(Column.Type.A));
		assertThat(columns[0].getGadgets().size(), is(4));
		assertThat(columns[0].getGadgets().contains("sqale-rating"), is(true));
		assertThat(columns[0].getGadgets().contains("sqale-fdr"), is(true));
		assertThat(columns[0].getGadgets().contains("sqale-rrc"), is(true));
		assertThat(columns[0].getGadgets().contains("sqale-pyramid"), is(true));
		assertThat(columns[1].getType(), is(Column.Type.A));
		assertThat(columns[1].getGadgets().size(), is(1));
		assertThat(columns[1].getGadgets().contains("sqale-sunburst"), is(true));
	}

	/**
	 * Test {@link SonarLayoutRestResource#addGadget(String, Integer, com.atlassian.plugins.rest.common.security.AuthenticationContext)}
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testAddGadgetNotAuthorised() throws Exception {
		setUser();
		WebResource resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class)
			.path(String.valueOf(SonarPluginTestConstants.JIRA_SONAR_PLUGIN_ID)).build());
		resource.accept(MediaType.APPLICATION_JSON);
		com.marvelution.jira.plugins.sonar.rest.model.Layout layout =
			resource.get(com.marvelution.jira.plugins.sonar.rest.model.Layout.class);
		Form form = new Form();
		form.add("columnId", layout.getColumns().iterator().next().getColumnId());
		resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class).path("/add/events").build());
		try {
			resource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(form);
			fail("The API is for admins only - check permission implementation");
		} catch (UniformInterfaceException e) {
			assertThat(e.getResponse().getStatus(), is(Response.Status.UNAUTHORIZED.getStatusCode()));
		}
	}

	/**
	 * Test {@link SonarLayoutRestResource#addGadget(String, Integer, com.atlassian.plugins.rest.common.security.AuthenticationContext)}
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testAddGadget() throws Exception {
		setAdmin();
		WebResource resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class)
			.path(String.valueOf(SonarPluginTestConstants.JIRA_SONAR_PLUGIN_ID)).build());
		resource.accept(MediaType.APPLICATION_JSON);
		com.marvelution.jira.plugins.sonar.rest.model.Layout layout =
			resource.get(com.marvelution.jira.plugins.sonar.rest.model.Layout.class);
		Form form = new Form();
		form.add("columnId", layout.getColumns().iterator().next().getColumnId());
		resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class).path("/add/events").build());
		ClientResponse response = resource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, form);
		assertThat(response.getStatus(), is(Response.Status.OK.getStatusCode()));
		resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class)
			.path(String.valueOf(SonarPluginTestConstants.JIRA_SONAR_PLUGIN_ID)).build());
		resource.accept(MediaType.APPLICATION_JSON);
		layout = resource.get(com.marvelution.jira.plugins.sonar.rest.model.Layout.class);
		com.marvelution.jira.plugins.sonar.rest.model.Column column = layout.getColumns().iterator().next();
		List<String> gadgets = Lists.newArrayList(column.getGadgets());
		assertThat(gadgets.contains("events"), is(true));
		assertThat(gadgets.get(gadgets.size() - 1), is("events"));
	}

	/**
	 * Test {@link SonarLayoutRestResource#addGadget(String, Integer, com.atlassian.plugins.rest.common.security.AuthenticationContext)}
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testAddGadgetInvalidGadgetShortName() throws Exception {
		setAdmin();
		WebResource resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class)
			.path(String.valueOf(SonarPluginTestConstants.JIRA_SONAR_PLUGIN_ID)).build());
		resource.accept(MediaType.APPLICATION_JSON);
		com.marvelution.jira.plugins.sonar.rest.model.Layout layout =
			resource.get(com.marvelution.jira.plugins.sonar.rest.model.Layout.class);
		Form form = new Form();
		form.add("columnId", layout.getColumns().iterator().next().getColumnId());
		resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class).path("/add/i-dont-exist").build());
		try {
			resource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(form);
			fail("Invalid Gadget was found! Check Gadget shortname check");
		} catch (UniformInterfaceException e) {
			assertThat(e.getResponse().getStatus(), is(Response.Status.NOT_FOUND.getStatusCode()));
			assertThat(e.getResponse().getEntity(String.class), is("No gadget is available with short name i-dont-exist"));
		}
	}

	/**
	 * Test {@link SonarLayoutRestResource#addGadget(String, Integer, com.atlassian.plugins.rest.common.security.AuthenticationContext)}
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testAddGadgetInvalidLayout() throws Exception {
		setAdmin();
		Form form = new Form();
		form.add("columnId", 111);
		WebResource resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class).path("/add/i-dont-exist").build());
		try {
			resource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(form);
			fail("Invalid Layout was found! Check getLayoutByColumnId() check");
		} catch (UniformInterfaceException e) {
			assertThat(e.getResponse().getStatus(), is(Response.Status.NOT_FOUND.getStatusCode()));
			assertThat(e.getResponse().getEntity(String.class), is("No layout with a column with id: 111"));
		}
	}

	/**
	 * Test {@link SonarLayoutRestResource#moveGadget(String, Integer, Integer, Integer, com.atlassian.plugins.rest.common.security.AuthenticationContext)}
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testMoveGadgetNotAuthorised() throws Exception {
		setUser();
		WebResource resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class)
			.path(String.valueOf(SonarPluginTestConstants.JIRA_SONAR_PLUGIN_ID)).build());
		resource.accept(MediaType.APPLICATION_JSON);
		com.marvelution.jira.plugins.sonar.rest.model.Layout layout =
			resource.get(com.marvelution.jira.plugins.sonar.rest.model.Layout.class);
		List<com.marvelution.jira.plugins.sonar.rest.model.Column> columns = Lists.newArrayList(layout.getColumns());
		Form form = new Form();
		form.add("newColumnId", columns.get(1).getColumnId());
		form.add("oldColumnId", columns.get(0).getColumnId());
		form.add("position", 0);
		resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class).path("/move/events").build());
		try {
			resource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(form);
			fail("The API is for admins only - check permission implementation");
		} catch (UniformInterfaceException e) {
			assertThat(e.getResponse().getStatus(), is(Response.Status.UNAUTHORIZED.getStatusCode()));
		}
	}

	/**
	 * Test {@link SonarLayoutRestResource#moveGadget(String, Integer, Integer, Integer, com.atlassian.plugins.rest.common.security.AuthenticationContext)}
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testMoveGadget() throws Exception {
		setAdmin();
		WebResource resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class)
			.path(String.valueOf(SonarPluginTestConstants.JIRA_SONAR_PLUGIN_ID)).build());
		resource.accept(MediaType.APPLICATION_JSON);
		com.marvelution.jira.plugins.sonar.rest.model.Layout layout =
			resource.get(com.marvelution.jira.plugins.sonar.rest.model.Layout.class);
		List<com.marvelution.jira.plugins.sonar.rest.model.Column> columns = Lists.newArrayList(layout.getColumns());
		Form form = new Form();
		form.add("newColumnId", columns.get(1).getColumnId());
		form.add("oldColumnId", columns.get(0).getColumnId());
		form.add("position", 0);
		resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class).path("/move/events").build());
		ClientResponse response = resource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, form);
		assertThat(response.getStatus(), is(Response.Status.OK.getStatusCode()));
		resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class)
			.path(String.valueOf(SonarPluginTestConstants.JIRA_SONAR_PLUGIN_ID)).build());
		resource.accept(MediaType.APPLICATION_JSON);
		layout = resource.get(com.marvelution.jira.plugins.sonar.rest.model.Layout.class);
		columns = Lists.newArrayList(layout.getColumns());
		assertThat(columns.get(0).getGadgets().contains("events"), is(false));
		assertThat(columns.get(1).getGadgets().contains("events"), is(true));
		assertThat(columns.get(1).getGadgets().iterator().next(), is("events"));
	}

	/**
	 * Test {@link SonarLayoutRestResource#moveGadget(String, Integer, Integer, Integer, com.atlassian.plugins.rest.common.security.AuthenticationContext)}
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testMoveGadgetInvalidGadgetShortName() throws Exception {
		setAdmin();
		WebResource resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class)
			.path(String.valueOf(SonarPluginTestConstants.JIRA_SONAR_PLUGIN_ID)).build());
		resource.accept(MediaType.APPLICATION_JSON);
		com.marvelution.jira.plugins.sonar.rest.model.Layout layout =
			resource.get(com.marvelution.jira.plugins.sonar.rest.model.Layout.class);
		List<com.marvelution.jira.plugins.sonar.rest.model.Column> columns = Lists.newArrayList(layout.getColumns());
		Form form = new Form();
		form.add("newColumnId", columns.get(1).getColumnId());
		form.add("oldColumnId", columns.get(0).getColumnId());
		form.add("position", 0);
		resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class).path("/move/i-dont-exist").build());
		try {
			resource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(form);
			fail("Invalid Gadget was found! Check Gadget shortname check");
		} catch (UniformInterfaceException e) {
			assertThat(e.getResponse().getStatus(), is(Response.Status.NOT_FOUND.getStatusCode()));
			assertThat(e.getResponse().getEntity(String.class), is("No gadget is available with short name i-dont-exist"));
		}
	}

	/**
	 * Test {@link SonarLayoutRestResource#moveGadget(String, Integer, Integer, Integer, com.atlassian.plugins.rest.common.security.AuthenticationContext)}
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testMoveGadgetInvalidLayout() throws Exception {
		setAdmin();
		WebResource resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class)
			.path(String.valueOf(SonarPluginTestConstants.JIRA_SONAR_PLUGIN_ID)).build());
		resource.accept(MediaType.APPLICATION_JSON);
		com.marvelution.jira.plugins.sonar.rest.model.Layout layout =
			resource.get(com.marvelution.jira.plugins.sonar.rest.model.Layout.class);
		List<com.marvelution.jira.plugins.sonar.rest.model.Column> columns = Lists.newArrayList(layout.getColumns());
		Form form = new Form();
		form.add("newColumnId", 111);
		form.add("oldColumnId", columns.get(0).getColumnId());
		form.add("position", 0);
		resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class).path("/move/i-dont-exist").build());
		try {
			resource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(form);
			fail("Invalid Layout was found! Check getLayoutByColumnId() check");
		} catch (UniformInterfaceException e) {
			assertThat(e.getResponse().getStatus(), is(Response.Status.NOT_FOUND.getStatusCode()));
			assertThat(e.getResponse().getEntity(String.class), is("No layout with a column with id: 111"));
		}
	}

	/**
	 * Test {@link SonarLayoutRestResource#removeGadget(String, Integer, com.atlassian.plugins.rest.common.security.AuthenticationContext)}
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testRemoveGadgetNotAuthorised() throws Exception {
		setUser();
		WebResource resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class)
			.path(String.valueOf(SonarPluginTestConstants.JIRA_SONAR_PLUGIN_ID)).build());
		resource.accept(MediaType.APPLICATION_JSON);
		com.marvelution.jira.plugins.sonar.rest.model.Layout layout =
			resource.get(com.marvelution.jira.plugins.sonar.rest.model.Layout.class);
		List<com.marvelution.jira.plugins.sonar.rest.model.Column> columns = Lists.newArrayList(layout.getColumns());
		Form form = new Form();
		form.add("columnId", columns.get(1).getColumnId());
		resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class).path("/remove/events").build());
		try {
			resource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(form);
			fail("The API is for admins only - check permission implementation");
		} catch (UniformInterfaceException e) {
			assertThat(e.getResponse().getStatus(), is(Response.Status.UNAUTHORIZED.getStatusCode()));
		}
	}

	/**
	 * Test {@link SonarLayoutRestResource#removeGadget(String, Integer, com.atlassian.plugins.rest.common.security.AuthenticationContext)}
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testRemoveGadget() throws Exception {
		setAdmin();
		WebResource resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class)
			.path(String.valueOf(SonarPluginTestConstants.JIRA_SONAR_PLUGIN_ID)).build());
		resource.accept(MediaType.APPLICATION_JSON);
		com.marvelution.jira.plugins.sonar.rest.model.Layout layout =
			resource.get(com.marvelution.jira.plugins.sonar.rest.model.Layout.class);
		List<com.marvelution.jira.plugins.sonar.rest.model.Column> columns = Lists.newArrayList(layout.getColumns());
		Form form = new Form();
		form.add("columnId", columns.get(1).getColumnId());
		resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class).path("/remove/events").build());
		ClientResponse response = resource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, form);
		assertThat(response.getStatus(), is(Response.Status.OK.getStatusCode()));
		resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class)
			.path(String.valueOf(SonarPluginTestConstants.JIRA_SONAR_PLUGIN_ID)).build());
		resource.accept(MediaType.APPLICATION_JSON);
		layout = resource.get(com.marvelution.jira.plugins.sonar.rest.model.Layout.class);
		columns = Lists.newArrayList(layout.getColumns());
		assertThat(columns.get(1).getGadgets().contains("events"), is(false));
	}

	/**
	 * Test {@link SonarLayoutRestResource#removeGadget(String, Integer, com.atlassian.plugins.rest.common.security.AuthenticationContext)}
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testRemoveGadgetInvalidGadgetShortName() throws Exception {
		setAdmin();
		WebResource resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class)
			.path(String.valueOf(SonarPluginTestConstants.JIRA_SONAR_PLUGIN_ID)).build());
		resource.accept(MediaType.APPLICATION_JSON);
		com.marvelution.jira.plugins.sonar.rest.model.Layout layout =
			resource.get(com.marvelution.jira.plugins.sonar.rest.model.Layout.class);
		Form form = new Form();
		form.add("columnId", layout.getColumns().iterator().next().getColumnId());
		resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class).path("/remove/i-dont-exist").build());
		try {
			resource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(form);
			fail("Invalid Gadget was found! Check Gadget shortname check");
		} catch (UniformInterfaceException e) {
			assertThat(e.getResponse().getStatus(), is(Response.Status.NOT_FOUND.getStatusCode()));
			assertThat(e.getResponse().getEntity(String.class), is("No gadget is available with short name i-dont-exist"));
		}
	}

	/**
	 * Test {@link SonarLayoutRestResource#removeGadget(String, Integer, com.atlassian.plugins.rest.common.security.AuthenticationContext)}
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testRemoveGadgetInvalidLayout() throws Exception {
		setAdmin();
		Form form = new Form();
		form.add("columnId", 111);
		WebResource resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class).path("/remove/i-dont-exist").build());
		try {
			resource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(form);
			fail("Invalid Layout was found! Check getLayoutByColumnId() check");
		} catch (UniformInterfaceException e) {
			assertThat(e.getResponse().getStatus(), is(Response.Status.NOT_FOUND.getStatusCode()));
			assertThat(e.getResponse().getEntity(String.class), is("No layout with a column with id: 111"));
		}
	}

	/**
	 * Test {@link SonarLayoutRestResource#changeLayout(Integer, com.marvelution.jira.plugins.sonar.services.layout.Layout.Type, com.atlassian.plugins.rest.common.security.AuthenticationContext)}
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testChangeLayoutNotAuthorised() throws Exception {
		setUser();
		WebResource resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class)
			.path(String.valueOf(SonarPluginTestConstants.JIRA_SONAR_PLUGIN_ID)).build());
		resource.accept(MediaType.APPLICATION_JSON);
		com.marvelution.jira.plugins.sonar.rest.model.Layout layout =
			resource.get(com.marvelution.jira.plugins.sonar.rest.model.Layout.class);
		Form form = new Form();
		form.add("type", Layout.Type.AB);
		resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class).path("/change/" + layout.getLayoutId()).build());
		try {
			resource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(form);
			fail("The API is for admins only - check permission implementation");
		} catch (UniformInterfaceException e) {
			assertThat(e.getResponse().getStatus(), is(Response.Status.UNAUTHORIZED.getStatusCode()));
		}
	}

	/**
	 * Test {@link SonarLayoutRestResource#changeLayout(Integer, com.marvelution.jira.plugins.sonar.services.layout.Layout.Type, com.atlassian.plugins.rest.common.security.AuthenticationContext)}
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testChangeLayout() throws Exception {
		setAdmin();
		WebResource resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class)
			.path(String.valueOf(SonarPluginTestConstants.JIRA_SONAR_PLUGIN_ID)).build());
		resource.accept(MediaType.APPLICATION_JSON);
		com.marvelution.jira.plugins.sonar.rest.model.Layout layout =
			resource.get(com.marvelution.jira.plugins.sonar.rest.model.Layout.class);
		Form form = new Form();
		form.add("type", Layout.Type.AB);
		resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class).path("/change/" + layout.getLayoutId()).build());
		ClientResponse response = resource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, form);
		assertThat(response.getStatus(), is(Response.Status.OK.getStatusCode()));
		resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class)
			.path(String.valueOf(SonarPluginTestConstants.JIRA_SONAR_PLUGIN_ID)).build());
		resource.accept(MediaType.APPLICATION_JSON);
		layout = resource.get(com.marvelution.jira.plugins.sonar.rest.model.Layout.class);
		assertThat(layout.getType(), is(Layout.Type.AB));
		List<com.marvelution.jira.plugins.sonar.rest.model.Column> columns = Lists.newArrayList(layout.getColumns());
		assertThat(columns.size(), is(2));
		assertThat(columns.get(0).getType(), is(Column.Type.A));
		assertThat(columns.get(1).getType(), is(Column.Type.B));
		form = new Form();
		form.add("type", Layout.Type.AAA);
		resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class).path("/change/" + layout.getLayoutId()).build());
		response = resource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class, form);
		assertThat(response.getStatus(), is(Response.Status.OK.getStatusCode()));
		resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class)
			.path(String.valueOf(SonarPluginTestConstants.JIRA_SONAR_PLUGIN_ID)).build());
		resource.accept(MediaType.APPLICATION_JSON);
		layout = resource.get(com.marvelution.jira.plugins.sonar.rest.model.Layout.class);
		assertThat(layout.getType(), is(Layout.Type.AAA));
		columns = Lists.newArrayList(layout.getColumns());
		assertThat(columns.size(), is(3));
		assertThat(columns.get(0).getType(), is(Column.Type.A));
		assertThat(columns.get(1).getType(), is(Column.Type.A));
		assertThat(columns.get(2).getType(), is(Column.Type.A));
	}

	/**
	 * Test {@link SonarLayoutRestResource#changeLayout(Integer, com.marvelution.jira.plugins.sonar.services.layout.Layout.Type, com.atlassian.plugins.rest.common.security.AuthenticationContext)}
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testChangeLayoutInvalidLayout() throws Exception {
		setAdmin();
		Form form = new Form();
		form.add("type", Layout.Type.AB);
		WebResource resource = client.resource(UriBuilder.fromUri(restUri).path(SonarLayoutRestResource.class).path("/change/111").build());
		try {
			resource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(form);
			fail("Invalid Layout was found! Check getLayoutById() check");
		} catch (UniformInterfaceException e) {
			assertThat(e.getResponse().getStatus(), is(Response.Status.NOT_FOUND.getStatusCode()));
			assertThat(e.getResponse().getEntity(String.class), is("No layout with ID: 111"));
		}
	}

}
