/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.com.marvelution.jira.plugins.sonar.web.action.admin.servers;

import org.junit.Test;

import com.atlassian.jira.functest.framework.locator.WebPageLocator;
import com.atlassian.jira.nimblefunctests.annotation.RestoreOnce;
import com.atlassian.jira.nimblefunctests.framework.NimbleFuncTestCase;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServer;
import com.marvelution.jira.plugins.sonar.utils.SonarPluginTestConstants;

/**
 * Integration testcase to test the {@link SonarServer} administration features
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 3.1.0
 */
@RestoreOnce(SonarPluginTestConstants.DEFAULT_JIRA_EXPORT)
public class SonarServerAdministrationTest extends NimbleFuncTestCase {

	/**
	 * Test the permissions validation for the Sonar Server administration
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testPermissionValidation() throws Exception {
		navigation.login("user");
		navigation.gotoPage("/secure/admin/sonar/AdministerSonarServers.jspa");
		tester.assertTitleEquals("Log in - Your Company JIRA");
		text.assertTextPresent(new WebPageLocator(tester), "'User' does not have permission to access this page.");
		navigation.logout();
		navigation.login("admin");
		navigation.gotoPage("/secure/admin/sonar/AdministerSonarServers.jspa");
		tester.assertTitleEquals("Sonar Servers - Your Company JIRA");
		navigation.logout();
	}

	/**
	 * Test Adding a Sonar Server
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testAddServer() throws Exception {
		navigation.login("admin");
		navigation.gotoPage("/secure/admin/sonar/AdministerSonarServers.jspa");
		tester.assertTitleEquals("Sonar Servers - Your Company JIRA");
		tester.clickLink("add_server");
		tester.assertTitleEquals("Add a Server - Your Company JIRA");
		tester.assertFormPresent("jiraform");
		tester.setWorkingForm("jiraform");
		tester.submit(" Add ");
		text.assertTextPresent(new WebPageLocator(tester), "The server name is required");
		tester.setFormElement("name", "IT case server");
		tester.submit(" Add ");
		text.assertTextPresent(new WebPageLocator(tester), "The server host url is required");
		tester.setFormElement("host", "localhost:9000");
		tester.submit(" Add ");
		text.assertTextPresent(new WebPageLocator(tester), "The server host url is invalid, it must start with http:// or https://");
		tester.setFormElement("host", "http://localhost:9000");
		tester.submit(" Add ");
		text.assertTextPresent(new WebPageLocator(tester), "Failed to get the currect status of the Sonar server. Please verify that it is up and running.");
		tester.setFormElement("host", "http://nemo.sonarsource.org");
		tester.submit(" Add ");
		tester.assertTitleEquals("Sonar Servers - Your Company JIRA");
		text.assertTextPresent(new WebPageLocator(tester), "IT case server");
		navigation.logout();
	}

	/**
	 * Test Editing a Sonar Server
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testEditServer() throws Exception {
		navigation.login("admin");
		navigation.gotoPage("/secure/admin/sonar/AdministerSonarServers.jspa");
		tester.assertTitleEquals("Sonar Servers - Your Company JIRA");
		text.assertTextNotPresent(new WebPageLocator(tester), "IT case server (Includable in Activity Streams)");
		tester.clickLinkWithTextAfterText("Edit", "IT case server");
		tester.assertTitleEquals("Update a Server - Your Company JIRA");
		tester.setWorkingForm("jiraform");
		tester.assertFormElementEquals("name", "IT case server");
		tester.assertFormElementEquals("host", "http://nemo.sonarsource.org");
		tester.setFormElement("includeInStreams", "true");
		tester.submit(" Update ");
		tester.assertTitleEquals("Sonar Servers - Your Company JIRA");
		text.assertTextPresent(new WebPageLocator(tester), "IT case server (Includable in Activity Streams)");
		navigation.logout();
	}

	/**
	 * Test deleting a Sonar Server
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testDeleteServer() throws Exception {
		navigation.login("admin");
		navigation.gotoPage("/secure/admin/sonar/AdministerSonarServers.jspa");
		tester.assertTitleEquals("Sonar Servers - Your Company JIRA");
		text.assertTextPresent(new WebPageLocator(tester), "IT case server (Includable in Activity Streams)");
		tester.clickLinkWithTextAfterText("Delete", "IT case server");
		tester.assertTitleEquals("Delete Server - Your Company JIRA");
		text.assertTextPresent(new WebPageLocator(tester), "Are you sure to delete Server IT case server?");
		tester.setWorkingForm("jiraform");
		tester.submit(" Delete ");
		tester.assertTitleEquals("Sonar Servers - Your Company JIRA");
		text.assertTextNotPresent(new WebPageLocator(tester), "IT case server");
		text.assertTextNotPresent(new WebPageLocator(tester), "IT case server (Includable in Activity Streams)");
		navigation.logout();
	}
}
