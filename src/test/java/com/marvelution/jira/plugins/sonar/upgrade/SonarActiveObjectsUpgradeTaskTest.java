/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.upgrade;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import net.java.ao.EntityManager;
import net.java.ao.test.jdbc.Data;
import net.java.ao.test.jdbc.DatabaseUpdater;
import net.java.ao.test.junit.ActiveObjectsJUnitRunner;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.test.TestActiveObjects;
import com.atlassian.jira.bc.whitelist.WhitelistManager;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import com.google.common.collect.Maps;
import com.marvelution.gadgets.sonar.utils.SonarGadgetsUtils;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociation;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociationManager;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociationManagerService;
import com.marvelution.jira.plugins.sonar.services.layout.Column;
import com.marvelution.jira.plugins.sonar.services.layout.ColumnToGadget;
import com.marvelution.jira.plugins.sonar.services.layout.Gadget;
import com.marvelution.jira.plugins.sonar.services.layout.Layout;
import com.marvelution.jira.plugins.sonar.services.layout.SonarPanelLayoutManager;
import com.marvelution.jira.plugins.sonar.services.layout.SonarPanelLayoutManagerService;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServer;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServerManager;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServerManagerService;
import com.marvelution.jira.plugins.sonar.upgrade.activeobjects.SonarGadgetsActiveObjectsValidator;
import com.marvelution.jira.plugins.sonar.utils.PluginHelper;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.module.propertyset.PropertySetManager;


/**
 * Test class for the {@link SonarActiveObjectsUpgradeTask}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * 
 * @since 2.4.0
 */
@RunWith(ActiveObjectsJUnitRunner.class)
@Data(SonarActiveObjectsUpgradeTaskTest.SonarActiveObjectsUpgradeTaskTestDatabaseUpdater.class)
public class SonarActiveObjectsUpgradeTaskTest {

	@Mock private WhitelistManager whitelistManager;
	@Mock private I18nResolver i18nResolver;

	private SonarGadgetsUtils gadgetsUtils;
	private EntityManager entityManager;
	private ActiveObjects objects;
	private SonarServerManager serverManager;
	private SonarAssociationManager associationManager;
	private SonarPanelLayoutManager layoutManager;
	private PluginUpgradeTask upgradeTask;
	private SonarGadgetsActiveObjectsValidator gadgetsValidator;

	/**
	 * Set-Up the tests
	 * 
	 * @throws Exception in case of errors
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		assertNotNull(entityManager);
		when(i18nResolver.getAllTranslationsForPrefix("sonar.gadget", Locale.ENGLISH)).thenAnswer(
			new Answer<Map<String, String>>() {

			/**
			 * {@inheritDoc}
			 */
			@Override
			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				Properties properties = new Properties();
				properties.load(getClass().getClassLoader().getResourceAsStream("i18n/sonar-gadgets.properties"));
				return Maps.fromProperties(properties);
			}

		});
		gadgetsUtils = new SonarGadgetsUtils(i18nResolver);
		objects = new TestActiveObjects(entityManager);
		gadgetsValidator = new SonarGadgetsActiveObjectsValidator(gadgetsUtils);
		gadgetsValidator.upgrade(null, objects);
		serverManager = new SonarServerManagerService(objects, whitelistManager);
		associationManager = new SonarAssociationManagerService(objects, serverManager);
		layoutManager = new SonarPanelLayoutManagerService(objects, gadgetsValidator);
		upgradeTask = new SonarActiveObjectsUpgradeTask(serverManager, associationManager, layoutManager) {

			/**
			 * {@inheritDoc}
			 */
			@Override
			protected PropertySet loadPropertySet() {
				PropertySet propertySet = PropertySetManager.getInstance("memory", Maps.newHashMap());
				Properties properties = new Properties();
				InputStream input = getClass().getResourceAsStream("SonarActiveObjectsUpgradeTaskTest.properties");
				try {
					properties.load(input);
				} catch (IOException e) {
					throw new RuntimeException("Failed to load Test Data", e);
				}
				for (Entry<Object, Object> entry : properties.entrySet()) {
					String key = (String) entry.getKey();
					String value = (String) entry.getValue();
					if (key.toLowerCase().endsWith("id")) {
						propertySet.setLong(key, Long.parseLong(value));
					} else {
						propertySet.setString(key, value);
					}
				}
				return propertySet;
			}

		};
	}

	/**
	 * Test {@link SonarActiveObjectsUpgradeTask#doUpgrade()}
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testDoUpgrade() throws Exception {
		assertEquals(0, objects.count(SonarServer.class));
		assertEquals(0, objects.count(SonarAssociation.class));
		assertEquals(0, serverManager.getServers().size());
		assertEquals(0, associationManager.getAssociations().size());
		assertNotNull(layoutManager.getSystemLayout());
		assertEquals(null, upgradeTask.doUpgrade());
		assertEquals(3, objects.count(SonarServer.class));
		assertEquals(3, serverManager.getServers().size());
		for (SonarServer server : serverManager.getServers()) {
			if (StringUtils.isNotBlank(server.getUsername())) {
				assertEquals(server.getUsername(), server.getPassword());
			}
		}
		assertEquals(5, objects.count(SonarAssociation.class));
		assertEquals(5, associationManager.getAssociations().size());
		assertNotNull(layoutManager.getSystemLayout());
		// TODO Validate the migration results
	}

	/**
	 * Test {@link SonarActiveObjectsUpgradeTask#getBuildNumber()}
	 */
	@Test
	public void testGetBuildNumber() {
		assertEquals(12, upgradeTask.getBuildNumber());
	}

	/**
	 * Test {@link SonarActiveObjectsUpgradeTask#getShortDescription()}
	 */
	@Test
	public void testGetShortDescription() {
		assertEquals("UpgradeTask to upgrade the Sonar Server starage to ActiveObjects",
			upgradeTask.getShortDescription());
	}

	/**
	 * Test {@link SonarActiveObjectsUpgradeTask#getPluginKey()}
	 */
	@Test
	public void testGetPluginKey() {
		assertEquals(PluginHelper.getPluginKey(), upgradeTask.getPluginKey());
	}

	/**
	 * {@link DatabaseUpdater} implementation
	 * 
	 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
	 */
	public static class SonarActiveObjectsUpgradeTaskTestDatabaseUpdater implements DatabaseUpdater {

		/**
		 * {@inheritDoc}
		 */
		@SuppressWarnings("unchecked")
		@Override
		public void update(EntityManager entityManager) throws Exception {
			entityManager.migrate(SonarServer.class, SonarAssociation.class, Layout.class, Column.class,
				Gadget.class, ColumnToGadget.class);
		}

	}

}
