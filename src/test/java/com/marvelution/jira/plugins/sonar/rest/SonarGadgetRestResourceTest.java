/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.rest;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.collect.Lists;
import com.marvelution.jira.plugins.sonar.rest.exceptions.NotAssociatedException;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociation;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociationManager;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServer;
import com.marvelution.jira.plugins.sonar.utils.SonarGadgetUtils;

/**
 * Testcase for {@link SonarGadgetRestResource}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class SonarGadgetRestResourceTest {

	private SonarGadgetRestResource restResource;

	@Mock private SonarAssociationManager sonarAssociationManager;
	@Mock private SonarAssociation association;
	@Mock private SonarServer server;
	@Mock private SonarGadgetUtils sonarGadgetUtils;
	@Mock private HttpServletRequest request;

	/**
	 * Setup test variables
	 * 
	 * @throws Exception in case of errors
	 */
	@Before
	public void before() throws Exception {
		MockitoAnnotations.initMocks(this);
		restResource = new SonarGadgetRestResource(sonarAssociationManager, sonarGadgetUtils);
	}

	/**
	 * Test {@link SonarGadgetRestResource#generateGadgetHtml(Long, String, javax.servlet.http.HttpServletRequest)}
	 */
	@Test
	public void testGenerateGadgetHtml() {
		when(association.getID()).thenReturn(1);
		when(association.getSonarServer()).thenReturn(server);
		when(association.getSonarProject()).thenReturn("com.marvelution.jira.plugins:jira-sonar-plugin");
		when(server.getHost()).thenReturn("http://sonar.marvelution.com");
		final MapBuilder<String, String> prefs = MapBuilder.newBuilder();
		when(sonarAssociationManager.getAssociation(1)).thenReturn(association);
		when(sonarGadgetUtils.getGadgetIds()).thenReturn(Lists.newArrayList("comments"));
		when(sonarGadgetUtils.createGadgetPreferences(association)).thenReturn(prefs);
		when(sonarGadgetUtils.generateGadgetHtml("comments", prefs, request)).thenReturn("Fake gadget html");
		assertThat(restResource.generateGadgetHtml(1, "comments", request), is("Fake gadget html"));
	}

	/**
	 * Test {@link SonarGadgetRestResource#generateGadgetHtml(Long, String, javax.servlet.http.HttpServletRequest)}
	 */
	@Test
	public void testGenerateGadgetHtmlNotAssociatedException() {
		when(sonarAssociationManager.getAssociation(1)).thenReturn(null);
		when(sonarGadgetUtils.getGadgetIds()).thenReturn(Lists.newArrayList("comments"));
		try {
			restResource.generateGadgetHtml(1, "comments", null);
			fail("This test should fail.");
		} catch (NotAssociatedException e) {
			assertThat(e.getResponse().getEntity().toString(), is("Project not Associated with a Sonar Server"));
		}
	}

}
