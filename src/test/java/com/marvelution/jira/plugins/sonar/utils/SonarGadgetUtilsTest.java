/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.utils;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.atlassian.gadgets.GadgetParsingException;
import com.atlassian.gadgets.GadgetRequestContext;
import com.atlassian.gadgets.GadgetRequestContextFactory;
import com.atlassian.gadgets.GadgetState;
import com.atlassian.gadgets.view.GadgetViewFactory;
import com.atlassian.gadgets.view.ModuleId;
import com.atlassian.gadgets.view.View;
import com.atlassian.gadgets.view.ViewComponent;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.sal.api.message.I18nResolver;
import com.marvelution.gadgets.sonar.utils.SonarGadgetsUtils;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociation;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServer;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServerUtils;

/**
 * Testcase {@link SonarGadgetUtils}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class SonarGadgetUtilsTest {

	@Mock private GadgetRequestContextFactory gadgetRequestContextFactory;
	@Mock private GadgetViewFactory gadgetViewFactory;
	@Mock private ViewComponent viewComponent;
	@Mock private I18nResolver i18nResolver;
	@Mock private SonarAssociation association;
	@Mock private SonarServer server;

	private SonarGadgetUtils gadgetUtils;
	private SonarGadgetsUtils sonarGadgetsUtils;

	/**
	 * Setup the test variables
	 * 
	 * @throws Exception in case of errors
	 */
	@Before
	public void before() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(gadgetRequestContextFactory.get(null)).thenReturn(GadgetRequestContext.NO_CURRENT_REQUEST);
		when(server.getHost()).thenReturn("http://sonar.marvelution.com");
		when(association.getID()).thenReturn(1);
		when(association.getProjectId()).thenReturn(1L);
		when(association.getComponentId()).thenReturn(0L);
		when(association.getSonarServer()).thenReturn(server);
		when(association.getSonarProject()).thenReturn("com.marvelution.jira.plugins:jira-sonar-plugin");
		sonarGadgetsUtils = new SonarGadgetsUtils(i18nResolver);
		gadgetUtils = new SonarGadgetUtils(gadgetRequestContextFactory, gadgetViewFactory, sonarGadgetsUtils);
	}

	/**
	 * Test {@link SonarGadgetUtils#createGadgetPreferences(SonarAssociation)}
	 */
	@Test
	public void testCreateGadgetPreferencesFromAssociation() {
		final MapBuilder<String, String> preferences = gadgetUtils.createGadgetPreferences(association);
		final Map<String, String> prefsMap = preferences.toMap();
		assertThat(prefsMap.size(), is(6));
		assertThat(prefsMap.get(SonarGadgetUtils.PREF_IS_CONFIGURED), is(Boolean.TRUE.toString()));
		assertThat(prefsMap.get(SonarGadgetUtils.PREFS_IS_CONFIGURABLE), is(Boolean.FALSE.toString()));
		assertThat(prefsMap.get(SonarGadgetUtils.PREFS_REFRESH), is(Boolean.FALSE.toString()));
		assertThat(prefsMap.get(SonarGadgetUtils.PREF_TITLE_REQUIRED), is(Boolean.FALSE.toString()));
		assertThat(prefsMap.get(SonarGadgetUtils.PREF_SONAR_SERVER),
			is(SonarServerUtils.getSonarServerGadgetUrl(association.getSonarServer())));
		assertThat(prefsMap.get(SonarGadgetUtils.PREF_SONAR_PROJECT), is(association.getSonarProject()));
	}

	/**
	 * Test {@link SonarGadgetUtils#createGadgetPreferences(String, String)}
	 */
	@Test
	public void testCreateGadgetPreferences() {
		final MapBuilder<String, String> preferences =
			gadgetUtils.createGadgetPreferences("http://sonar.marvelution.com",
				"com.marvelution.jira.plugins:jira-sonar-plugin");
		final Map<String, String> prefsMap = preferences.toMap();
		assertThat(prefsMap.size(), is(6));
		assertThat(prefsMap.get(SonarGadgetUtils.PREF_IS_CONFIGURED), is(Boolean.TRUE.toString()));
		assertThat(prefsMap.get(SonarGadgetUtils.PREFS_IS_CONFIGURABLE), is(Boolean.FALSE.toString()));
		assertThat(prefsMap.get(SonarGadgetUtils.PREFS_REFRESH), is(Boolean.FALSE.toString()));
		assertThat(prefsMap.get(SonarGadgetUtils.PREF_TITLE_REQUIRED), is(Boolean.FALSE.toString()));
		assertThat(prefsMap.get(SonarGadgetUtils.PREF_SONAR_SERVER), is("http://sonar.marvelution.com"));
		assertThat(prefsMap.get(SonarGadgetUtils.PREF_SONAR_PROJECT),
			is("com.marvelution.jira.plugins:jira-sonar-plugin"));
	}

	/**
	 * Test {@link SonarGadgetUtils#generateGadgetHtml(String, MapBuilder, HttpServletRequest)}
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testGenerateGadgetHtml() throws Exception {
		final MapBuilder<String, String> preferences = gadgetUtils.createGadgetPreferences(association);
		when(gadgetViewFactory.createGadgetView(any(GadgetState.class), any(ModuleId.class), any(View.class),
			any(GadgetRequestContext.class))).thenAnswer(new Answer<ViewComponent>() {
				public ViewComponent answer(InvocationOnMock invocation) throws Throwable {
					return new ViewComponent() {
						public void writeTo(Writer writer) throws IOException {
							writer.write("Violations gadget HTML will be here");
						}
					};
				}
			});
		assertThat(gadgetUtils.generateGadgetHtml("violations", preferences, null),
			is("Violations gadget HTML will be here"));
	}

	/**
	 * Test {@link SonarGadgetUtils#generateGadgetHtml(String, MapBuilder, HttpServletRequest)}
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testGenerateGadgetHtmlIOException() throws Exception {
		final MapBuilder<String, String> preferences = gadgetUtils.createGadgetPreferences(association);
		when(gadgetViewFactory.createGadgetView(any(GadgetState.class), any(ModuleId.class), any(View.class),
			any(GadgetRequestContext.class))).thenReturn(viewComponent);
		doThrow(new IOException("Fialure")).when(viewComponent).writeTo(any(Writer.class));
		assertThat(gadgetUtils.generateGadgetHtml("violations", preferences, null), is(""));
	}

	/**
	 * Test {@link SonarGadgetUtils#generateGadgetHtml(String, MapBuilder, HttpServletRequest)}
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testGenerateGadgetHtmlRuntimeException() throws Exception {
		final MapBuilder<String, String> preferences = gadgetUtils.createGadgetPreferences(association);
		when(gadgetViewFactory.createGadgetView(any(GadgetState.class), any(ModuleId.class), any(View.class),
				any(GadgetRequestContext.class))).thenThrow(new GadgetParsingException("Failure"));
		assertThat(gadgetUtils.generateGadgetHtml("violations", preferences, null), is(""));
	}

	/**
	 * Test {@link SonarGadgetUtils#getGadgetIds()}
	 */
	@Test
	public void testGetGadgetIds() {
		when(i18nResolver.getAllTranslationsForPrefix("sonar.gadget", Locale.ENGLISH)).thenAnswer(
			new Answer<Map<String, String>>() {
				public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
					final Map<String, String> bundle = new HashMap<String, String>();
					bundle.put("sonar.gadget.violations.title", "Sonar Violations");
					bundle.put("sonar.gadget.violations.project.title", "Sonar Violations: {0}");
					bundle.put("sonar.gadget.coverage.title", "Sonar Coverage");
					bundle.put("sonar.gadget.coverage.project.title", "Sonar Coverage: {0}");
					bundle.put("sonar.gadget.complexity.title", "Sonar Complexity");
					bundle.put("sonar.gadget.complexity.project.title", "Sonar Complexity: {0}");
					bundle.put("sonar.gadget.loc.title", "Sonar Lines of Code");
					bundle.put("sonar.gadget.loc.project.title", "Sonar Lines of Code: {0}");
					bundle.put("sonar.gadget.comments.title", "Sonar Comments");
					bundle.put("sonar.gadget.comments.project.title", "Sonar Comments: {0}");
					bundle.put("sonar.gadget.treemap.title", "Sonar Treemap");
					bundle.put("sonar.gadget.treemap.project.title", "Sonar Treemap: {0}");
					bundle.put("sonar.gadget.sqale-sunburst.title", "SQALE Rating");
					bundle.put("sonar.gadget.sqale-sunburst.project.title", "SQALE Rating: {0}");
					bundle.put("sonar.gadget.sqale-rating.title", "SQALE Sunburst");
					bundle.put("sonar.gadget.sqale-rating.project.title", "SQALE Sunburst: {0}");
					return bundle;
				}
		});
		final Collection<String> gadgetIds = gadgetUtils.getGadgetIds();
		assertThat(gadgetIds.isEmpty(), is(false));
		assertThat(gadgetIds.size(), is(8));
		assertThat(gadgetIds.contains("comments"), is(true));
		assertThat(gadgetIds.contains("coverage"), is(true));
		assertThat(gadgetIds.contains("violations"), is(true));
		assertThat(gadgetIds.contains("complexity"), is(true));
		assertThat(gadgetIds.contains("loc"), is(true));
		assertThat(gadgetIds.contains("treemap"), is(true));
		verify(i18nResolver, VerificationModeFactory.times(1)).getAllTranslationsForPrefix("sonar.gadget",
			Locale.ENGLISH);
	}

}
