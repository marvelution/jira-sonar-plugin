/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.utils;

/**
 * Plugin test constants
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 3.1.0
 */
public class SonarPluginTestConstants {

	public static final String ADMIN_USERNAME = "admin";
	public static final String ADMIN_PASSWORD = ADMIN_USERNAME;

	public static final String USER_USERNAME = "user";
	public static final String USER_PASSWORD = USER_USERNAME;

	public static final Long JIRA_SONAR_PLUGIN_ID = 10000L;
	public static final String JIRA_SONAR_PLUGIN_KEY = "MARVJIRASONAR";

	public static final String DEFAULT_JIRA_EXPORT = "jira-export.zip";

}
