/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.rest;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.sonar.wsclient.Sonar;
import org.sonar.wsclient.connectors.ConnectionException;
import org.sonar.wsclient.connectors.Connector;
import org.sonar.wsclient.services.ResourceQuery;

import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociation;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociationManager;
import com.marvelution.jira.plugins.sonar.services.servers.SonarClientFactory;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServer;

/**
 * Testcase for {@link SonarAssociationRestResource}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class SonarAssociationRestResourceTest {

	@Mock private SonarAssociationManager associationManager;
	@Mock private SonarClientFactory clientFactory;
	@Mock private SonarAssociation association;
	@Mock private SonarServer server;
	@Mock private Connector connector;

	private SonarAssociationRestResource restResource;

	/**
	 * Setup the test variables
	 * 
	 * @throws Exception in case of errors
	 */
	@Before
	public void before() throws Exception {
		MockitoAnnotations.initMocks(this);
		restResource = new SonarAssociationRestResource(associationManager, clientFactory);
	}

	/**
	 * Test {@link SonarAssociationRestResource#getSonarProjectName(Integer)}
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testGetSonarProjectName() throws Exception {
		when(association.getSonarServer()).thenReturn(server);
		when(association.getSonarProject()).thenReturn("jira-sonar-plugin");
		when(associationManager.getAssociation(1)).thenReturn(association);
		when(clientFactory.create(server)).thenReturn(new Sonar(connector));
		when(connector.execute(any(ResourceQuery.class))).thenReturn("[{\"id\":48569,\"key\":\"jira-sonar-plugin\","
			+ "\"name\":\"JIRA Sonar Plugin\",\"scope\":\"PRJ\",\"qualifier\":\"TRK\","
			+ "\"date\":\"2011-11-09T17:39:32+0000\",\"lname\":\"JIRA Sonar Plugin\",\"lang\":\"java\","
			+ "\"version\":\"2.4.0-SNAPSHOT\",\"description\":\"\"}]");
		Response response = restResource.getSonarProjectName(1);
		assertEquals(String.class, response.getEntity().getClass());
		assertEquals("JIRA Sonar Plugin", response.getEntity());
	}

	/**
	 * Test {@link SonarAssociationRestResource#getSonarProjectName(Integer)} with an invalid association id
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testGetSonarProjectNameNoAssociation() throws Exception {
		when(associationManager.getAssociation(1)).thenReturn(null);
		Response response = restResource.getSonarProjectName(1);
		assertEquals(String.class, response.getEntity().getClass());
		assertEquals("UNKNOWN", response.getEntity());
	}

	/**
	 * Test {@link SonarAssociationRestResource#getSonarProjectName(Integer)} with an communication error
	 * 
	 * @throws Exception in case of test failures
	 */
	@Test
	public void testGetSonarProjectNameConnectionException() throws Exception {
		when(association.getSonarServer()).thenReturn(server);
		when(association.getSonarProject()).thenReturn("jira-sonar-plugin");
		when(associationManager.getAssociation(1)).thenReturn(association);
		when(connector.execute(any(ResourceQuery.class))).thenThrow(new ConnectionException());
		when(clientFactory.create(server)).thenReturn(new Sonar(connector));
		Response response = restResource.getSonarProjectName(1);
		assertEquals(String.class, response.getEntity().getClass());
		assertEquals("jira-sonar-plugin", response.getEntity());
	}

}
