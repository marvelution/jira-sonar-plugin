/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.upgrade.activeobjects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import net.java.ao.EntityManager;
import net.java.ao.test.junit.ActiveObjectsJUnitRunner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.ActiveObjectsUpgradeTask;
import com.atlassian.activeobjects.external.ModelVersion;
import com.atlassian.activeobjects.test.TestActiveObjects;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.collect.Maps;
import com.marvelution.gadgets.sonar.utils.SonarGadgetsUtils;
import com.marvelution.jira.plugins.sonar.services.layout.Gadget;
import com.marvelution.jira.plugins.sonar.utils.PluginHelper;

/**
 * Testcase for the {@link SonarGadgetsActiveObjectsValidator}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 *
 * @since 2.4.1
 */
@RunWith(ActiveObjectsJUnitRunner.class)
public class SonarGadgetsActiveObjectsValidatorTest {

	@Mock private I18nResolver i18nResolver;

	private SonarGadgetsUtils gadgetsUtils;
	private EntityManager entityManager;
	private ActiveObjects objects;
	private ActiveObjectsUpgradeTask upgradeTask;

	/**
	 * Set-Up the tests
	 * 
	 * @throws Exception in case of errors
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		assertNotNull(entityManager);
		when(i18nResolver.getAllTranslationsForPrefix("sonar.gadget", Locale.ENGLISH)).thenAnswer(
			new Answer<Map<String, String>>() {
	
			/**
			 * {@inheritDoc}
			 */
			@Override
			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				Map<String, String> gadgets = Maps.newHashMap();
				Properties properties = new Properties();
				properties.load(getClass().getClassLoader().getResourceAsStream("i18n/sonar-gadgets.properties"));
				for (Entry<Object, Object> entry : properties.entrySet()) {
					gadgets.put((String) entry.getKey(), (String) entry.getValue());
				}
				return gadgets;
			}

		});
		gadgetsUtils = new SonarGadgetsUtils(i18nResolver);
		objects = new TestActiveObjects(entityManager);
		upgradeTask = new SonarGadgetsActiveObjectsValidator(gadgetsUtils);
	}

	/**
	 * Test {@link SonarGadgetsActiveObjectsValidator#upgrade(ModelVersion, ActiveObjects)} without an existing GADGETS
	 * table, this should trigger the upgrade task to {@link EntityManager#migrate(Class...)} the {@link Gadget}
	 * interface.
	 * 
	 * @throws Exception in case of errors
	 */
	@Test
	public void testUpgradeNoGagdetsTable() throws Exception {
		upgradeTask.upgrade(ModelVersion.valueOf("240"), objects);
		assertEquals(gadgetsUtils.getGadgetIds().size(), objects.count(Gadget.class));
		verify(i18nResolver, VerificationModeFactory.times(3)).getAllTranslationsForPrefix("sonar.gadget",
			Locale.ENGLISH);
	}

	/**
	 * Test {@link SonarGadgetsActiveObjectsValidator#upgrade(ModelVersion, ActiveObjects)} with an existing GADGETS
	 * table
	 * 
	 * @throws Exception in case of errors
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testUpgradeWithGadgetsTable() throws Exception {
		entityManager.migrate(Gadget.class);
		upgradeTask.upgrade(ModelVersion.valueOf("240"), objects);
		assertEquals(gadgetsUtils.getGadgetIds().size(), objects.count(Gadget.class));
		verify(i18nResolver, VerificationModeFactory.times(3)).getAllTranslationsForPrefix("sonar.gadget",
			Locale.ENGLISH);
	}

	/**
	 * Test to check that the {@link ModelVersion} of the {@link ActiveObjectsUpgradeTask} is the same as the version of 
	 * the plugin
	 */
	@Test
	public void testGetModelVersion() {
		String rawVersion = PluginHelper.getPluginVersion().replaceAll("[^0-9]+", "");
		ModelVersion expectedCurrent = ModelVersion.valueOf(rawVersion);
		assertEquals(true, expectedCurrent.isSame(upgradeTask.getModelVersion()));
	}

}
