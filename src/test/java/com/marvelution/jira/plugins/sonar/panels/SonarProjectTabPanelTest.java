/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.sonar.panels;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Collection;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.plugin.projectpanel.ProjectTabPanelModuleDescriptorImpl;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.browse.BrowseContext;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociation;
import com.marvelution.jira.plugins.sonar.services.associations.SonarAssociationManager;
import com.marvelution.jira.plugins.sonar.services.servers.SonarServer;
import com.marvelution.jira.plugins.sonar.utils.PluginHelper;

/**
 * Testcase for {@link SonarProjectTabPanel}
 * 
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 */
public class SonarProjectTabPanelTest {

	@Mock private JiraAuthenticationContext authenticationContext;
	@Mock private ModuleFactory moduleFactory;
	@Mock private SonarAssociationManager associationManager;
	@Mock private PermissionManager permissionManager;
	@Mock private WebResourceManager webResourceManager;
	@Mock private BrowseContext browseContext;
	@Mock private Project project;
	@Mock private SonarAssociation association;
	@Mock private SonarServer server;
	@Mock private User user;

	private SonarProjectTabPanel panel;
	private Map<String, Object> velocityParams;

	/**
	 * Setup the test variables
	 * 
	 * @throws Exception in case of errors
	 */
	@Before
	public void before() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(user.getName()).thenReturn("markrekveld");
		when(server.getHost()).thenReturn("http://sonar.marvelution.com");
		when(association.getID()).thenReturn(1);
		when(association.getProjectId()).thenReturn(1L);
		when(association.getComponentId()).thenReturn(0L);
		when(association.getSonarServer()).thenReturn(server);
		when(association.getSonarProject()).thenReturn("com.marvelution.jira.plugins:jira-sonar-plugin");
		panel = new SonarProjectTabPanel(authenticationContext, new SonarTabPanelHelper(associationManager,
			permissionManager, webResourceManager));
		panel.init(new ProjectTabPanelModuleDescriptorImpl(authenticationContext, moduleFactory) {

			/**
			 * {@inheritDoc}
			 */
			@SuppressWarnings({"unchecked", "rawtypes"})
			@Override
			public String getHtml(String resourceName, Map startingParams) {
				velocityParams = startingParams;
				return "fake html for " + resourceName;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public Map<String, String> getParams() {
				return Maps.newHashMap();
			}

		});
	}

	/**
	 * Test {@link SonarProjectTabPanel#getHtml(BrowseContext)}
	 */
	@Test
	public void testGetHtmlNoAssociationAdminUser() {
		when(browseContext.getProject()).thenReturn(project);
		when(browseContext.getUser()).thenReturn(user);
		when(project.getId()).thenReturn(1L);
		when(associationManager.hasAssociations(project, null)).thenReturn(false);
		when(permissionManager.hasPermission(Permissions.PROJECT_ADMIN, project, user)).thenReturn(true);
		assertThat(panel.getHtml(browseContext), is("fake html for view"));
		assertThat((String) velocityParams.get("waitImage"),
			is("/download/resources/" + PluginHelper.getPluginKey() + "/images/wait.gif"));
		assertThat((Long) velocityParams.get("context"), is(1L));
		assertThat((Long) velocityParams.get("componentId"), is(0L));
		assertThat((Boolean) velocityParams.get("isAssociated"), is(false));
		assertThat((Boolean) velocityParams.get("isAdmin"), is(true));
		verify(webResourceManager, VerificationModeFactory.times(1)).requireResource(
			PluginHelper.getPluginKey() + ":sonar-admin");
	}

	/**
	 * Test {@link SonarProjectTabPanel#getHtml(BrowseContext)}
	 */
	@Test
	public void testGetHtmlAssociatedNoAdminUser() {
		final Collection<SonarAssociation> associations = Lists.newArrayList(association);
		when(browseContext.getProject()).thenReturn(project);
		when(browseContext.getUser()).thenReturn(user);
		when(project.getId()).thenReturn(1L);
		when(associationManager.getAssociations(project, null)).thenReturn(associations);
		when(associationManager.hasAssociations(project, null)).thenReturn(true);
		when(permissionManager.hasPermission(Permissions.PROJECT_ADMIN, project, user)).thenReturn(false);
		assertThat(panel.getHtml(browseContext), is("fake html for view"));
		assertThat((String) velocityParams.get("waitImage"),
			is("/download/resources/" + PluginHelper.getPluginKey() + "/images/wait.gif"));
		assertThat((Long) velocityParams.get("context"), is(1L));
		assertThat((Long) velocityParams.get("componentId"), is(0L));
		assertThat((Boolean) velocityParams.get("isAssociated"), is(true));
		assertThat((Boolean) velocityParams.get("isAdmin"), is(false));
	}

	/**
	 * Test {@link SonarProjectTabPanel#showPanel(BrowseContext)} with permission
	 */
	@Test
	public void testShowPanel() {
		when(project.getId()).thenReturn(1L);
		when(browseContext.getProject()).thenReturn(project);
		when(browseContext.getUser()).thenReturn(user);
		when(associationManager.hasAssociations(project, null)).thenReturn(true);
		when(associationManager.getAssociations(project, null)).thenReturn(Lists.newArrayList(association));
		when(permissionManager.hasPermission(Permissions.BROWSE, project, user)).thenReturn(true);
		assertThat(panel.showPanel(browseContext), is(true));
		verify(browseContext, VerificationModeFactory.times(1)).getProject();
		verify(browseContext, VerificationModeFactory.times(1)).getUser();
		verify(permissionManager, VerificationModeFactory.times(1)).hasPermission(Permissions.BROWSE, project, user);
	}

	/**
	 * Test {@link SonarProjectTabPanel#showPanel(BrowseContext)} with permission no associations
	 */
	@Test
	public void testShowPanelNoAssociation() {
		when(browseContext.getProject()).thenReturn(project);
		when(browseContext.getUser()).thenReturn(user);
		when(project.getId()).thenReturn(1L);
		when(associationManager.hasAssociations(project, null)).thenReturn(false);
		when(permissionManager.hasPermission(Permissions.BROWSE, project, user)).thenReturn(true);
		assertThat(panel.showPanel(browseContext), is(false));
		verify(browseContext, VerificationModeFactory.times(1)).getProject();
		verify(browseContext, VerificationModeFactory.times(1)).getUser();
		verify(permissionManager, VerificationModeFactory.times(1)).hasPermission(Permissions.BROWSE, project, user);
	}

	/**
	 * Test {@link SonarProjectTabPanel#showPanel(BrowseContext)} with out permission
	 */
	@Test
	public void testShowPanelNoPermission() {
		when(browseContext.getProject()).thenReturn(project);
		when(browseContext.getUser()).thenReturn(user);
		when(project.getId()).thenReturn(1L);
		when(associationManager.hasAssociations(project, null)).thenReturn(true);
		when(permissionManager.hasPermission(Permissions.BROWSE, project, user)).thenReturn(false);
		assertThat(panel.showPanel(browseContext), is(false));
		verify(browseContext, VerificationModeFactory.times(1)).getProject();
		verify(browseContext, VerificationModeFactory.times(1)).getUser();
		verify(permissionManager, VerificationModeFactory.times(1)).hasPermission(Permissions.BROWSE, project, user);
	}

	/**
	 * Test {@link SonarProjectTabPanel#showPanel(BrowseContext)} with out permission
	 */
	@Test
	public void testShowPanelNoPermissionNoAssociation() {
		when(browseContext.getProject()).thenReturn(project);
		when(browseContext.getUser()).thenReturn(user);
		when(project.getId()).thenReturn(1L);
		when(associationManager.hasAssociations(project, null)).thenReturn(false);
		when(permissionManager.hasPermission(Permissions.BROWSE, project, user)).thenReturn(false);
		assertThat(panel.showPanel(browseContext), is(false));
		verify(browseContext, VerificationModeFactory.times(1)).getProject();
		verify(browseContext, VerificationModeFactory.times(1)).getUser();
		verify(permissionManager, VerificationModeFactory.times(1)).hasPermission(Permissions.BROWSE, project, user);
	}

}
